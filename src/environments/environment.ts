// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyDAx-vXZqln1pSGRK5-34LZvWc8Yse3p9M',
    authDomain: 'selo-ods-dev.firebaseapp.com',
    projectId: 'selo-ods-dev',
    storageBucket: 'selo-ods-dev.appspot.com',
    messagingSenderId: '224118826331',
    appId: '1:224118826331:web:595907a131a8b33594e67e'
  },
  sessionStoragePrefix: 'seloods'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
