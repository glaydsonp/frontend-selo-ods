# Padrões de Código e Organização

## Estrutura de diretórios

```json

scss
  /1-settings
    /_index.scss // organiza ordem de importações desta camada
    /_colors.scss
    /_breakpoints.scss
    /_container.scss
  /2-tools
    /_index.scss // organiza ordem de importações desta camada
  /3-base // bare elements
    /_index.scss // organiza ordem de importações desta camada
    /_vendor.scss // estilos de terceiro
    /_generic.scss // seletores genericos
    /_reset.scss // pode ser de terceiro ou o rexport de terceiro
  /4-objects // layout classes - classes aqui geralmente usam apenas o bloco e o modifier
    /_index.scss // organiza ordem de importações desta camada
    /_container.scss
  /5-components // ui classes
    /_index.scss // organiza ordem de importações desta camada
    /_button.scss
      - BEM component
      - variants
  /6-trumps // permitido o uso de !important nesta camada
    /_index.scss // organiza ordem de importações desta camada
  styles.scss
  shared.scss

```

## Modificação do padrão BEM

* O que é BEM?
 > http://getbem.com/introduction/

### BEM no geral

```scss

.the-block {
  &__the-element {

  }

  // usar todos os modifier com &. (em conjunto com um bloco ou elemento)
  // afim de evitar problemas com conflitos de css
  &.-modifier {

  }

  // é permitido porém porém é preferivel .-modifier
  &--modifier {

  }

  &[color="blue"] {

  }
}

```
```html

<div class="the-block -modifier">
  <!-- O nome do bloco mais o elemento aqui ajuda o desenvolvedor a entender que este elemento é usado com determinado bloco -->
  <div class="the-block__element">
</div>

```

### Uso do BEM em components angular

```scss

// bloco do componente
:host {

}

// modificadores do componente
:host([color="blue"]) {
  // estilo para quando o host usa este modifier
}



// elementos ou blocos aninhados do componente
.element-or-block {
  /** 
    gerado:
    :host([color="blue"]) {

    }
  **/
  @include host('modifier') {
    // estilo para quando o host usa este modifier
  }
}

```

> Blocos aninhados dentro de um componente são usados de forma auxiliar quando componente mais complexo

```html
// uso por fora do component
<c-component modifier></c-component>
```

## Regras para quando necessário não seguir os padrões
- Foi reutilizado componente de outro projeto que segue outro padrão
  > - explicitar no inicio do arquivo nome do padrão ultilizado e link (local ou remoto) com documentação do mesmo
  > - caso não tenha nome indicar arquivo que contenha documentação
  > - caso documentação curta usar apenas um comentário de bloco

