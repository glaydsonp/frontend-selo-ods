import { PipesModule } from './pipes/pipes.module';
import { GenericDialogModule } from './generic-dialog/generic-dialog.module';
import { LoaderModule } from './loader/loader.module';
import { ConfirmationDialogModule } from './confirmation-dialog/confirmation-dialog.module';
import { StepperModule } from './stepper/stepper.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatInputModule } from '@angular/material/input';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatRadioModule } from '@angular/material/radio';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatIconModule } from '@angular/material/icon';
import { MatDialogModule } from '@angular/material/dialog';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { NgxMaskModule } from 'ngx-mask';
import { SafePipe } from './pipes/safe.pipe';

@NgModule({
  imports: [
    CommonModule,
    MatInputModule,
    ReactiveFormsModule,
    FormsModule,
    MatRadioModule,
    MatCheckboxModule,
    StepperModule,
    MatIconModule,
    MatDialogModule,
    ConfirmationDialogModule,
    LoaderModule,
    MatSnackBarModule,
    NgxMaskModule,
    GenericDialogModule,
    PipesModule
  ],
  exports: [
    MatInputModule,
    ReactiveFormsModule,
    FormsModule,
    MatRadioModule,
    MatCheckboxModule,
    StepperModule,
    MatIconModule,
    MatDialogModule,
    ConfirmationDialogModule,
    LoaderModule,
    MatSnackBarModule,
    NgxMaskModule,
    GenericDialogModule,
    PipesModule
  ]
})
export class SharedModule {}
