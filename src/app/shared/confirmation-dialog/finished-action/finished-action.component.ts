import { IUserRole } from './../../../models/user';
import { AuthService } from './../../../services/auth.service';
import { Router } from '@angular/router';
import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-finished-action',
  templateUrl: './finished-action.component.html',
  styleUrls: ['./finished-action.component.scss']
})
export class FinishedActionComponent implements OnInit {
  sendTo: { text: string; link: string };
  IUserRole = IUserRole;

  constructor(
    private _router: Router,
    private dialogRef: MatDialogRef<FinishedActionComponent>,
    private dialog: MatDialog,
    private authService: AuthService,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit(): void {
    console.log('finished', this.data)

    const user = this.authService.getUserRole();

    if (this.data) {
      if (user === IUserRole.Participante) {
        if (this.data === 'org') {
          this.sendTo = {
            text: 'Ir para projetos',
            link: '/meus-projetos'
          };
        }

        if (this.data === 'projetos') {
          this.sendTo = {
            text: 'Ir para relatórios',
            link: '/protocolos-e-relatorios'
          };
        }
      }

      if (user === IUserRole.Curador) {
        this.sendTo = {
          text: 'Ir para parceiros',
          link: '/validacao-parceiros'
        };
      }
    }
  }

  navigate() {
    this._router.navigateByUrl(this.sendTo.link);
    this.dialog.closeAll();
  }
}
