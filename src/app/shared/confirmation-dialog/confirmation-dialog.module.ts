import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConfirmationDialogComponent } from './confirmation-dialog.component';
import { FinishedActionComponent } from './finished-action/finished-action.component';

@NgModule({
  declarations: [ConfirmationDialogComponent, FinishedActionComponent],
  imports: [CommonModule],
  exports: [ConfirmationDialogComponent]
})
export class ConfirmationDialogModule {}
