import { FinishedActionComponent } from './finished-action/finished-action.component';
import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-confirmation-dialog',
  templateUrl: './confirmation-dialog.component.html',
  styleUrls: ['./confirmation-dialog.component.scss']
})
export class ConfirmationDialogComponent implements OnInit {
  constructor(public dialog: MatDialog, @Inject(MAT_DIALOG_DATA) public data: any) {}

  ngOnInit(): void {
    console.log('confirmation', this.data);
  }

  confirm(): void {
    this.dialog.open(FinishedActionComponent, { data: this.data });
  }
}
