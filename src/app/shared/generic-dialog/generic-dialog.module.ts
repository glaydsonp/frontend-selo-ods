import { GenericDialogComponent } from './generic-dialog.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [GenericDialogComponent],
  imports: [CommonModule],
  exports: [GenericDialogComponent]
})
export class GenericDialogModule {}
