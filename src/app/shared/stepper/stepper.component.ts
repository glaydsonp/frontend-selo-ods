import { IProject } from 'src/app/models/project';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { FormGroup } from '@angular/forms';
import { OrganizationService } from 'src/app/services/organization.service';
import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  OnChanges
} from '@angular/core';
import { IStep } from 'src/app/models/register';
import { StepperActionEnum } from 'src/app/models/stepper-action.enum';
import { ProjectService } from 'src/app/services/project.service';

@Component({
  selector: 'app-stepper',
  templateUrl: './stepper.component.html',
  styleUrls: ['./stepper.component.scss']
})
export class StepperComponent implements OnInit, OnChanges {
  form = new FormGroup({});

  @Input()
  currentStep = 0;

  @Input()
  steps: IStep[] = [];

  @Input()
  stepperFrom: string;

  @Input()
  projectId: string;

  @Output()
  changeStepEmiiter: EventEmitter<number> = new EventEmitter();

  constructor(
    private _organizationService: OrganizationService,
    private _db: AngularFirestore,
    private _projectService: ProjectService
  ) {}

  ngOnInit(): void {
    this._organizationService.getForm().subscribe((res) => {
      this.form = res;
      this.checkIfStepIsValid();
    });
  }

  ngOnChanges() {
    console.log(this.stepperFrom);
  }

  changeStep(step: number): void {
    if (step !== this.currentStep) {
      this.currentStep = step;
      this.changeStepEmiiter.emit(step);
    }
  }

  async checkIfStepIsValid() {
    if (this.stepperFrom === 'organization') {
      const dadosCadastraisValid = this.form.get('dadosCadastrais')?.valid;
      const enderecoValid = this.form.get('endereco')?.valid;
      const pessoalValid = this.form.get('pessoal')?.valid;

      if (dadosCadastraisValid && enderecoValid && pessoalValid) {
        this.steps[0].valid = true;
      }

      const perfilValid = this.form.get('perfil')?.valid;
      const projetosValid = this.form.get('projetos')?.valid;
      const expectativaValid = this.form.get('expectativa')?.valid;

      if (perfilValid && projetosValid && expectativaValid) {
        this.steps[1].valid = true;
      }

      const comunicacaoValid = this.form.get('comunicacao')?.valid;

      if (comunicacaoValid) {
        this.steps[2].valid = true;
      }
    }

    if (this.stepperFrom === 'curadoria') {
      this.steps[0].valid = true;

      const projectRef = await this._db
        .collection('projects')
        .doc<IProject>(this.projectId)
        .get()
        .toPromise();

      const project = projectRef.data();

      if (
        !project?.geral.finalidadesSelosOds
          .filter((el) => el.checked)
          .some((el) => !el.validado)
      ) {
        this.steps[1].valid = true;
      }
    }

    if (this.stepperFrom === 'projetos') {
      // this._projectService.getForm().subscribe((form) => {
      const form = this._projectService.buildForm();

      this._projectService.form.subscribe((val) => {
        form.patchValue(val.value);

        const geralValid = form.get('geral')?.valid;
        const estudoRealidadeValid = form.get('estudoRealidade')?.valid;
        const justificativaValid = form.get('justificativa')?.valid;
        const objetivosValid = form.get('objetivos')?.valid;
        const publicoValid = form.get('publico')?.valid;
        const resumoValid = form.get('resumo')?.valid;

        if (
          geralValid &&
          estudoRealidadeValid &&
          justificativaValid &&
          objetivosValid &&
          publicoValid &&
          resumoValid
        ) {
          this.steps[0].valid = true;
        }

        const investimentosValid = form.get('investimentos')?.valid;

        if (investimentosValid) {
          this.steps[1].valid = true;
        }

        const albunsValid = form.get('albuns')?.value.length > 0;
        const artigosValid = form.get('artigos')?.value.length > 0;
        const eventosValid = form.get('eventos')?.value.length > 0;
        const videosValid = form.get('albuns')?.value.length > 0;

        if (albunsValid && artigosValid && eventosValid && videosValid) {
          this.steps[2].valid = true;
        }
      });
      // });
    }

    // switch (step) {
    //   case 0:
    //     const dadosCadastraisValid = this.form.get('dadosCadastrais')?.valid;
    //     const enderecoValid = this.form.get('endereco')?.valid;
    //     const pessoalValid = this.form.get('pessoal')?.valid;

    //     console.log('dadosCadastraisValid', dadosCadastraisValid);

    //     if (dadosCadastraisValid && enderecoValid && pessoalValid) {
    //       return true;
    //     }
    //     break;
    //   case 1:
    //     const perfilValid = this.form.get('perfil')?.valid;
    //     const projetosValid = this.form.get('projetos')?.valid;
    //     const expectativaValid = this.form.get('expectativa')?.valid;

    //     if (perfilValid && projetosValid && expectativaValid) {
    //       return true;
    //     }
    //     break;
    //   case 2:
    //     const comunicacaoValid = this.form.get('comunicacao')?.valid;

    //     if (comunicacaoValid) {
    //       return true;
    //     }
    //     break;

    //   default:
    //     return false;
    // }

    // switch (step) {
    //   case 0:
    //     const dadosCadastraisValid = this.form.get('dadosCadastrais')?.valid;
    //     const enderecoValid = this.form.get('endereco')?.valid;
    //     const pessoalValid = this.form.get('pessoal')?.valid;

    //     console.log('dadosCadastraisValid', dadosCadastraisValid);

    //     if (dadosCadastraisValid && enderecoValid && pessoalValid) {
    //       return true;
    //     }
    //     break;
    //   case 1:
    //     const perfilValid = this.form.get('perfil')?.valid;
    //     const projetosValid = this.form.get('projetos')?.valid;
    //     const expectativaValid = this.form.get('expectativa')?.valid;

    //     if (perfilValid && projetosValid && expectativaValid) {
    //       return true;
    //     }
    //     break;
    //   case 2:
    //     const comunicacaoValid = this.form.get('comunicacao')?.valid;

    //     if (comunicacaoValid) {
    //       return true;
    //     }
    //     break;

    //   default:
    //     return false;
    // }

    // return false;
  }
}
