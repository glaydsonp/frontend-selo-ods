import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output
} from '@angular/core';
import { ISubStep } from 'src/app/models/register';

@Component({
  selector: 'app-substepper',
  templateUrl: './substepper.component.html',
  styleUrls: ['./substepper.component.scss']
})
export class SubstepperComponent implements OnInit {
  @Input() substeps: ISubStep[] = [];
  @Input() currentSubStep = 0;

  @Output() changeSubStepEmitter: EventEmitter<number> = new EventEmitter();

  constructor() {}

  ngOnInit(): void {}

  changeSubStep(substep: number): void {
    if (substep !== this.currentSubStep) {
      this.currentSubStep = substep;
      this.changeSubStepEmitter.emit(this.currentSubStep);
    }
  }
}
