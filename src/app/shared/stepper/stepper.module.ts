import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StepperComponent } from './stepper.component';
import { SubstepperComponent } from './substepper/substepper.component';

@NgModule({
  declarations: [StepperComponent, SubstepperComponent],
  imports: [CommonModule],
  exports: [StepperComponent, SubstepperComponent]
})
export class StepperModule {}
