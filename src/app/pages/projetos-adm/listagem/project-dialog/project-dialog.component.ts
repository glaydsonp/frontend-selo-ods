import { SnackbarType } from './../../../../models/snackbar-type.enum';
import { IDialogMessage } from './../../../../models/dialog-message';
import { GenericDialogComponent } from './../../../../shared/generic-dialog/generic-dialog.component';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA
} from '@angular/material/dialog';
import { IProject } from 'src/app/models/project';
import { NovoObjetivoComponent } from 'src/app/pages/projetos/cadastro-projeto/objetivos/novo-objetivo/novo-objetivo.component';
import { SnackbarService } from 'src/app/services/snackbar.service';
import { IUser } from 'src/app/models/user';

@Component({
  selector: 'app-project-dialog',
  templateUrl: './project-dialog.component.html',
  styleUrls: ['./project-dialog.component.scss']
})
export class ProjectDialogComponent implements OnInit {
  form: FormGroup;

  constructor(
    private _fb: FormBuilder,
    private _snackbarService: SnackbarService,
    private dialog: MatDialog,
    private dialogRef: MatDialogRef<NovoObjetivoComponent>,
    private _db: AngularFirestore,
    @Inject(MAT_DIALOG_DATA) public data: IProject
  ) {}

  ngOnInit(): void {
    this.buildForm();
  }

  async buildForm() {
    const userRef = await this._db
      .collection('users')
      .doc<IUser>(this.data.owner)
      .get()
      .toPromise();

    const user = userRef.data();

    this.form = this._fb.group({
      nomeProjeto: this.data.geral.nome,
      instituicao: this.data.ownerOrganizationName,
      responsavel: user?.nome,
      email: user?.email,
      membroDesde: user?.registro
    });
  }

  deleteProject() {
    const texts: IDialogMessage = {
      title: 'Tem certeza que deseja excluir este projeto?',
      message: 'Essa ação é permanente e não pode ser desfeita!',
      dismiss: 'Cancelar',
      confirm: 'Confirmar'
    };

    const dialogRef = this.dialog.open(GenericDialogComponent, { data: texts });

    dialogRef.afterClosed().subscribe(async (res) => {
      if (res) {
        try {
          await this._db.collection('projects').doc(this.data.id).delete();

          this._snackbarService.openSnackBar(
            'Projeto apagado com sucesso!',
            SnackbarType.Success
          );

          this.dialog.closeAll();
        } catch (err) {
          if (err instanceof Error) {
            this._snackbarService.openSnackBar(
              err.message,
              SnackbarType.Success
            );
          }
        }
      }
    });
  }

  close() {
    this.dialogRef.close(null);
  }
}
