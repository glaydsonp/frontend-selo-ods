import { IDialogMessage } from './../../../../models/dialog-message';
import { GenericDialogComponent } from './../../../../shared/generic-dialog/generic-dialog.component';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA
} from '@angular/material/dialog';
import { IProject } from 'src/app/models/project';
import { NovoObjetivoComponent } from 'src/app/pages/projetos/cadastro-projeto/objetivos/novo-objetivo/novo-objetivo.component';
import { SnackbarService } from 'src/app/services/snackbar.service';
import { IUser } from 'src/app/models/user';
import { IOrganization } from 'src/app/models/organization';

@Component({
  selector: 'app-organization-dialog',
  templateUrl: './organization-dialog.component.html',
  styleUrls: ['./organization-dialog.component.scss']
})
export class OrganizationDialogComponent implements OnInit {
  form: FormGroup;

  constructor(
    private _fb: FormBuilder,
    private _snackbarService: SnackbarService,
    private dialog: MatDialog,
    private dialogRef: MatDialogRef<NovoObjetivoComponent>,
    private _db: AngularFirestore,
    @Inject(MAT_DIALOG_DATA) public data: IOrganization
  ) {}

  ngOnInit(): void {
    this.buildForm();
  }

  async buildForm() {
    const userRef = await this._db
      .collection('users')
      .doc<IUser>(this.data.owner)
      .get()
      .toPromise();

    const user = userRef.data();

    this.form = this._fb.group({
      cnpj: this.data?.dadosCadastrais?.cnpj,
      nome: this.data?.dadosCadastrais?.nomeInstituicao,
      responsavel: user?.nome,
      celular: user?.whatsapp,
      registro: user?.registro
    });
  }

  deleteOrganization() {
    const texts: IDialogMessage = {
      title: 'Tem certeza que deseja excluir esta organização?',
      message: 'Essa ação é permanente e não pode ser desfeita!',
      dismiss: 'Cancelar',
      confirm: 'Confirmar'
    };

    const dialogRef = this.dialog.open(GenericDialogComponent, { data: texts });

    dialogRef.afterClosed().subscribe(async (res) => {
      if (res) {
        await this._db.collection('organizations').doc(this.data.id).delete();
        await this._db
          .collection('users')
          .doc(this.data.owner)
          .update({ organizationId: '' });

        this.dialog.closeAll();
      }
    });
  }

  close() {
    this.dialogRef.close(null);
  }
}
