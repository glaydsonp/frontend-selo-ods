import { OrganizationDialogComponent } from './organization-dialog/organization-dialog.component';
import { ProjectDialogComponent } from './project-dialog/project-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { IOrganization } from './../../../models/organization';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { IProject } from 'src/app/models/project';
import { IUserRole } from 'src/app/models/user';
import { AuthService } from 'src/app/services/auth.service';
import { ProjectService } from 'src/app/services/project.service';
import { CuradorStatusEnum } from 'src/app/models/project-status.enum';

@Component({
  selector: 'app-listagem',
  templateUrl: './listagem.component.html',
  styleUrls: ['./listagem.component.scss']
})
export class ListagemComponent implements OnInit {
  ciclos = [
    { desc: 'Todos', value: '' },
    { desc: 'Ciclo 2021', value: 2021 },
    { desc: 'Ciclo 2022', value: 2022 },
    { desc: 'Ciclo 2023', value: 2023 }
  ];

  projetos: IProject[] = [];

  filtroProjetos = new FormGroup({});

  IUserRole = IUserRole;

  constructor(
    public projectService: ProjectService,
    private _authService: AuthService,
    private _db: AngularFirestore,
    private dialog: MatDialog,
    private _fb: FormBuilder
  ) {}

  ngOnInit() {
    this.buildFiltros();
    this.getProjects();
  }

  buildFiltros() {
    this.filtroProjetos = this._fb.group({
      nome: null,
      ciclo: null
    });
  }

  getProjects() {
    const seeAll =
      this._authService.getUserRole() === IUserRole.Curador || this._authService.getUserRole() === IUserRole.Adm;

    this.projectService.getAll(seeAll).subscribe((res) => {
      this.projetos = res;
    });
  }

  filtrar() {
    const filterForm = this.filtroProjetos.value;

    const isCurator =
      this._authService.getUserRole() === IUserRole.Curador || this._authService.getUserRole() === IUserRole.Adm;

    this.projectService.getAll(isCurator).subscribe((res) => {
      this.projetos = res.filter((el) => {
        // if (!filterForm?.nome && !filterForm?.ciclo) {
        //   return true;
        // }

        console.log(el?.geral?.nome?.toLowerCase().includes(filterForm?.nome?.toLowerCase()));
        // if (
        //   el.geral.nome?.toLowerCase().includes(filterForm?.nome?.toLowerCase()) &&
        //   el?.status === filterForm?.ciclo
        // ) {
        //   return true;
        // }

        // if (el?.geral?.nome?.toLowerCase().includes(filterForm?.nome?.toLowerCase()) && !filterForm?.avaliacao) {
        //   return true;
        // }

        if (el?.geral?.nome?.toLowerCase().includes(filterForm?.nome?.toLowerCase())) {
          return true;
        }

        return false;
      });
    });
  }

  resetFilter() {
    this.filtroProjetos.reset();
  }

  openProjectDialog(project: IProject) {
    const dialogRef = this.dialog.open(ProjectDialogComponent, {
      data: project
    });

    dialogRef.afterClosed().subscribe((res) => {});
  }

  async openOrganizationDialog(ownerOrganizationId: string) {
    const ownerOrganizationNameRef = await this._db
      .collection('organizations')
      .doc<IOrganization>(ownerOrganizationId)
      .get()
      .toPromise();

    const ownerOrganization = ownerOrganizationNameRef.data() as IOrganization;

    const dialogRef = this.dialog.open(OrganizationDialogComponent, {
      data: ownerOrganization
    });

    dialogRef.afterClosed().subscribe((res) => {});
  }

  // async getOrganizationName(id: string) {
  //   const orgId = (
  //     await this._db.collection('users').doc<IUser>(id).get().toPromise()
  //   ).data()?.organizationId;

  //   const orgName = (
  //     await this._db
  //       .collection('organizations')
  //       .doc<IOrganization>(orgId)
  //       .get()
  //       .toPromise()
  //   ).data()?.dadosCadastrais.nomeInstituicao as string;

  //   return orgName;
  // }
}
