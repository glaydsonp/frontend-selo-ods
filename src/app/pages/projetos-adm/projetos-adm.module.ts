import { MatSelectModule } from '@angular/material/select';
import { SharedModule } from 'src/app/shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProjetosAdmRoutingModule } from './projetos-adm-routing.module';
import { ListagemComponent } from './listagem/listagem.component';
import { OrganizationDialogComponent } from './listagem/organization-dialog/organization-dialog.component';
import { ProjectDialogComponent } from './listagem/project-dialog/project-dialog.component';


@NgModule({
  declarations: [
    ListagemComponent,
    OrganizationDialogComponent,
    ProjectDialogComponent
  ],
  imports: [
    CommonModule,
    ProjetosAdmRoutingModule,
    SharedModule,
    MatSelectModule
  ]
})
export class ProjetosAdmModule { }
