import { ActivatedRoute, Router } from '@angular/router';
import { SessionService } from './../../services/session.service';
import { SnackbarType } from './../../models/snackbar-type.enum';
import { SnackbarService } from './../../services/snackbar.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { IOrganization } from './../../models/organization';
import { OrganizationService } from './../../services/organization.service';
import { AuthService } from 'src/app/services/auth.service';
import { ConfirmationDialogComponent } from './../../shared/confirmation-dialog/confirmation-dialog.component';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { IStep } from 'src/app/models/register';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { StepperActionEnum } from 'src/app/models/stepper-action.enum';
import { IUserRole } from 'src/app/models/user';

@Component({
  selector: 'app-organization',
  templateUrl: './organization.component.html',
  styleUrls: ['./organization.component.scss']
})
export class OrganizationComponent implements OnInit {
  step = 0;
  currentSubStep = 0;

  form = new FormGroup({});
  organizationId = '';

  StepperAction = StepperActionEnum;
  userRoles = IUserRole;

  steps: IStep[] = [
    {
      title: 'Cadastro da Instituição',
      step: 0,
      substeps: [
        {
          title: 'Dados cadastrais',
          substep: 0
        },
        {
          title: 'Endereço',
          substep: 1
        },
        {
          title: 'Pessoal',
          substep: 2
        }
      ]
    },
    {
      title: 'Pefil da Instituição',
      step: 1,
      substeps: [
        {
          title: 'Perfil',
          substep: 0
        },
        {
          title: 'Projetos',
          substep: 1
        },
        {
          title: 'Expectativa',
          substep: 2
        }
      ]
    },
    {
      title: 'Comunicação',
      step: 2,
      substeps: []
    }
  ];

  constructor(
    public dialog: MatDialog,
    public authService: AuthService,
    private _organizationService: OrganizationService,
    private _db: AngularFirestore,
    public sessionService: SessionService,
    private _snackbarService: SnackbarService,
    private _fb: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) {
    const params = activatedRoute.snapshot.params;

    if (params.data === 'perfil') {
      this.step = 1;
    }

    if (params.data === 'comunicacoes') {
      this.step = 2;
    }
  }

  ngOnInit(): void {
    this.getOrganizationId();
    this.buildForm();
  }

  getOrganizationId() {
    const params = this.activatedRoute.snapshot.params;

    this.organizationId =
      params && params.id
        ? params.id
        : this.authService.getOrganizationId() || ' ';
  }

  buildForm() {
    this.form = this._organizationService.buildForm();

    const orgRef = this._db
      .collection('organizations')
      .doc<IOrganization>(this.organizationId)
      .get();

    orgRef.subscribe((res) => {
      const org = res.data() as IOrganization;
      this.form.patchValue(org);
      this._organizationService.initializeForm(this.form);
    });
  }

  changeStep(step: number): void {
    if (step !== this.step) {
      this.step = step;
      this.currentSubStep = 0;
    }
  }

  changeSubStep(substep: number): void {
    if (substep !== this.currentSubStep) {
      this.currentSubStep = substep;
    }
  }

  previousOrNextStep(action: number) {
    if (action === StepperActionEnum.Next) {
      if (
        this.step < this.steps.length &&
        this.currentSubStep < this.steps[this.step].substeps.length
      ) {
        this.currentSubStep++;
      }

      if (
        this.step < this.steps.length &&
        this.currentSubStep === this.steps[this.step].substeps.length
      ) {
        this.step++;
        this.currentSubStep = 0;
      }
    }

    if (action === StepperActionEnum.Previous) {
      if (this.step > 0 && this.currentSubStep > 0) {
        this.currentSubStep--;
        return;
      }

      if (this.step === 0 && this.currentSubStep > 0) {
        this.currentSubStep--;
        return;
      }

      if (this.step > 0 && this.currentSubStep === 0) {
        this.step--;
        this.currentSubStep = this.steps[this.step].substeps.length - 1;
        return;
      }
    }
  }

  patchForm(form: FormGroup) {
    this.form = form;
  }

  updateOrganization(action?: number) {
    this._organizationService
      .updateOrganization(this.organizationId)
      .then((res) => {
        if (res && res.success) {
          if (action) {
            this.previousOrNextStep(action);
          }
        }
      });
  }

  submit(): void {
    this.updateOrganization();
    this.dialog.open(ConfirmationDialogComponent, { data: 'org' });
    // const dialogRef = this.dialog.open(ConfirmationDialogComponent);

    // dialogRef.afterClosed().subscribe((result) => {
    //   console.log(`Dialog result: ${result}`);
    // });
  }
}
