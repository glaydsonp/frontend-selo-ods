import { IUserRole } from './../../../models/user';
import { SessionService } from './../../../services/session.service';
import { AuthService } from './../../../services/auth.service';
import { Component, OnInit } from '@angular/core';
import { AngularFireStorage } from '@angular/fire/compat/storage';
import { FormGroup } from '@angular/forms';
import { OrganizationCurrentFormEnum } from 'src/app/models/current-form.enum';
import { OrganizationService } from 'src/app/services/organization.service';
import { finalize } from 'rxjs/operators';
import { AngularFirestore } from '@angular/fire/compat/firestore';

@Component({
  selector: 'app-comunicacao',
  templateUrl: './comunicacao.component.html',
  styleUrls: ['./comunicacao.component.scss']
})
export class ComunicacaoComponent implements OnInit {
  form = new FormGroup({});

  fileName = '';

  userRoles = IUserRole;

  constructor(
    private _organizationService: OrganizationService,
    private storage: AngularFireStorage,
    public authService: AuthService,
    private _db: AngularFirestore,
    public sessionService: SessionService
  ) {}

  ngOnInit(): void {
    this.buildForm();

    this._organizationService.getForm().subscribe((val) => {
      this.form.patchValue({ ...val.value });
    });

    this.form.valueChanges.subscribe((val) => {
      if (val) {
        this._organizationService.setUpdatingForm(
          this.form,
          OrganizationCurrentFormEnum.Comunicacao
        );
      }
    });
  }

  buildForm(): void {
    this.form = this._organizationService.buildForm();
  }

  async onFileChange(event: any) {
    const file = event.files[0];

    if (file) {
      const organizationId = this.authService.getOrganizationId();

      this.fileName = file.name;

      const fileExt = file.type.split('/')[1];
      const filePath = `images/organizations/${organizationId}/logo.${fileExt}`;
      const fileRef = this.storage.ref(filePath);

      const task = this.storage.upload(filePath, file);

      task
        .snapshotChanges()
        .pipe(
          finalize(() => {
            const downloadUrl = fileRef.getDownloadURL();
            downloadUrl.subscribe((url) => {
              if (url) {
                this._db
                  .collection('organizations')
                  .doc(organizationId)
                  .update({ logoUrl: url });
              }
            });
          })
        )
        .subscribe();
      // .toPromise();

      // task.percentageChanges().subscribe((val) => {
      //   this.uploadProgress$ = val;
      //   console.log(val);
      // });
      // return upload.ref.getDownloadURL();
    } else return;
  }
}
