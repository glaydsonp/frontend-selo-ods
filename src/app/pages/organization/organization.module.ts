import { MatSelectModule } from '@angular/material/select';
import { SharedModule } from './../../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OrganizationRoutingModule } from './organization-routing.module';
import { OrganizationComponent } from './organization.component';
import { DadosCadastraisComponent } from './dados-cadastrais/dados-cadastrais.component';
import { EnderecoComponent } from './endereco/endereco.component';
import { PessoalComponent } from './pessoal/pessoal.component';
import { PerfilComponent } from './perfil/perfil.component';
import { ProjetosComponent } from './projetos/projetos.component';
import { ExpectativaComponent } from './expectativa/expectativa.component';
import { ComunicacaoComponent } from './comunicacao/comunicacao.component';

@NgModule({
  declarations: [
    OrganizationComponent,
    DadosCadastraisComponent,
    EnderecoComponent,
    PessoalComponent,
    PerfilComponent,
    ProjetosComponent,
    ExpectativaComponent,
    ComunicacaoComponent
  ],
  imports: [CommonModule, OrganizationRoutingModule, SharedModule, MatSelectModule]
})
export class OrganizationModule {}
