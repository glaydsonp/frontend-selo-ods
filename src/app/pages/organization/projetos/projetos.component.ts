import { AuthService } from './../../../services/auth.service';
import { IUserRole } from './../../../models/user';
import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { FormGroup } from '@angular/forms';
import { OrganizationCurrentFormEnum } from 'src/app/models/current-form.enum';
import { OrganizationService } from 'src/app/services/organization.service';
import { SessionService } from 'src/app/services/session.service';

@Component({
  selector: 'app-projetos',
  templateUrl: './projetos.component.html',
  styleUrls: ['./projetos.component.scss']
})
export class ProjetosComponent implements OnInit {
  form = new FormGroup({});
  updatingForm = new FormGroup({});

  userRoles = IUserRole

  selosOds: { id: string; url: string }[] = [];

  projetosSociais = [
    'Mensalmente',
    'Periodicamente',
    'Eventualmente',
    'Continuamente'
  ];

  finalidades = [
    'Ações de voluntariado',
    'Ações internas entre colaboradores',
    'Arrecadação de recursos para causas',
    'Atendimento direto, intervenções',
    'Consultorias, parcerias, apoio logístico à organizações',
    'Doação, patrocínio, apoio financeiro',
    'Estímulo ao Empreendedorismo Social',
    'Mobilização, mutirão, cooperação',
    'Palestras, oficinas, workshops',
    'Participação ou promoção de campanhas temáticas e conscientização',
    'Promoção de atividades físicas, culturais, interação com o meio ambiente',
    'Outros'
  ];

  setores = ['Público', 'Privado', 'SCO'];

  constructor(
    private _organizationService: OrganizationService,
    private _db: AngularFirestore,
    public sessionService: SessionService,
    public authService: AuthService
  ) {}

  ngOnInit(): void {
    this.buildForm();
    this.getSelosOds();

    this._organizationService.getForm().subscribe((val) => {
      this.form.patchValue({ ...val.value });
    });

    this.form.valueChanges.subscribe((val) => {
      if (val) {
        this.updatingForm.patchValue(val.value);

        this._organizationService.setUpdatingForm(
          this.form,
          OrganizationCurrentFormEnum.Projetos
        );
      }
    });

    this.form
      .get('projetos')
      ?.get('projetosComColaboradoresOds')
      ?.valueChanges.subscribe((val) => {
        const ods = val.map((el: string, index: number) => ({
          id: this.selosOds[index].id,
          checked: val[index]
        }));

        const filteredOds = ods.filter((el: any) => el.checked);

        this._organizationService.updateSelos(filteredOds);
      });

    this.form
      .get('projetos')
      ?.get('projetosComunidadeLocalOds')
      ?.valueChanges.subscribe((val) => {
        const ods = val.map((el: string, index: number) => ({
          id: this.selosOds[index].id,
          checked: val[index]
        }));

        const filteredOds = ods.filter((el: any) => el.checked);

        this._organizationService.updateSelosLocal(filteredOds);
      });
  }

  buildForm(): void {
    this.form = this._organizationService.buildForm();
  }

  getSelosOds() {
    this._db
      .collection<{ id: string; url: string }>('ods')
      .get()
      .subscribe((ods) => {
        this.selosOds = ods.docs.map((el) => el.data());
      });
  }
}
