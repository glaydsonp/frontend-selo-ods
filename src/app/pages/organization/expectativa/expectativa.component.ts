import { AuthService } from './../../../services/auth.service';
import { IUserRole } from './../../../models/user';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { OrganizationCurrentFormEnum } from 'src/app/models/current-form.enum';
import { OrganizationService } from 'src/app/services/organization.service';
import { SessionService } from 'src/app/services/session.service';

@Component({
  selector: 'app-expectativa',
  templateUrl: './expectativa.component.html',
  styleUrls: ['./expectativa.component.scss']
})
export class ExpectativaComponent implements OnInit {
  form = new FormGroup({});

  userRoles = IUserRole;

  options = [
    {
      text: 'Aumentar sua rede de comunicação',
      formControl: 'aumentarRedeComunicacao',
      checked: false
    },
    {
      text: 'Conhecer outras organizações que desenvolvem projetos sociais',
      formControl: 'conhecerOutrasOrganizacoes'
    },
    {
      text: 'Divulgar o trabalho da sua organização',
      formControl: 'divulgarOTrabalhoOrganizacao'
    },
    {
      text: 'Integrar um grupo que discute desenvolvimento social local',
      formControl: 'integrarGrupoSocialLocal'
    },
    {
      text: 'Ser certificada pelo compromisso social com a sociedade',
      formControl: 'serCertificadaPeloCompromisso'
    }
  ];

  constructor(
    private _fb: FormBuilder,
    private _organizationService: OrganizationService,
    public sessionService: SessionService,
    public authService: AuthService
  ) {}

  ngOnInit(): void {
    this.buildForm();

    this._organizationService.getForm().subscribe((val) => {
      this.form.patchValue({ ...val.value });

      setTimeout(() => {
        this.setRatingInitialColor();
      }, 1);
    });

    this.form.valueChanges.subscribe((val) => {
      if (val) {
        this._organizationService.setUpdatingForm(this.form, OrganizationCurrentFormEnum.Expectativa);
      }
    });
  }

  setRatingInitialColor() {
    const colors = ['#cf0505', '#cf0505', '#e29a0f', '#e29a0f', '#609e27'];

    const values = this.form.get('expectativa')?.value;

    (
      document.getElementById(`aumentarRedeComunicacao${values.aumentarRedeComunicacao}label`) as HTMLElement
    ).style.background = colors[values.aumentarRedeComunicacao - 1];

    (
      document.getElementById(`conhecerOutrasOrganizacoes${values.conhecerOutrasOrganizacoes}label`) as HTMLElement
    ).style.background = colors[values.conhecerOutrasOrganizacoes - 1];

    (
      document.getElementById(`divulgarOTrabalhoOrganizacao${values.divulgarOTrabalhoOrganizacao}label`) as HTMLElement
    ).style.background = colors[values.divulgarOTrabalhoOrganizacao - 1];

    (
      document.getElementById(`integrarGrupoSocialLocal${values.integrarGrupoSocialLocal}label`) as HTMLElement
    ).style.background = colors[values.integrarGrupoSocialLocal - 1];

    (
      document.getElementById(`serCertificadaPeloCompromisso${values.serCertificadaPeloCompromisso}label`) as HTMLElement
    ).style.background = colors[values.serCertificadaPeloCompromisso - 1];

    // if (element1) {
    //   element1.style.background = 'black';
    // }

    console.log('values', values);
  }

  setRatingColor(event: any, color: string) {
    const id = event.target.id.substring(0, event.target.id.length - 1);

    const elements = [
      document.getElementById(`${id}1label`) as HTMLElement,
      document.getElementById(`${id}2label`) as HTMLElement,
      document.getElementById(`${id}3label`) as HTMLElement,
      document.getElementById(`${id}4label`) as HTMLElement,
      document.getElementById(`${id}5label`) as HTMLElement
    ];

    elements.forEach((el) => (el.style.background = 'none'));

    const element = document.getElementById(`${event.target.id}label`) as HTMLElement;
    // console.log(element);
    element.style.background = color;
  }

  buildForm(): void {
    this.form = this._organizationService.buildForm();
  }
}
