import { SessionService } from './../../../services/session.service';
import { UserService } from './../../../services/user.service';
import { GenericDialogComponent } from './../../../shared/generic-dialog/generic-dialog.component';
import { SnackbarType } from './../../../models/snackbar-type.enum';
import { SnackbarService } from './../../../services/snackbar.service';
import { IUserRole } from './../../../models/user';
import { AuthService } from 'src/app/services/auth.service';
import { OrganizationService } from './../../../services/organization.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { OrganizationCurrentFormEnum } from 'src/app/models/current-form.enum';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { MatDialog } from '@angular/material/dialog';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { IOrganization } from 'src/app/models/organization';

@Component({
  selector: 'app-dados-cadastrais',
  templateUrl: './dados-cadastrais.component.html',
  styleUrls: ['./dados-cadastrais.component.scss']
})
export class DadosCadastraisComponent implements OnInit {
  form = new FormGroup({});

  setores: string[] = ['Público', 'Privado', 'Terceiro Setor/Sociedade Civil'];

  abrangencias: string[] = ['Municipal', 'Regional', 'Estadual', 'Federal'];

  userRoles = IUserRole;

  constructor(
    private _organizationService: OrganizationService,    public authService: AuthService,

    private _snackbarService: SnackbarService,
    private dialog: MatDialog,
    private _db: AngularFirestore,
    public sessionService: SessionService
  ) {}

  ngOnInit(): void {
    this.buildForm();

    this._organizationService.getForm().subscribe((val) => {
      const userRole = this.authService.getUserRole();

      if (val?.value?.dadosCadastrais?.cnpj) {
        this.form.patchValue({ ...val.value });
      }

      this.form
        .get('dadosCadastrais')
        ?.get('cnpj')
        ?.valueChanges.pipe(debounceTime(1500), distinctUntilChanged())
        .subscribe((value: string) => {
          if (value.length === 14 && userRole !== this.userRoles.Adm) {
            this._organizationService
              .checkOrganizationCnpj(value)
              .then((res) => {
                const orgId = this.authService.getOrganizationId();

                if (
                  res.cnpjExists &&
                  res.organizationId !== orgId &&
                  this.authService.getUserRole() !== this.userRoles.Curador
                ) {
                  this._snackbarService.openSnackBar(
                    'CNPJ já existe!',
                    SnackbarType.Danger
                  );

                  this.form.get('dadosCadastrais')?.get('cnpj')?.setValue('');
                } else if (
                  res.cnpjExists &&
                  res.organizationId !== orgId &&
                  this.authService.getUserRole() === this.userRoles.Curador
                ) {
                  const dialogRef = this.dialog.open(GenericDialogComponent, {
                    data: {
                      title: 'CNPJ Existente',
                      message:
                        'Este CNPJ já esta vinculado a uma instituição, deseja utilizar as mesmas informações ou cadastrar uma nova insituição?',
                      confirm: 'Utilizar informações',
                      dismiss: 'Cadastrar nova'
                    }
                  });

                  dialogRef.afterClosed().subscribe(async (dialogRes) => {
                    const { userId } = this.authService.getCurrentUser();

                    const organizationId =
                      this.sessionService.getOrganizationId();

                    if (dialogRes) {
                      await this._db
                        .collection('organizations')
                        .doc(organizationId)
                        .delete();

                      await this._db.collection('users').doc(userId).update({
                        organizationId: res.organizationId,
                        curatorRef: true
                      });

                      this.sessionService.setSessionCustomItem(
                        'curator_ref',
                        true
                      );

                      this.sessionService.setSessionCustomItem(
                        'organization_id',
                        res.organizationId
                      );

                      // this.getExistOrganization(organizationId);
                      window.location.reload();
                    } else {
                      this.dialog.closeAll();

                      this.form
                        .get('dadosCadastrais')
                        ?.get('cnpj')
                        ?.setValue('');

                      this._db
                        .collection('organizations')
                        .add({ owner: userId })
                        .then(async (added) => {
                          added.update({ id: added.id });

                          this.sessionService.setSessionCustomItem(
                            'organization_id',
                            added.id
                          );

                          await this._db
                            .collection('users')
                            .doc(userId)
                            .update({
                              organizationId: added.id,
                              curatorRef: false
                            });

                          this.sessionService.setSessionCustomItem(
                            'curator_ref',
                            false
                          );
                        })
                        .catch((err) => {
                          this._snackbarService.openSnackBar(
                            err.message,
                            SnackbarType.Danger
                          );
                        });
                    }
                  });
                }
              });
          }
        });
    });

    this.form.valueChanges.subscribe((val) => {
      if (val) {
        this._organizationService.setUpdatingForm(
          this.form,
          OrganizationCurrentFormEnum.DadosCadastrais
        );
      }
    });
  }

  buildForm(): void {
    this.form = this._organizationService.buildForm();
  }

  // getExistOrganization(organizationId: string) {
  //   this.form = this._organizationService.buildForm();

  //   const orgRef = this._db
  //     .collection('organizations')
  //     .doc<IOrganization>(organizationId)
  //     .get();

  //   orgRef.subscribe((res) => {
  //     const org = res.data() as IOrganization;
  //     this.form.patchValue(org);
  //     this._organizationService.initializeForm(this.form);
  //   });
  // }
}
