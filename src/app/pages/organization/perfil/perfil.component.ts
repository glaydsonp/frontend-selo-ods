import { AuthService } from './../../../services/auth.service';
import { IUserRole } from './../../../models/user';
import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { OrganizationCurrentFormEnum } from 'src/app/models/current-form.enum';
import { OrganizationService } from 'src/app/services/organization.service';
import { SessionService } from 'src/app/services/session.service';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.scss']
})
export class PerfilComponent implements OnInit {
  form = new FormGroup({});

  userRoles = IUserRole;

  theRankingLink =
    'https://www.timeshighereducation.com/world-university-rankings';

  areasAtuacao: string[] = ['Humanas', 'Biológicas', 'Exatas', 'Tecnológicas'];

  colaboradores: string[] = [
    '1 a 10',
    '11 a 20',
    '21 a 50',
    '51 a 100',
    '101 a 500',
    'Mais de 500'
  ];

  constructor(
    private _organizationService: OrganizationService,
    public sessionService: SessionService,
    public authService: AuthService
  ) {}

  ngOnInit(): void {
    this.buildForm();

    this._organizationService.getForm().subscribe((val) => {
      this.form.patchValue({ ...val.value });
    });

    this.form.valueChanges.subscribe((val) => {
      if (val) {
        this._organizationService.setUpdatingForm(
          this.form,
          OrganizationCurrentFormEnum.DadosCadastrais
        );
      }
    });
  }

  buildForm(): void {
    this.form = this._organizationService.buildForm();
  }
}
