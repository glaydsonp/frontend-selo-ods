import { AngularFirestore } from '@angular/fire/compat/firestore';
import { AuthService } from './../../../services/auth.service';
import { IUser, IUserRole } from './../../../models/user';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { OrganizationCurrentFormEnum } from 'src/app/models/current-form.enum';
import { OrganizationService } from 'src/app/services/organization.service';
import { SessionService } from 'src/app/services/session.service';

@Component({
  selector: 'app-pessoal',
  templateUrl: './pessoal.component.html',
  styleUrls: ['./pessoal.component.scss']
})
export class PessoalComponent implements OnInit {
  form = new FormGroup({});

  userRoles = IUserRole;

  constructor(
    private _fb: FormBuilder,
    private _db: AngularFirestore,
    private _organizationService: OrganizationService,
    public sessionService: SessionService,
    public authService: AuthService
  ) {}

  ngOnInit(): void {
    this.buildForm();

    this._organizationService.getForm().subscribe((val) => {
      this.form.patchValue({ ...val.value });

      const action = this.form
        .get('pessoal')
        ?.get('representanteDaPagina')
        ?.get('usarDadosPreCadastro')?.value;

      this.enableDisableRepresentanteDaPagina(action);
    });

    this.form.valueChanges.subscribe((val) => {
      if (val) {
        this._organizationService.setUpdatingForm(
          this.form,
          OrganizationCurrentFormEnum.Pessoal
        );
      }
    });

    this.form
      .get('pessoal')
      ?.get('representanteDaPagina')
      ?.get('usarDadosPreCadastro')
      ?.valueChanges.subscribe((val) => {
        console.log('val', val);
        if (val) {
          this.enableDisableRepresentanteDaPagina(true);
        } else if (!val) {
          this.enableDisableRepresentanteDaPagina(false);
        }
      });
  }

  async enableDisableRepresentanteDaPagina(action: boolean) {
    const id = this.authService.getCurrentUser().userId;

    const userRef = await this._db
      .collection('users')
      .doc(id)
      .get()
      .toPromise();

    const user = userRef.data() as IUser;

    const data = {
      nomeCompleto: user.nome,
      ocupacaoInstituicao: user.cargo,
      emailContato: user.email,
      emailOpcional: '',
      telefone: user.telefone,
      whatsapp: user.whatsapp
    };

    const emptyData = {
      nomeCompleto: '',
      ocupacaoInstituicao: '',
      emailContato: '',
      emailOpcional: '',
      telefone: '',
      whatsapp: '',
      cpf: '',
    };

    if (action) {
      this.form
        .get('pessoal')
        ?.get('representanteDaPagina')
        ?.get('nomeCompleto')
        ?.disable();
      this.form
        .get('pessoal')
        ?.get('representanteDaPagina')
        ?.get('cpf')
        ?.disable();
      this.form
        .get('pessoal')
        ?.get('representanteDaPagina')
        ?.get('ocupacaoInstituicao')
        ?.disable();
      this.form
        .get('pessoal')
        ?.get('representanteDaPagina')
        ?.get('telefone')
        ?.disable();
      this.form
        .get('pessoal')
        ?.get('representanteDaPagina')
        ?.get('whatsapp')
        ?.disable();
      this.form
        .get('pessoal')
        ?.get('representanteDaPagina')
        ?.get('emailContato')
        ?.disable();
      this.form
        .get('pessoal')
        ?.get('representanteDaPagina')
        ?.get('emailOpcional')
        ?.disable();

      this.form
        .get('pessoal')
        ?.get('representanteDaPagina')
        ?.patchValue({ ...data });
    }

    if (!action) {
      this.form
        .get('pessoal')
        ?.get('representanteDaPagina')
        ?.get('nomeCompleto')
        ?.enable();
      this.form
        .get('pessoal')
        ?.get('representanteDaPagina')
        ?.get('cpf')
        ?.enable();
      this.form
        .get('pessoal')
        ?.get('representanteDaPagina')
        ?.get('ocupacaoInstituicao')
        ?.enable();
      this.form
        .get('pessoal')
        ?.get('representanteDaPagina')
        ?.get('telefone')
        ?.enable();
      this.form
        .get('pessoal')
        ?.get('representanteDaPagina')
        ?.get('whatsapp')
        ?.enable();
      this.form
        .get('pessoal')
        ?.get('representanteDaPagina')
        ?.get('emailContato')
        ?.enable();
      this.form
        .get('pessoal')
        ?.get('representanteDaPagina')
        ?.get('emailOpcional')
        ?.enable();

      this.form
        .get('pessoal')
        ?.get('representanteDaPagina')
        ?.patchValue({ ...emptyData });
    }
  }

  buildForm(): void {
    this.form = this._organizationService.buildForm();
  }
}
