import { IUserRole } from './../../../models/user';
import { AuthService } from './../../../services/auth.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { OrganizationService } from 'src/app/services/organization.service';
import { OrganizationCurrentFormEnum } from 'src/app/models/current-form.enum';
import { SessionService } from 'src/app/services/session.service';

@Component({
  selector: 'app-endereco',
  templateUrl: './endereco.component.html',
  styleUrls: ['./endereco.component.scss']
})
export class EnderecoComponent implements OnInit {
  form = new FormGroup({});

  userRoles = IUserRole;

  states = {
    values: [
      'AC',
      'AL',
      'AP',
      'AM',
      'BA',
      'CE',
      'DF',
      'ES',
      'GO',
      'MA',
      'MT',
      'MS',
      'MG',
      'PA',
      'PB',
      'PR',
      'PE',
      'PI',
      'RJ',
      'RN',
      'RS',
      'RO',
      'RR',
      'SC',
      'SP',
      'SE',
      'TO'
    ],

    titles: [
      'Acre',
      'Alagoas',
      'Amapá',
      'Amazonas',
      'Bahia',
      'Ceará',
      'Distrito Federal',
      'Espirito Santo',
      'Goiás',
      'Maranhão',
      'Mato Grosso',
      'Mato Grosso do Sul',
      'Minas Gerais',
      'Pará',
      'Paraíba',
      'Paraná',
      'Pernambuco',
      'Piauí',
      'Rio de Janeiro',
      'Rio Grande do Norte',
      'Rio Grande do Sul',
      'Rondônia',
      'Roraima',
      'Santa Catarina',
      'São Paulo',
      'Sergipe',
      'Tocantins'
    ]
  };

  constructor(
    private _fb: FormBuilder,
    private _organizationService: OrganizationService,
    public sessionService: SessionService,
    public authService: AuthService
  ) {}

  ngOnInit(): void {
    this.buildForm();

    this._organizationService.getForm().subscribe((val) => {
      this.form.patchValue({ ...val.value });
    });

    this.form.valueChanges.subscribe((val) => {
      if (val) {
        this._organizationService.setUpdatingForm(
          this.form,
          OrganizationCurrentFormEnum.Endereco
        );
      }
    });
  }

  buildForm(): void {
    // this.form = this._fb.group({
    //   endereco: this._fb.group({
    //     cep: [null, Validators.required],
    //     longradouro: [null, Validators.required],
    //     numero: [null, Validators.required],
    //     complemento: [null],
    //     cidade: [null, Validators.required],
    //     bairro: [null, Validators.required],
    //     estado: [null, Validators.required]
    //   })
    // });
    this.form = this._organizationService.buildForm();
  }
}
