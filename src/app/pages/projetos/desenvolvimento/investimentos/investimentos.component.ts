import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ProjectCurrentFormEnum } from 'src/app/models/project-current-form.enum';
import { ProjectService } from 'src/app/services/project.service';

@Component({
  selector: 'app-investimentos',
  templateUrl: './investimentos.component.html',
  styleUrls: ['./investimentos.component.scss']
})
export class InvestimentosComponent implements OnInit {
  form: FormGroup;
  updatingForm = new FormGroup({});

  investimentos = [
    'Próprio',
    'Agência de Fomento (CNPq/FAPESP/FAPERJ/etc)',
    'Fundos de Investimento Privado',
    'Outro'
  ];

  constructor(private _projectService: ProjectService) {}

  ngOnInit() {
    this.buildForm();

    this._projectService.form.subscribe((val) => {
      this.form?.patchValue(val.value);

      if (this.form) {
        this.form.valueChanges.subscribe((val) => {
          if (val) {
            this.updatingForm.patchValue(val.value);
            this._projectService.setUpdatingForm(
              this.form,
              ProjectCurrentFormEnum.Resumo
            );
          }
        });
      }
    });
  }

  buildForm() {
    this.form = this._projectService.buildForm();
  }
}
