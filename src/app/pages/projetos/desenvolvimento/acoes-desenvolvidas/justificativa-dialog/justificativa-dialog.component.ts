import { FormBuilder, FormControl } from '@angular/forms';
import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { NovoObjetivoComponent } from '../../../cadastro-projeto/objetivos/novo-objetivo/novo-objetivo.component';

@Component({
  templateUrl: './justificativa-dialog.component.html',
  styleUrls: ['./justificativa-dialog.component.scss']
})
export class JustificativaDialogComponent implements OnInit {
  justificativa = '';

  constructor(
    private dialogRef: MatDialogRef<NovoObjetivoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: string,
    private _fb: FormBuilder
  ) {}

  ngOnInit(): void {
    if (this.data) {
      this.justificativa = this.data;
    }
  }

  close() {
    this.dialogRef.close(this.justificativa);
  }
}
