import { SnackbarService } from 'src/app/services/snackbar.service';
import { JustificativaDialogComponent } from './justificativa-dialog/justificativa-dialog.component';
import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatRadioChange } from '@angular/material/radio';
import { ProjectCurrentFormEnum } from 'src/app/models/project-current-form.enum';
import { ProjectService } from 'src/app/services/project.service';
import { SnackbarType } from 'src/app/models/snackbar-type.enum';

@Component({
  selector: 'app-acoes-desenvolvidas',
  templateUrl: './acoes-desenvolvidas.component.html',
  styleUrls: ['./acoes-desenvolvidas.component.scss']
})
export class AcoesDesenvolvidasComponent implements OnInit {
  form: FormGroup;
  updatingForm = new FormGroup({});

  objetivos = ['Objetivo 1', 'Objetivo 2', 'Objetivo 3'];
  planos = ['Plano 1', 'Plano 2', 'Plano 3', 'Plano 4'];

  constructor(
    private _projectService: ProjectService,
    private _fb: FormBuilder,
    private dialog: MatDialog,
    private _snackbarService: SnackbarService
  ) {}

  ngOnInit() {
    this.buildForm();

    this._projectService.form.subscribe((val) => {
      this.form?.patchValue(val.value);

      if (this.form) {
        this.form.valueChanges.subscribe((val) => {
          if (val) {
            this.updatingForm.patchValue(val.value);
            this._projectService.setUpdatingForm(
              this.form,
              ProjectCurrentFormEnum.Objetivos
            );
          }
        });
      }
    });
  }

  getObjetivosEspecificos() {
    const form = this.form
      ?.get('objetivos')
      ?.get('objetivosEspecificos') as FormArray;

    if (form && form.controls) {
      return form.controls;
    } else return [];
  }

  getPlanoAcao(objetivo: any) {
    const planosDeAcao = objetivo.get('planosDeAcao') as FormArray;

    if (planosDeAcao && planosDeAcao.length > 0) {
      return planosDeAcao.controls;
    } else return [];
  }

  buildForm() {
    this.form = this._projectService.buildForm();
  }

  checkOpenJustificationDialog(plano: any, val: any) {
    if (!val) {
      this.dialog
        .open(JustificativaDialogComponent, {
          data: plano?.value?.justificativa || ''
        })
        .afterClosed()
        .subscribe(
          (res) => {
            plano?.controls?.justificativa.patchValue(res);
          },
          (err) => {
            this._snackbarService.openSnackBar(
              err.message,
              SnackbarType.Danger
            );
          }
        );
    } else {
      plano?.controls?.justificativa.patchValue('');
    }
  }
}
