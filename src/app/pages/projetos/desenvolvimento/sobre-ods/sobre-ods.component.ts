import { ValidacaoOdsComponent } from './../../cadastro-projeto/validacao-ods/validacao-ods.component';
import { Component, OnInit } from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  FormArray,
  AbstractControl,
  FormControl
} from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ProjectCurrentFormEnum } from 'src/app/models/project-current-form.enum';
import { ProjectService } from 'src/app/services/project.service';

@Component({
  selector: 'app-sobre-ods',
  templateUrl: './sobre-ods.component.html',
  styleUrls: ['./sobre-ods.component.scss']
})
export class SobreOdsComponent implements OnInit {
  form: FormGroup;
  updatingForm = new FormGroup({});

  constructor(
    private _projectService: ProjectService,
    private _fb: FormBuilder,
    private dialog: MatDialog
  ) {}

  ngOnInit() {
    this.buildForm();

    this._projectService.form.subscribe((val) => {
      this.form?.patchValue(val.value);

      if (this.form) {
        this.form.valueChanges.subscribe((val) => {
          if (val) {
            this.updatingForm.patchValue(val.value);
            this._projectService.setUpdatingForm(
              this.form,
              ProjectCurrentFormEnum.Geral
            );
          }
        });
      }
    });
  }

  buildForm() {
    this.form = this._projectService.buildForm();
  }

  getSelosOdsArray() {
    const formSelos = this.form
      .get('geral')
      ?.get('finalidadesSelosOds') as FormArray;

    if (formSelos && formSelos.controls) {
      return formSelos.controls;
    } else return [];
  }

  editOds(ods: AbstractControl, i: number) {
    const odsControl = ods as FormControl;

    const modalRef = this.dialog.open(ValidacaoOdsComponent, {
      data: odsControl
    });

    modalRef.afterClosed().subscribe((result) => {
      if (result) {
        const control = this.form
          .get('geral')
          ?.get('finalidadesSelosOds') as FormArray;

        control.controls[i].patchValue(result.value);
      }
    });
  }
}
