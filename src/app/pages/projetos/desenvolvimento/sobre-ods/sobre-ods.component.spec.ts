/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { SobreOdsComponent } from './sobre-ods.component';

describe('SobreOdsComponent', () => {
  let component: SobreOdsComponent;
  let fixture: ComponentFixture<SobreOdsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SobreOdsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SobreOdsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
