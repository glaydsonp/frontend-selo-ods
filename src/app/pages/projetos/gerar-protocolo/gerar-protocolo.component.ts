import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { SnackbarService } from 'src/app/services/snackbar.service';
import { NovoObjetivoComponent } from '../cadastro-projeto/objetivos/novo-objetivo/novo-objetivo.component';

@Component({
  templateUrl: './gerar-protocolo.component.html',
  styleUrls: ['./gerar-protocolo.component.scss']
})
export class GerarProtocoloComponent implements OnInit {
  constructor(
    private _snackbarService: SnackbarService,
    private dialog: MatDialog,
    private dialogRef: MatDialogRef<NovoObjetivoComponent>,
    private router: Router
  ) {}

  ngOnInit(): void {}

  downloadProtocolo() {
    // window.open('/assets/pdf/Protocolo teste.pdf', '_blank');

    const blob = new Blob()
  }

  submit() {
    this.router.navigate(['/protocolos-e-relatorios']);

    this.dialog.closeAll();
  }

  close() {
    this.dialogRef.close(null);
  }
}
