import { Component, OnInit } from '@angular/core';
import { FormArray, FormGroup, FormBuilder } from '@angular/forms';
import { ISelo } from 'src/app/models/project';
import { ProjectCurrentFormEnum } from 'src/app/models/project-current-form.enum';
import { ProjectService } from 'src/app/services/project.service';

@Component({
  selector: 'app-geral',
  templateUrl: './geral.component.html',
  styleUrls: ['./geral.component.scss']
})
export class GeralComponent implements OnInit {
  eixosProjeto = ['Ensino', 'Pesquisa', 'Extensão', 'Gestão'];
  tiposProjeto = ['Próprio', 'Parceria', 'Terceiro'];
  finalidadesSelosOds: any = [];

  form: FormGroup;
  updatingForm = new FormGroup({});

  selosOds: ISelo[] = [];

  constructor(
    private _projectService: ProjectService,
    private _fb: FormBuilder
  ) {}

  ngOnInit() {
    this.buildForm();

    this._projectService.form.subscribe((val) => {
      const selos = val.get('geral')?.get('finalidadesSelosOds')
        ?.value as ISelo[];

      this.form?.patchValue(val.value);

      const selosFormArray = this.form
        .get('geral')
        ?.get('finalidadesSelosOds') as FormArray;

      selos?.forEach((el) => {
        if (!selosFormArray.value.some((selo: any) => selo.id === el.id)) {
          selosFormArray.push(
            this._fb.group({
              url: el.url,
              id: el.id,
              checked: el.checked,
              descricao: el.descricao,
              comoResultadoFoiPercebido: el.comoResultadoFoiPercebido,
              quemBeneficiouResultado: el.quemBeneficiouResultado,
              contextoQueAconteceu: el.contextoQueAconteceu,
              resultadoPercebido: el.resultadoPercebido,
              nome: el.nome
            })
          );
        }
      });

      if (this.form) {
        this.form.valueChanges.subscribe((val) => {
          if (val) {
            this.updatingForm.patchValue(val.value);
            this._projectService.setUpdatingForm(
              this.form,
              ProjectCurrentFormEnum.Geral
            );
          }
        });
      }
    });
  }

  buildForm() {
    this.form = this._projectService.buildForm();
  }

  getSelosOdsArray() {
    const formSelos = this.form
      .get('geral')
      ?.get('finalidadesSelosOds') as FormArray;

    if (formSelos && formSelos.controls) {
      return formSelos.controls;
    } else return [];
  }
}
