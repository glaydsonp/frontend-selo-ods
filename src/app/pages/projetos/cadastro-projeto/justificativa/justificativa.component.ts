import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ProjectCurrentFormEnum } from 'src/app/models/project-current-form.enum';
import { ProjectService } from 'src/app/services/project.service';

@Component({
  selector: 'app-justificativa',
  templateUrl: './justificativa.component.html',
  styleUrls: ['./justificativa.component.scss']
})
export class JustificativaComponent implements OnInit {
  form: FormGroup;
  updatingForm = new FormGroup({});

  constructor(private _projectService: ProjectService) {}

  ngOnInit() {
    this.buildForm();

    this._projectService.form.subscribe((val) => {
      this.form?.patchValue(val.value);

      if (this.form) {
        this.form.valueChanges.subscribe((val) => {
          if (val) {
            this.updatingForm.patchValue(val.value);
            this._projectService.setUpdatingForm(
              this.form,
              ProjectCurrentFormEnum.Justificativa
            );
          }
        });
      }
    });
  }

  buildForm() {
    this.form = this._projectService.buildForm();
  }
}
