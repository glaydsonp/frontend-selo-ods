import { Component, Inject, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { SnackbarService } from 'src/app/services/snackbar.service';
import { NovoPublicoComponent } from '../publico/novo-publico/novo-publico.component';

@Component({
  selector: 'app-validacao-ods',
  templateUrl: './validacao-ods.component.html',
  styleUrls: ['./validacao-ods.component.scss']
})
export class ValidacaoOdsComponent implements OnInit {
  form: FormGroup;

  constructor(
    private _fb: FormBuilder,
    private _snackbarService: SnackbarService,
    private dialogRef: MatDialogRef<NovoPublicoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: FormGroup
  ) {}

  ngOnInit() {
    this.buildForm();

    if (this.data) {
      this.form.patchValue(this.data.value);
    }
  }

  buildForm() {
    this.form = this._fb.group({
      comoResultadoFoiPercebido: [null],
      quemBeneficiouResultado: [null],
      contextoQueAconteceu: [null],
      resultadoPercebido: [null]
    });
  }

  cancel() {
    this.dialogRef.close(null);
  }

  save() {
    this.dialogRef.close(this.form);
  }
}
