import { Component, Inject, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { SnackbarService } from 'src/app/services/snackbar.service';

@Component({
  selector: 'app-novo-publico',
  templateUrl: './novo-publico.component.html',
  styleUrls: ['./novo-publico.component.scss']
})
export class NovoPublicoComponent implements OnInit {
  form: FormGroup;

  constructor(
    private _fb: FormBuilder,
    private _snackbarService: SnackbarService,
    private dialogRef: MatDialogRef<NovoPublicoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: FormGroup
  ) {}

  ngOnInit(): void {
    this.buildForm();

    if (this.data) {
      this.form.patchValue(this.data.value);
    }
  }

  buildForm() {
    this.form = this._fb.group({
      nomePublico: [null],
      descricaoPublico: [null],
      ambitoImpactoGerado: [null],
      tipoDeImpacto: [null],
      endereco: [null]
    });
  }

  cancel() {
    this.dialogRef.close(null);
  }

  save() {
    this.dialogRef.close(this.form);
  }
}
