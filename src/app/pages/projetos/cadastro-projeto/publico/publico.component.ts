import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, FormArray } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ProjectCurrentFormEnum } from 'src/app/models/project-current-form.enum';
import { ProjectService } from 'src/app/services/project.service';
import { NovoPublicoComponent } from './novo-publico/novo-publico.component';

@Component({
  selector: 'app-publico',
  templateUrl: './publico.component.html',
  styleUrls: ['./publico.component.scss']
})
export class PublicoComponent implements OnInit {
  form: FormGroup;
  updatingForm = new FormGroup({});

  publicos = [
    {
      descricao: 'Número de pessoas impactadas diretamente:',
      formControl: 'pessoasImpactadasDiretamente'
    },
    {
      descricao: 'Número de pessoas impactadas indiretamente:',
      formControl: 'pessoasImpactadasIndiretamente'
    },
    {
      descricao: 'Número de mulheres impactadas:',
      formControl: 'mulheresImpactadas'
    },
    {
      descricao: 'Número de pessoas com deficiência impactadas:',
      formControl: 'pessoasComDeficienciaImpactadas'
    },
    {
      descricao: 'Número de pessoas impactadas por igualdade racial: ',
      formControl: 'impactadasIgualdadeSocial'
    },
    {
      descricao: 'Número de pessoas idosas impactadas:',
      formControl: 'pessoasIdosasImpactadas'
    },
    {
      descricao: 'Número de crianças/adolescentes impactados: ',
      formControl: 'criancaAdolescentesImpactados'
    },
    {
      descricao: 'Número de pessoas LGBTQIA+ impactadas:',
      formControl: 'pessoasLGBTQIAImpactadas'
    },
    {
      descricao: 'Número de pessoas atendidas por programas contra a miséria:',
      formControl: 'pessoasAtendidasProgramasContraMiseria'
    },
    {
      descricao: 'Número de pessoas atendidas por programas de combate à fome:',
      formControl: 'pessoasAtendidasProgramasContraFome'
    },
    {
      descricao:
        'Número de pessoas atendidas por programas de combate ao câncer:',
      formControl: 'pessoasAtendidasProgramasContraCancer'
    },
    {
      descricao:
        'Número de pessoas atendidas por programas de estímulo ao esporte:',
      formControl: 'pessoasAtendidasProgramasEstimuloEsporte'
    }
  ];

  objetivos = [
    {
      nome: 'Nome do público',
      planosDeAcao: [
        'Info do pop up',
        'Info do pop up',
        'Info do pop up',
        'Info do pop up'
      ]
    },
    {
      nome: 'Nome do público',
      planosDeAcao: [
        'Info do pop up',
        'Info do pop up',
        'Info do pop up',
        'Info do pop up'
      ]
    }
  ];

  constructor(
    private _projectService: ProjectService,
    private _fb: FormBuilder,
    private dialog: MatDialog
  ) {}

  ngOnInit() {
    this.buildForm();

    this._projectService.form.subscribe((val) => {
      this.form?.patchValue(val.value);

      if (this.form) {
        this.form.valueChanges.subscribe((val) => {
          if (val) {
            this.updatingForm.patchValue(val.value);
            this._projectService.setUpdatingForm(
              this.form,
              ProjectCurrentFormEnum.Publico
            );
          }
        });
      }
    });
  }

  decrement(control: string) {
    const a = this.form.get('publico')?.get(control) as FormControl;
    const val = a.value;

    if (val > 0) {
      a.patchValue(val - 1);
    } else return;
  }

  increment(control: string) {
    const a = this.form.get('publico')?.get(control) as FormControl;
    const val = a.value;
    a.patchValue(val + 1);
  }

  buildForm() {
    this.form = this._projectService.buildForm();
  }

  addPublico() {
    const dialogRef = this.dialog.open(NovoPublicoComponent);

    dialogRef.afterClosed().subscribe((result) => {
      if (result && result.value.nomePublico && result.value.descricaoPublico) {
        const publicos = this.form
          ?.get('publico')
          ?.get('publicosAdicionados') as FormArray;

        publicos.push(result);
      }
    });
  }

  updatePublico(i: number | null) {
    const publicos = this.form
      ?.get('publico')
      ?.get('publicosAdicionados') as FormArray;

    if (typeof i === 'number') {
      const publicoBeforeUpdate = publicos.controls[i];

      const dialogRef = this.dialog.open(NovoPublicoComponent, {
        data: publicoBeforeUpdate
      });

      dialogRef.afterClosed().subscribe((result) => {
        if (
          result &&
          result.value.nomePublico &&
          result.value.descricaoPublico
        ) {
          const publico = publicos.controls[i];

          publico.patchValue(result.value);
        }
      });
    }
  }

  getPublicos() {
    const form = this.form
      ?.get('publico')
      ?.get('publicosAdicionados') as FormArray;

    // console.log('teste', this.form);

    if (form && form.controls) {
      return form.controls;
    } else return [];
  }
}
