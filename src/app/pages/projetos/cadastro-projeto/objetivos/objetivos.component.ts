import { NovoObjetivoComponent } from './novo-objetivo/novo-objetivo.component';
import { MatDialog } from '@angular/material/dialog';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormArray, FormBuilder, FormControl } from '@angular/forms';
import { ProjectCurrentFormEnum } from 'src/app/models/project-current-form.enum';
import { ProjectService } from 'src/app/services/project.service';

@Component({
  selector: 'app-objetivos',
  templateUrl: './objetivos.component.html',
  styleUrls: ['./objetivos.component.scss']
})
export class ObjetivosComponent implements OnInit {
  form: FormGroup;
  updatingForm = new FormGroup({});

  constructor(
    private _projectService: ProjectService,
    private dialog: MatDialog,
    private _fb: FormBuilder
  ) {}

  ngOnInit() {
    this.buildForm();

    this._projectService.form.subscribe((val) => {
      this.form?.patchValue(val.value);
      // console.log(this.form.value);

      if (this.form) {
        this.form.valueChanges.subscribe((val) => {
          if (val) {
            this.updatingForm.patchValue(val.value);
            this._projectService.setUpdatingForm(
              this.form,
              ProjectCurrentFormEnum.Objetivos
            );
          }
        });
      }
    });
  }

  getObjetivosEspecificos() {
    const form = this.form
      ?.get('objetivos')
      ?.get('objetivosEspecificos') as FormArray;

    // console.log('teste', this.form);

    if (form && form.controls) {
      return form.controls;
    } else return [];
  }

  getPlanoAcao(objetivo: any) {
    const planosDeAcao = objetivo.get('planosDeAcao') as FormArray;

    if (planosDeAcao && planosDeAcao.length > 0) {
      return planosDeAcao.controls;
    } else return [];
  }

  buildForm() {
    this.form = this._projectService.buildForm();
  }

  updateObjetivo(i: number | null) {
    const objetivos = this.form
      ?.get('objetivos')
      ?.get('objetivosEspecificos') as FormArray;

    if (typeof i === 'number') {
      const objetivoBeforeUpdate = objetivos.controls[i];

      const dialogRef = this.dialog.open(NovoObjetivoComponent, {
        data: objetivoBeforeUpdate
      });

      dialogRef.afterClosed().subscribe((result) => {
        if (
          result &&
          result.value.nomeObjetivo &&
          result.value.planosDeAcao.length > 0
        ) {
          const objetivo = objetivos.controls[i];
          const acoes = objetivo.get('planosDeAcao') as FormArray;

          objetivo.patchValue({ nomeObjetivo: result.value.nomeObjetivo });

          for (let i = 0; i < 5; i++) {
            const control = result.value.planosDeAcao[i];

            if (control) {
              acoes.setControl(i, new FormControl(control));
            } else {
              acoes.removeAt(i);
            }
          }
        }
      });
    }
  }

  addObjetivo(i: number | null) {
    const dialogRef = this.dialog.open(NovoObjetivoComponent);

    dialogRef.afterClosed().subscribe((result) => {
      if (
        result &&
        result.value.nomeObjetivo &&
        result.value.planosDeAcao.length > 0
      ) {
        let objetivosEspecificos = this.form
          .get('objetivos')
          ?.get('objetivosEspecificos') as FormArray;

        objetivosEspecificos.push(result);
      }
    });
  }
}
