import { SnackbarType } from './../../../../../models/snackbar-type.enum';
import { FormGroup, FormBuilder, FormArray } from '@angular/forms';
import { Component, Inject, OnInit } from '@angular/core';
import { SnackbarService } from 'src/app/services/snackbar.service';
import { MatDialogRef } from '@angular/material/dialog';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-novo-objetivo',
  templateUrl: './novo-objetivo.component.html',
  styleUrls: ['./novo-objetivo.component.scss']
})
export class NovoObjetivoComponent implements OnInit {
  form: FormGroup;

  constructor(
    private _fb: FormBuilder,
    private _snackbarService: SnackbarService,
    private dialogRef: MatDialogRef<NovoObjetivoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: FormGroup
  ) {}

  ngOnInit() {
    this.buildForm();

    if (this.data) {
      this.form.patchValue(this.data.value);
      let array = this.form.get('planosDeAcao') as FormArray;

      this.data.get('planosDeAcao')?.value.forEach((el: any) => {
        const control = this._fb.group({
          descricao: el.descricao,
          realizado: el.realizado,
          justificativa: el.justificativa
        });

        array.push(control);
      });
    }
  }

  buildForm() {
    this.form = this._fb.group({
      nomeObjetivo: [null],
      planosDeAcao: this.buildAcoes(),
    });
  }

  addAction() {
    const objetivosArray = this.form.get('planosDeAcao') as FormArray;

    if (objetivosArray.length < 4) {
      objetivosArray.push(
        this._fb.group({
          descricao: '',
          realizado: true,
          justificativa: ''
        })
      );
    } else {
      this._snackbarService.openSnackBar(
        'Número limite de planos de ação atingido.',
        SnackbarType.Danger
      );
    }
  }

  deleteAction(i: number) {
    const objetivosArray = this.form.get('planosDeAcao') as FormArray;

    objetivosArray.removeAt(i);
  }

  buildAcoes() {
    return this._fb.array([]);
  }

  getAcoes() {
    const form = this.form.get('planosDeAcao') as FormArray;

    if (form && form.controls) {
      return form.controls;
    } else return [];
  }

  cancel() {
    this.dialogRef.close(null);
  }

  save() {
    this.dialogRef.close(this.form);
  }
}
