import { NovoArtigoComponent } from './novo-artigo/novo-artigo.component';
import { IProject } from './../../../models/project';
import { NovoEventoComponent } from './novo-evento/novo-evento.component';
import { NovoVideoComponent } from './novo-video/novo-video.component';
import { ProjectService } from 'src/app/services/project.service';
import { FormArray, FormGroup } from '@angular/forms';
import { NovoAlbumComponent } from './novo-album/novo-album.component';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ComponentType } from '@angular/cdk/portal';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-evidencias',
  templateUrl: './evidencias.component.html',
  styleUrls: ['./evidencias.component.scss']
})
export class EvidenciasComponent implements OnInit {
  form: FormGroup;

  evidencias = [
    {
      titulo: 'Álbum',
      botao: 'álbum',
      component: NovoAlbumComponent,
      formGroup: 'albuns'
    },
    {
      titulo: 'Vídeos',
      botao: 'vídeos',
      component: NovoVideoComponent,
      formGroup: 'videos'
    },
    {
      titulo: 'Artigos, notícias, publicações',
      botao: 'artigos',
      component: NovoArtigoComponent,
      formGroup: 'artigos'
    },
    {
      titulo: 'Eventos',
      botao: 'eventos',
      component: NovoEventoComponent,
      formGroup: 'eventos'
    }
  ];

  constructor(
    private dialog: MatDialog,
    private _db: AngularFirestore,
    private _projectService: ProjectService,
    public sanitizer: DomSanitizer
  ) {}

  ngOnInit() {
    this.buildForm();

    this._projectService.form.subscribe((val) => {
      this.form?.patchValue(val.value);
    });
  }

  newFeature(component: any) {
    this.dialog
      .open(component)
      .afterClosed()
      .subscribe(async (result) => {
        // debugger;
        // if (result) {

        // }
        const projectId = this._projectService.projectId;

        const project = (await this._db
          .collection('projects')
          .doc<IProject>(projectId)
          .get()
          .toPromise()
          .then((el) => {
            return el.data();
          })) as IProject;

        this._projectService.setInitialInfo(project);

        this.buildForm();

        this._projectService.form.subscribe((val) => {
          this.form?.patchValue(val.value);
        });
      });
  }

  getAlbuns() {
    const form = this.form?.get('albuns') as FormArray;

    if (form && form.controls) {
      return form.controls;
    } else return [];
  }

  sanitizeUrl(url: string) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }

  getVideos() {
    const form = this.form?.get('videos') as FormArray;

    if (form && form.controls) {
      return form.controls;
    } else return [];
  }

  getArtigos() {
    const form = this.form?.get('artigos') as FormArray;

    if (form && form.controls) {
      return form.controls;
    } else return [];
  }

  buildForm() {
    this.form = this._projectService.buildForm();
  }
}
