import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { IVideo, IProject, IEvento } from 'src/app/models/project';
import { ProjectService } from 'src/app/services/project.service';
import { SnackbarService } from 'src/app/services/snackbar.service';
import { NovoPublicoComponent } from '../../cadastro-projeto/publico/novo-publico/novo-publico.component';

@Component({
  selector: 'app-novo-evento',
  templateUrl: './novo-evento.component.html',
  styleUrls: ['./novo-evento.component.scss']
})
export class NovoEventoComponent implements OnInit {
  form: FormGroup;

  constructor(
    private _fb: FormBuilder,
    private _snackbarService: SnackbarService,
    private _db: AngularFirestore,
    private _projectService: ProjectService,
    private dialogRef: MatDialogRef<NovoPublicoComponent>
  ) {}

  ngOnInit() {
    this.buildForm();
  }

  buildForm() {
    this.form = this._fb.group({
      nomeEvento: [null],
      descricao: [null],
      formato: [null],
      linkCobertura: [null],
      numeroParticipantes: [null]
    });
  }

  cancel() {
    this.dialogRef.close(null);
  }

  async save() {
    const projectId = this._projectService.projectId;

    const data: IEvento = {
      nomeEvento: this.form.value.nomeEvento,
      descricao: this.form.value.descricao,
      formato: this.form.value.formato,
      linkCobertura: this.form.value.linkCobertura,
      numeroParticipantes: this.form.value.numeroParticipantes
    };

    const projectEventosRef = await this._db
      .collection('projects')
      .doc<IProject>(projectId)
      .get()
      .toPromise();

    const projectEventos =
      (projectEventosRef.data()?.eventos as IEvento[]) || [];

    projectEventos?.push(data);

    this._db
      .collection('projects')
      .doc<IProject>(projectId)
      .update({ eventos: projectEventos });

    this.dialogRef.close(true);
  }

  async onFileChange(event: any) {
    const file = event.files[0];

    // if (file) {
    //   const organizationId = this._authService.getCurrentUser().organizationId;

    //   this.fileName = file.name;

    //   const fileExt = file.type.split('/')[1];
    //   const filePath = `images/organizations/${organizationId}/logo.${fileExt}`;
    //   const fileRef = this.storage.ref(filePath);

    //   const task = this.storage.upload(filePath, file);

    //   console.log('aaaaaaaaaaaaaaaa');

    //   task
    //     .snapshotChanges()
    //     .pipe(
    //       finalize(() => {
    //         const downloadUrl = fileRef.getDownloadURL();
    //         downloadUrl.subscribe((url) => {
    //           if (url) {
    //             this._db
    //               .collection('organizations')
    //               .doc(organizationId)
    //               .update({ logoUrl: url });
    //           }
    //         });
    //       })
    //     )
    //     .subscribe();
    //   // .toPromise();

    //   // task.percentageChanges().subscribe((val) => {
    //   //   this.uploadProgress$ = val;
    //   //   console.log(val);
    //   // });
    //   // return upload.ref.getDownloadURL();
    // } else return;
  }
}
