import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { IArtigo, IProject } from 'src/app/models/project';
import { ProjectService } from 'src/app/services/project.service';
import { SnackbarService } from 'src/app/services/snackbar.service';

@Component({
  selector: 'app-novo-artigo',
  templateUrl: './novo-artigo.component.html',
  styleUrls: ['./novo-artigo.component.scss']
})
export class NovoArtigoComponent implements OnInit {
  form: FormGroup;

  constructor(
    private _db: AngularFirestore,
    private _projectService: ProjectService,
    private _fb: FormBuilder,
    private _snackbarService: SnackbarService,
    private dialogRef: MatDialogRef<NovoArtigoComponent>
  ) {}

  ngOnInit() {
    this.buildForm();
  }

  buildForm() {
    this.form = this._fb.group({
      titulo: [null],
      linkAcesso: [null]
    });
  }

  cancel() {
    this.dialogRef.close(null);
  }

  async save() {
    const projectId = this._projectService.projectId;

    const data: IArtigo = {
      titulo: this.form.value.titulo,
      linkAcesso: this.form.value.linkAcesso
    };

    const projectArtigosRef = await this._db
      .collection('projects')
      .doc<IProject>(projectId)
      .get()
      .toPromise();

    const projectArtigos =
      (projectArtigosRef.data()?.artigos as IArtigo[]) || [];

    projectArtigos?.push(data);

    this._db
      .collection('projects')
      .doc<IProject>(projectId)
      .update({ artigos: projectArtigos });

    this.dialogRef.close(true);
  }
}
