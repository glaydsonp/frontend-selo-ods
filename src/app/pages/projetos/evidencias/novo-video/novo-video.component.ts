import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { IProject, IVideo } from 'src/app/models/project';
import { ProjectService } from 'src/app/services/project.service';
import { SnackbarService } from 'src/app/services/snackbar.service';
import { NovoPublicoComponent } from '../../cadastro-projeto/publico/novo-publico/novo-publico.component';

@Component({
  selector: 'app-novo-video',
  templateUrl: './novo-video.component.html',
  styleUrls: ['./novo-video.component.scss']
})
export class NovoVideoComponent implements OnInit {
  form: FormGroup;

  constructor(
    private _db: AngularFirestore,
    private _projectService: ProjectService,
    private _fb: FormBuilder,
    private _snackbarService: SnackbarService,
    private dialogRef: MatDialogRef<NovoPublicoComponent>
  ) {}

  ngOnInit() {
    this.buildForm();
  }

  buildForm() {
    this.form = this._fb.group({
      nomeVideo: [null],
      legenda: [null],
      linkVideo: [null]
    });
  }

  cancel() {
    this.dialogRef.close(null);
  }

  async save() {
    const projectId = this._projectService.projectId;

    const data: IVideo = {
      nomeVideo: this.form.value.nomeVideo,
      legenda: this.form.value.legenda,
      linkVideo: this.form.value.linkVideo
    };

    const projectVideossRef = await this._db
      .collection('projects')
      .doc<IProject>(projectId)
      .get()
      .toPromise();

    const projectVideos = (projectVideossRef.data()?.videos as IVideo[]) || [];

    projectVideos?.push(data);

    this._db
      .collection('projects')
      .doc<IProject>(projectId)
      .update({ videos: projectVideos });

    this.dialogRef.close(true);
  }

  // async onFileChange(event: any) {
  //   const file = event.files[0];
  // }
}
