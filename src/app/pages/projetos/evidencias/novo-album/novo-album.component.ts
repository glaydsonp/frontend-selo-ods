import { ProjectService } from 'src/app/services/project.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { SnackbarService } from 'src/app/services/snackbar.service';
import { NovoPublicoComponent } from '../../cadastro-projeto/publico/novo-publico/novo-publico.component';
import { AngularFireStorage } from '@angular/fire/compat/storage';
import { finalize } from 'rxjs/operators';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { IAlbum, IProject } from 'src/app/models/project';

@Component({
  selector: 'app-novo-album',
  templateUrl: './novo-album.component.html',
  styleUrls: ['./novo-album.component.scss']
})
export class NovoAlbumComponent implements OnInit {
  form: FormGroup;

  images: any[] = [];
  imagesSourceToStorage: any[] = [];
  imagesUrl: string[] = [];

  constructor(
    private _fb: FormBuilder,
    private _snackbarService: SnackbarService,
    private storage: AngularFireStorage,
    private _db: AngularFirestore,
    private _projectService: ProjectService,
    private dialogRef: MatDialogRef<NovoPublicoComponent>
  ) {}

  ngOnInit() {
    this.buildForm();
  }

  buildForm() {
    this.form = this._fb.group({
      nomeAlbum: [null],
      legenda: [null]
    });
  }

  cancel() {
    this.imagesUrl.forEach((el) => {
      this.storage.refFromURL(el).delete();
    });

    this.dialogRef.close(null);
  }

  async save() {
    const projectId = this._projectService.projectId;

    const data: IAlbum = {
      nomeAlbum: this.form.value.nomeAlbum,
      legenda: this.form.value.legenda,
      imagesUrl: this.imagesUrl
    };

    const projectAlbunsRef = await this._db
      .collection('projects')
      .doc<IProject>(projectId)
      .get()
      .toPromise();

    const projectAlbuns = (projectAlbunsRef.data()?.albuns as IAlbum[]) || [];

    projectAlbuns?.push(data);

    this._db
      .collection('projects')
      .doc<IProject>(projectId)
      .update({ albuns: projectAlbuns });

    this.dialogRef.close(true);
  }

  async onFileChange(event: any) {
    const file = event.files[0];

    const fr = new FileReader();
    fr.readAsDataURL(file);

    fr.onload = (e) => {
      const img = e.target?.result;
      this.images.push(img);
      this.imagesSourceToStorage.push(file);

      const projectId = this._projectService.projectId;

      const fileName = file.name.split('.')[0];
      const fileExt = file.type.split('/')[1];
      const filePath = `images/projects/${projectId}/${fileName}.${fileExt}`;

      const fileRef = this.storage.ref(filePath);

      const task = this.storage.upload(filePath, file);

      task
        .snapshotChanges()
        .pipe(
          finalize(() => {
            const downloadUrl = fileRef.getDownloadURL();
            downloadUrl.subscribe((url) => {
              if (url) {
                this.imagesUrl.push(url);
              }
            });
          })
        )
        .subscribe();
    };

    // if (file) {
    //   const organizationId = this._authService.getCurrentUser().organizationId;

    //   this.fileName = file.name;

    //   const fileExt = file.type.split('/')[1];
    //   const filePath = `images/organizations/${organizationId}/logo.${fileExt}`;
    //   const fileRef = this.storage.ref(filePath);

    //   const task = this.storage.upload(filePath, file);

    //   console.log('aaaaaaaaaaaaaaaa');

    //   task
    //     .snapshotChanges()
    //     .pipe(
    //       finalize(() => {
    //         const downloadUrl = fileRef.getDownloadURL();
    //         downloadUrl.subscribe((url) => {
    //           if (url) {
    //             this._db
    //               .collection('organizations')
    //               .doc(organizationId)
    //               .update({ logoUrl: url });
    //           }
    //         });
    //       })
    //     )
    //     .subscribe();
    //   // .toPromise();

    //   // task.percentageChanges().subscribe((val) => {
    //   //   this.uploadProgress$ = val;
    //   //   console.log(val);
    //   // });
    //   // return upload.ref.getDownloadURL();
    // } else return;
  }
}
