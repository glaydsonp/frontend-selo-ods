import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProjetosComponent } from './projetos.component';
import { NovoProjetoComponent } from './novo-projeto/novo-projeto.component';

const routes: Routes = [
  { path: '', component: ProjetosComponent },
  { path: ':id', component: NovoProjetoComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProjetosRoutingModule {}
