import { NovoArtigoComponent } from './evidencias/novo-artigo/novo-artigo.component';
import { NovoEventoComponent } from './evidencias/novo-evento/novo-evento.component';
import { NovoVideoComponent } from './evidencias/novo-video/novo-video.component';
import { NovoAlbumComponent } from './evidencias/novo-album/novo-album.component';
import { ValidacaoOdsComponent } from './cadastro-projeto/validacao-ods/validacao-ods.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProjetosComponent } from './projetos.component';
import { ProjetosRoutingModule } from './projetos-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { EstudoRealidadeComponent } from './cadastro-projeto/estudo-realidade/estudo-realidade.component';
import { GeralComponent } from './cadastro-projeto/geral/geral.component';
import { JustificativaComponent } from './cadastro-projeto/justificativa/justificativa.component';
import { ObjetivosComponent } from './cadastro-projeto/objetivos/objetivos.component';
import { PublicoComponent } from './cadastro-projeto/publico/publico.component';
import { ResumoComponent } from './cadastro-projeto/resumo/resumo.component';
import { AcoesDesenvolvidasComponent } from './desenvolvimento/acoes-desenvolvidas/acoes-desenvolvidas.component';
import { InvestimentosComponent } from './desenvolvimento/investimentos/investimentos.component';
import { SobreOdsComponent } from './desenvolvimento/sobre-ods/sobre-ods.component';
import { EvidenciasComponent } from './evidencias/evidencias.component';
import { NovoProjetoComponent } from './novo-projeto/novo-projeto.component';
import { MatSelectModule } from '@angular/material/select';
import { NovoObjetivoComponent } from './cadastro-projeto/objetivos/novo-objetivo/novo-objetivo.component';
import { NovoPublicoComponent } from './cadastro-projeto/publico/novo-publico/novo-publico.component';
import { JustificativaDialogComponent } from './desenvolvimento/acoes-desenvolvidas/justificativa-dialog/justificativa-dialog.component';
import { GerarProtocoloComponent } from './gerar-protocolo/gerar-protocolo.component';

@NgModule({
  imports: [CommonModule, ProjetosRoutingModule, SharedModule, MatSelectModule],
  declarations: [
    ProjetosComponent,
    EstudoRealidadeComponent,
    GeralComponent,
    JustificativaComponent,
    ObjetivosComponent,
    PublicoComponent,
    ResumoComponent,
    AcoesDesenvolvidasComponent,
    InvestimentosComponent,
    SobreOdsComponent,
    EvidenciasComponent,
    NovoProjetoComponent,
    NovoObjetivoComponent,
    NovoPublicoComponent,
    ValidacaoOdsComponent,
    NovoAlbumComponent,
    NovoVideoComponent,
    NovoEventoComponent,
    NovoArtigoComponent,
    JustificativaDialogComponent,
    GerarProtocoloComponent
  ]
})
export class ProjetosModule {}
