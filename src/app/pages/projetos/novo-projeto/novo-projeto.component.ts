import { IProject } from './../../../models/project';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { IStep } from 'src/app/models/register';
import { StepperActionEnum } from 'src/app/models/stepper-action.enum';
import { ProjectService } from 'src/app/services/project.service';
import { ConfirmationDialogComponent } from 'src/app/shared/confirmation-dialog/confirmation-dialog.component';
import { FormGroup } from '@angular/forms';
import { IOrganization } from 'src/app/models/organization';
import { AngularFirestore } from '@angular/fire/compat/firestore';

@Component({
  selector: 'app-novo-projeto',
  templateUrl: './novo-projeto.component.html',
  styleUrls: ['./novo-projeto.component.scss']
})
export class NovoProjetoComponent implements OnInit {
  step = 0;
  currentSubStep = 0;
  form = new FormGroup({});
  projectId = '';
  StepperAction = StepperActionEnum;

  steps: IStep[] = [
    {
      title: 'Cadastro do Projeto',
      step: 0,
      substeps: [
        {
          title: 'Geral',
          substep: 0
        },
        {
          title: 'Estudo da Realidade',
          substep: 1
        },
        {
          title: 'Justificativa',
          substep: 2
        },
        {
          title: 'Objetivos',
          substep: 3
        },
        {
          title: 'Público',
          substep: 4
        },
        {
          title: 'Resumo',
          substep: 5
        }
      ]
    },
    {
      title: 'Desenvolvimento',
      step: 1,
      substeps: [
        {
          title: 'Ações Desenvolvidas',
          substep: 0
        },
        {
          title: 'Investimentos',
          substep: 1
        },
        {
          title: 'Sobre os ODS',
          substep: 2
        }
      ]
    },
    {
      title: 'Evidências',
      step: 2,
      substeps: []
    }
  ];

  constructor(
    public dialog: MatDialog,
    private _projectService: ProjectService,
    private _activatedRoute: ActivatedRoute,
    private _db: AngularFirestore
  ) {
    _activatedRoute.params.subscribe(async (res) => {
      this.projectId = res.id;

      const project = (await this._db
        .collection('projects')
        .doc<IProject>(this.projectId)
        .get()
        .toPromise()
        .then((el) => {
          return el.data();
        })) as IProject;

      // const finalidadesSelosOds = project?.geral.finalidadesSelosOds;

      _projectService.setInitialInfo(project);
      // _projectService.setInitialInfo(this.projectId, finalidadesSelosOds);
    });
  }

  ngOnInit() {
    this.buildForm();
  }

  changeStep(step: number): void {
    if (step !== this.step) {
      this.step = step;
      this.currentSubStep = 0;
    }
  }

  changeSubStep(substep: number): void {
    if (substep !== this.currentSubStep) {
      this.currentSubStep = substep;
    }
  }

  previousOrNextStep(action: number) {
    if (action === StepperActionEnum.Next) {
      if (this.step < this.steps.length && this.currentSubStep < this.steps[this.step].substeps.length) {
        this.currentSubStep++;
      }

      if (this.step < this.steps.length && this.currentSubStep === this.steps[this.step].substeps.length) {
        this.step++;
        this.currentSubStep = 0;
      }
    }

    if (action === StepperActionEnum.Previous) {
      if (this.step > 0 && this.currentSubStep > 0) {
        this.currentSubStep--;
        return;
      }

      if (this.step === 0 && this.currentSubStep > 0) {
        this.currentSubStep--;
        return;
      }

      if (this.step > 0 && this.currentSubStep === 0) {
        this.step--;
        this.currentSubStep = this.steps[this.step].substeps.length - 1;
        return;
      }
    }
  }

  updateProject(action?: number) {
    this._projectService.updateProject(this.projectId).then((res) => {
      if (res && res.success) {
        if (action) {
          this.previousOrNextStep(action);
          this.buildForm();
        }
      }
    });
  }

  submit(): void {
    this.updateProject();
    this.dialog.open(ConfirmationDialogComponent, { data: 'projetos' });
    // const dialogRef = this.dialog.open(ConfirmationDialogComponent);

    // dialogRef.afterClosed().subscribe((result) => {
    //   console.log(`Dialog result: ${result}`);
    // });
  }

  buildForm() {
    const projectRef = this._db.collection('projects').doc<IProject>(this.projectId).get();

    projectRef.subscribe((res) => {
      this.form = this._projectService.buildForm();

      const project = res.data() as IProject;
      this.form.patchValue(project);

      // console.log('project', project);
      this._projectService.setInitialInfo(project);
      this._projectService.initializeForm(this.form);
    });
  }
}
