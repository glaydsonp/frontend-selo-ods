import { GerarProtocoloComponent } from './gerar-protocolo/gerar-protocolo.component';
import { MatDialog } from '@angular/material/dialog';
import { AuthService } from '../../services/auth.service';
import { IProject } from '../../models/project';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ProjectService } from 'src/app/services/project.service';
import { IUserRole } from 'src/app/models/user';
import { ProjectStatusEnum } from 'src/app/models/project-status.enum';

@Component({
  selector: 'app-projetos',
  templateUrl: './projetos.component.html',
  styleUrls: ['./projetos.component.scss']
})
export class ProjetosComponent implements OnInit {
  avaliacoes = [
    { desc: 'Todos', value: '' },
    { desc: 'Sim', value: 0 },
    { desc: 'Não', value: 1 }
  ];

  ciclos = [
    { desc: 'Todos', value: '' },
    { desc: 'Ciclo 2021', value: 2021 },
    { desc: 'Ciclo 2022', value: 2022 },
    { desc: 'Ciclo 2023', value: 2023 }
  ];

  projetos: IProject[] = [];

  filtroProjetos = new FormGroup({});

  ProjectStatusEnum = ProjectStatusEnum;
  IUserRole = IUserRole;

  constructor(
    public projectService: ProjectService,
    public authService: AuthService,
    private dialog: MatDialog,
    private _fb: FormBuilder
  ) {}

  ngOnInit() {
    this.buildFiltros();
    this.getProjects();
  }

  buildFiltros() {
    this.filtroProjetos = this._fb.group({
      nome: null,
      avaliacao: null,
      ciclo: null
    });
  }

  getProjects() {
    const isCurator = this.authService.getUserRole() === IUserRole.Curador;

    this.projectService.getAll(isCurator).subscribe((res) => {
      this.projetos = res;
    });
  }

  filtrar() {
    const filterForm = this.filtroProjetos.value;

    const isCurator = this.authService.getUserRole() === IUserRole.Curador;

    this.projectService.getAll(isCurator).subscribe((res) => {
      this.projetos = res.filter((el) => {
        if (!filterForm?.nome && !filterForm?.avaliacao) {
          return true;
        }

        if (
          el.geral.nome?.toLowerCase().includes(filterForm?.nome?.toLowerCase()) &&
          el?.status === filterForm?.avaliacao
        ) {
          return true;
        }

        if (el?.geral?.nome?.toLowerCase().includes(filterForm?.nome?.toLowerCase()) && !filterForm?.avaliacao) {
          return true;
        }

        return false;
      });
    });
  }

  resetFilter() {
    this.filtroProjetos.reset();
  }

  openProtocoloDialog() {
    const dialogRef = this.dialog.open(GerarProtocoloComponent);

    dialogRef.afterClosed().subscribe((res) => {});
  }
}
