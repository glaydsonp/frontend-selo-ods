import { OrganizationService } from './../../services/organization.service';
import { IOrganization } from 'src/app/models/organization';
import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { IUserRole } from 'src/app/models/user';
import { AuthService } from 'src/app/services/auth.service';
import { ProjectService } from 'src/app/services/project.service';

@Component({
  selector: 'app-organizations',
  templateUrl: './organizations.component.html',
  styleUrls: ['./organizations.component.scss']
})
export class OrganizationsComponent implements OnInit {
  organizations: IOrganization[] = [];

  ciclos = [
    { desc: 'Todos', value: '' },
    { desc: '2021', value: 2021 },
    { desc: '2022', value: 2022 },
    { desc: '2023', value: 2023 }
  ];

  filtroOrganizacoes = new FormGroup({});

  IUserRole = IUserRole;

  constructor(
    public _organizationService: OrganizationService,
    private _authService: AuthService,
    private _db: AngularFirestore,
    private dialog: MatDialog,
    private _fb: FormBuilder
  ) {}

  ngOnInit() {
    this.buildFiltros();
    this.getOrganizations();
  }

  buildFiltros() {
    this.filtroOrganizacoes = this._fb.group({
      nome: null,
      ciclo: null
    });
  }

  async getOrganizations() {
    this._organizationService.getAll().subscribe((res) => {
      this.organizations = res;

      res.forEach(async (el) => {
        const form = this._organizationService.buildForm();

        form.patchValue(el);

        // const orgRef = await this._db.collection('organizations').doc(el.)

        const dadosCadastraisValid = form.get('dadosCadastrais')?.valid;
        const enderecoValid = form.get('endereco')?.valid;
        const pessoalValid = form.get('pessoal')?.valid;
        const perfilValid = form.get('perfil')?.valid;
        const projetosValid = form.get('projetos')?.valid;
        const expectativaValid = form.get('expectativa')?.valid;
        const comunicacaoValid = form.get('comunicacao')?.valid;

        if (
          dadosCadastraisValid &&
          enderecoValid &&
          pessoalValid &&
          perfilValid &&
          projetosValid &&
          expectativaValid &&
          comunicacaoValid
        ) {
          el.valid = true;
        }
      });
    });
  }

  filtrar() {
    const filterForm = this.filtroOrganizacoes.value;

    const isCurator = this._authService.getUserRole() === IUserRole.Curador;

    this._organizationService.getAll().subscribe((res) => {
      this.organizations = res.filter((el) => {
        if (!filterForm?.nome && !filterForm?.ciclo) {
          return true;
        }

        if (el?.dadosCadastrais?.nomeInstituicao.toLowerCase().includes(filterForm?.nome?.toLowerCase())) {
          return true;
        }

        // if (
        //   el?.dadosCadastrais?.nomeInstituicao.toLowerCase().includes(filterForm?.nome?.toLowerCase()) &&
        //   el?.status === filterForm?.avaliacao
        // ) {
        //   return true;
        // }

        // if (el?.geral?.nome?.toLowerCase().includes(filterForm?.nome?.toLowerCase()) && !filterForm?.avaliacao) {
        //   return true;
        // }

        return false;
      });
    });
  }

  resetFilter() {
    this.filtroOrganizacoes.reset();
  }
}
