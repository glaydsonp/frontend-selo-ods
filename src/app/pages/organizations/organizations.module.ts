import { MatSelectModule } from '@angular/material/select';
import { SharedModule } from './../../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OrganizationsRoutingModule } from './organizations-routing.module';
import { OrganizationsComponent } from './organizations.component';


@NgModule({
  declarations: [
    OrganizationsComponent
  ],
  imports: [
    CommonModule,
    OrganizationsRoutingModule,
    SharedModule,
    MatSelectModule
  ]
})
export class OrganizationsModule { }
