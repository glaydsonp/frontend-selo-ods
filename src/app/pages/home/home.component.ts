import { AuthService } from 'src/app/services/auth.service';
import { Component, OnInit } from '@angular/core';
import { IUserRole } from 'src/app/models/user';

interface IStep {
  title: string;
  status: number;
}

interface IAdmPermission {
  img: string;
  subtitle: string;
  link: string;
}

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  IUserRole = IUserRole;

  name = 'Lucas';

  steps: IStep[] = [
    {
      title: 'Pré-cadastro',
      status: 1
    },
    {
      title: 'Ler e aceitar o Regulamento do Selo Social',
      status: 1
    },
    // {
    //   title: 'Cadastro da organização',
    //   status: 1
    // },
    // {
    //   title: 'Perfil da organização',
    //   status: 1
    // },
    // {
    //   title: 'Material de divulgação',
    //   status: 1
    // },
    // {
    //   title: 'Termo de Adesão',
    //   status: 1
    // },
    // {
    //   title: 'Boas vindas',
    //   status: 1
    // },
    // {
    //   title: 'Cadastro de comprovantes',
    //   status: 1
    // },
    // {
    //   title: 'Cadastro de projetos',
    //   status: 1
    // },
    // {
    //   title: 'Relatório de projetos',
    //   status: 0
    // },
    // {
    //   title: 'Assessoria de Projetos',
    //   status: 0
    // },
    // {
    //   title: 'Assessoria de Relatório',
    //   status: 0
    // },
    // {
    //   title: 'Gerar Protocolo',
    //   status: 0
    // },
    // {
    //   title: 'Certificação',
    //   status: 0
    // }
  ];

  admPermissions: IAdmPermission[] = [
    {
      img: '/assets/icons/adm_projetos.svg',
      subtitle: 'Visualizar projetos',
      link: '/projetos'
    },
    {
      img: '/assets/icons/adm_participantes.svg',
      subtitle: 'Administrar participantes',
      link: '/usuarios/listagem'
    },
    {
      img: '/assets/icons/adm_orgs.svg',
      subtitle: 'Administrar organizações',
      link: '/organizacoes'
    },
    {
      img: '/assets/icons/adm_protocolos.svg',
      subtitle: 'Visualizar protocolos e certificados',
      link: '/protocolos-e-relatorios'
    },
    {
      img: '/assets/icons/adm_relatorios.svg',
      subtitle: 'Relatórios',
      link: '/protocolos-e-relatorios'
    }
  ];

  constructor(public _authService: AuthService) {}

  ngOnInit(): void {}
}
