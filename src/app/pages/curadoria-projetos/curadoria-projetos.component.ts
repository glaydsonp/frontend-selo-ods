import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { IProject } from 'src/app/models/project';
import { CuradorStatusEnum, ProjectStatusEnum } from 'src/app/models/project-status.enum';
import { IUserRole } from 'src/app/models/user';
import { AuthService } from 'src/app/services/auth.service';
import { ProjectService } from 'src/app/services/project.service';

@Component({
  selector: 'app-curadoria-projetos',
  templateUrl: './curadoria-projetos.component.html',
  styleUrls: ['./curadoria-projetos.component.scss']
})
export class CuradoriaProjetosComponent implements OnInit {
  avaliacoes = [
    { desc: 'Todos', value: '' },
    { desc: 'Sim', value: 0 },
    { desc: 'Não', value: 1 }
  ];

  projetos: IProject[] = [];

  filtroProjetos = new FormGroup({});

  IUserRole = IUserRole;
  CuradorStatusEnum = CuradorStatusEnum;
  ProjectStatusEnum = ProjectStatusEnum;

  constructor(public projectService: ProjectService, private _authService: AuthService, private _fb: FormBuilder) {}

  ngOnInit() {
    this.buildFiltros();
    this.getProjects();
  }

  buildFiltros() {
    this.filtroProjetos = this._fb.group({
      nome: null,
      avaliacao: null
    });
  }

  getProjects() {
    const isCurator = this._authService.getUserRole() === IUserRole.Curador;
    const userId = this._authService.getCurrentUser().userId;

    this.projectService.getAll(isCurator).subscribe((res) => {
      this.projetos = res.filter(
        (el) =>
          (!el.curador1 && !el.curador2) ||
          (el.curador1 && !el.curador2) ||
          el.curador1?.curadorId === userId ||
          el.curador2?.curadorId === userId
      );
    });
  }

  filtrar() {
    const filterForm = this.filtroProjetos.value;

    const isCurator = this._authService.getUserRole() === IUserRole.Curador;

    this.projectService.getAll(isCurator).subscribe((res) => {
      this.projetos = res.filter((el) => {
        if (!filterForm?.nome && !filterForm?.avaliacao) {
          return true;
        }

        if (
          el.geral.nome?.toLowerCase().includes(filterForm?.nome?.toLowerCase()) &&
          el?.status === filterForm?.avaliacao
        ) {
          return true;
        }

        if (el?.geral?.nome?.toLowerCase().includes(filterForm?.nome?.toLowerCase()) && !filterForm?.avaliacao) {
          return true;
        }

        return false;
      });
    });
  }

  resetFilter() {
    this.filtroProjetos.reset();
  }
}
