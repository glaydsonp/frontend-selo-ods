import { CuradoriaAnaliseComponent } from './curadoria-analise/curadoria-analise.component';
import { CuradoriaProjetosComponent } from './curadoria-projetos.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    component: CuradoriaProjetosComponent
  },
  {
    path: ':id',
    component: CuradoriaAnaliseComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CuradoriaProjetosRoutingModule {}
