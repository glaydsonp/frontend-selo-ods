import { MatDialog } from '@angular/material/dialog';
import { SnackbarType } from './../../../models/snackbar-type.enum';
import { SnackbarService } from './../../../services/snackbar.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { IStep } from 'src/app/models/register';
import { StepperActionEnum } from 'src/app/models/stepper-action.enum';
import { SessionService } from 'src/app/services/session.service';
import { CuradoriaService } from 'src/app/services/curadoria.service';

@Component({
  selector: 'app-curadoria-analise',
  templateUrl: './curadoria-analise.component.html',
  styleUrls: ['./curadoria-analise.component.scss']
})
export class CuradoriaAnaliseComponent implements OnInit {
  ods: any;

  step = 0;
  currentSubStep = 0;

  projectId = '';

  form = new FormGroup({});
  organizationId = '';

  StepperAction = StepperActionEnum;

  steps: IStep[] = [
    {
      title: 'Resumo do Projeto',
      step: 0,
      substeps: []
    },
    {
      title: 'Sobre os ODS',
      step: 1,
      substeps: []
    }
  ];

  constructor(
    public sessionService: SessionService,
    private activatedRoute: ActivatedRoute,
    private _curadoriaService: CuradoriaService,
    private _snackbarService: SnackbarService,
    private router: Router,
    private dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.getProjectId();
  }

  getProjectId(): void {
    const params = this.activatedRoute.snapshot.params;
    this.projectId = params.id;
  }

  changeStep(step: number): void {
    if (step !== this.step) {
      this.step = step;
      this.currentSubStep = 0;
    }
  }

  changeSubStep(substep: number): void {
    if (substep !== this.currentSubStep) {
      this.currentSubStep = substep;
    }
  }

  previousOrNextStep(action: number) {
    if (action === StepperActionEnum.Next) {
      if (
        this.step < this.steps.length &&
        this.currentSubStep < this.steps[this.step].substeps.length
      ) {
        this.currentSubStep++;
      }

      if (
        this.step < this.steps.length &&
        this.currentSubStep === this.steps[this.step].substeps.length
      ) {
        this.step++;
        this.currentSubStep = 0;
      }
    }

    if (action === StepperActionEnum.Previous) {
      if (this.step > 0 && this.currentSubStep > 0) {
        this.currentSubStep--;
        return;
      }

      if (this.step === 0 && this.currentSubStep > 0) {
        this.currentSubStep--;
        return;
      }

      if (this.step > 0 && this.currentSubStep === 0) {
        this.step--;
        this.currentSubStep = this.steps[this.step].substeps.length - 1;
        return;
      }
    }
  }

  setSeloOds(value: any) {
    this.ods = value;
  }

  submit(): void {
    this._curadoriaService
      .updateProjectOds(this.projectId, this.ods.value)
      .then((res) => {
        this._snackbarService.openSnackBar(
          'Projeto atualizado com sucesso!',
          SnackbarType.Success
        );

        this.dialog.closeAll();
        this.router.navigate(['/curadoria-projetos']);
      })
      .catch((err) => {
        console.log(err);
        this._snackbarService.openSnackBar(err.message, SnackbarType.Success);
      });
    // this.updateOrganization();
    // this.dialog.open(ConfirmationDialogComponent);
    // const dialogRef = this.dialog.open(ConfirmationDialogComponent);
    // dialogRef.afterClosed().subscribe((result) => {
    //   console.log(`Dialog result: ${result}`);
    // });
  }
}
