import { CuradoriaService } from './../../../services/curadoria.service';
import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { IProjectCurador } from 'src/app/models/project';

@Component({
  selector: 'app-resumo-projeto',
  templateUrl: './resumo-projeto.component.html',
  styleUrls: ['./resumo-projeto.component.scss']
})
export class ResumoProjetoComponent implements OnInit, OnChanges {
  project: IProjectCurador;

  @Input() projectId = '';

  constructor(private _curadorService: CuradoriaService) {}

  ngOnInit(): void {}

  ngOnChanges(): void {
    this._curadorService.getProjectById(this.projectId).then((res) => {
      this.project = res;
    });
  }
}
