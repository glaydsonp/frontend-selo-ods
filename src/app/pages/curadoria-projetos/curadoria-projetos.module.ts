import { SharedModule } from 'src/app/shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CuradoriaProjetosRoutingModule } from './curadoria-projetos-routing.module';
import { CuradoriaProjetosComponent } from './curadoria-projetos.component';
import { MatSelectModule } from '@angular/material/select';
import { CuradoriaAnaliseComponent } from './curadoria-analise/curadoria-analise.component';
import { ResumoProjetoComponent } from './resumo-projeto/resumo-projeto.component';
import { SobreOdsComponent } from './sobre-ods/sobre-ods.component';
import { SugestaoIdeiaDialogComponent } from './sobre-ods/sugestao-ideia-dialog/sugestao-ideia-dialog.component';


@NgModule({
  declarations: [
    CuradoriaProjetosComponent,
    CuradoriaAnaliseComponent,
    ResumoProjetoComponent,
    SobreOdsComponent,
    SugestaoIdeiaDialogComponent
  ],
  imports: [
    CommonModule,
    CuradoriaProjetosRoutingModule,
    SharedModule,
    MatSelectModule
  ]
})
export class CuradoriaProjetosModule { }
