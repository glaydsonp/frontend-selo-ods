import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { NovoObjetivoComponent } from 'src/app/pages/projetos/cadastro-projeto/objetivos/novo-objetivo/novo-objetivo.component';

@Component({
  templateUrl: './sugestao-ideia-dialog.component.html',
  styleUrls: ['./sugestao-ideia-dialog.component.scss']
})
export class SugestaoIdeiaDialogComponent implements OnInit {
  ideia = '';

  constructor(
    private dialogRef: MatDialogRef<NovoObjetivoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: string,
    private _fb: FormBuilder
  ) {}

  ngOnInit(): void {
    if (this.data) {
      this.ideia = this.data;
    }
  }

  close() {
    this.dialogRef.close(this.ideia);
  }
}
