import { FormGroup, FormBuilder, FormArray } from '@angular/forms';
import { SugestaoIdeiaDialogComponent } from './sugestao-ideia-dialog/sugestao-ideia-dialog.component';
import { IProjectCurador } from './../../../models/project';
import { CuradoriaService } from './../../../services/curadoria.service';
import { Component, Input, OnInit, EventEmitter, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { SnackbarType } from 'src/app/models/snackbar-type.enum';
import { JustificativaDialogComponent } from '../../projetos/desenvolvimento/acoes-desenvolvidas/justificativa-dialog/justificativa-dialog.component';
import { SnackbarService } from 'src/app/services/snackbar.service';

@Component({
  selector: 'app-sobre-ods',
  templateUrl: './sobre-ods.component.html',
  styleUrls: ['./sobre-ods.component.scss']
})
export class SobreOdsComponent implements OnInit {
  form = new FormGroup({});
  // validado: any;

  @Input() projectId = '';

  @Output() sendOds = new EventEmitter();

  project: IProjectCurador;

  constructor(
    private _curadorService: CuradoriaService,
    private dialog: MatDialog,
    private _snackbarService: SnackbarService,
    private _fb: FormBuilder
  ) {}

  ngOnInit(): void {
    // this._curadoriaService.getProjectCurador().subscribe((res) => {
    //   console.log('res subscribe', res);
    //   this.project = res;
    // });
    // this.form = this._fb.group({
    //   ods: this._fb.array([])
    // })
  }

  ngOnChanges(): void {
    this._curadorService.getProjectById(this.projectId).then((res) => {
      const ods = res.ods.filter((el) => el.checked);

      const projeto = {
        objetivos: res.objetivos,
        ods
      };

      this.project = projeto;

      this.buildForm();
    });
  }

  checkOpenIdeiaDialog(ods: any, val: any) {
    if (!val) {
      this.dialog
        .open(SugestaoIdeiaDialogComponent, {
          data: ods?.value?.ideiaSugerida || ''
        })
        .afterClosed()
        .subscribe(
          (res) => {
            ods?.controls?.ideiaSugerida.patchValue(res);

            this.sendOds.emit(this.form);
          },
          (err) => {
            this._snackbarService.openSnackBar(
              err.message,
              SnackbarType.Danger
            );
          }
        );
    } else {
      ods?.controls?.ideiaSugerida.patchValue('');
      this.sendOds.emit(this.form);
    }
  }

  buildForm(): void {
    this.form = this._fb.group({
      ods: this.buildOds()
    });
  }

  buildOds() {
    const values = this.project?.ods?.map((el) => this._fb.group(el));

    return this._fb.array(values);
  }

  getOds() {
    const form = this.form?.get('ods') as FormArray;

    if (form && form.controls) {
      return form.controls;
    } else return [];
  }
}
