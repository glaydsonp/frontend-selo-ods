import { SnackbarService } from './../../services/snackbar.service';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { AuthService } from './../../services/auth.service';
import { CuradoriaService } from 'src/app/services/curadoria.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AddPartnerDialogComponent } from './add-partner-dialog/add-partner-dialog.component';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { IPartner, IUser } from 'src/app/models/user';
import { SnackbarType } from 'src/app/models/snackbar-type.enum';

@Component({
  selector: 'app-partners-validation',
  templateUrl: './partners-validation.component.html',
  styleUrls: ['./partners-validation.component.scss']
})
export class PartnersValidationComponent implements OnInit {
  form: FormGroup;
  partners: IPartner[] = [];
  user: IUser;

  constructor(
    private dialog: MatDialog,
    private _fb: FormBuilder,
    private _curadoriaService: CuradoriaService,
    private _authService: AuthService,
    private _db: AngularFirestore,
    private _snackbarService: SnackbarService
  ) {}

  ngOnInit(): void {
    this.getPartners();
    this.buildForm();
  }

  async getPartners() {
    const userId = this._authService.getCurrentUser().userId || '';

    const userRef = await this._db
      .collection('users')
      .doc(userId)
      .get()
      .toPromise();

    this.user = userRef.data() as IUser;

    this._curadoriaService.getCuratorPartners(userId).subscribe((res) => {
      this.partners =
        (res?.parceiros?.parceirosList as unknown as IPartner[]) || [];
    });
  }

  addNewPartner() {
    const dialogRef = this.dialog.open(AddPartnerDialogComponent);

    dialogRef.afterClosed().subscribe((res) => {
      this.partners.push(res);
    });
  }

  deletePartner(index: number) {
    this.partners.splice(index, 1);
  }

  buildForm() {
    this.form = this._fb.group({
      projetoCicloSelo: [null, Validators.required],
      instituicoesParceiras: [null, Validators.required]
    });
  }

  submit() {
    const user = this.user;
    user.parceiros = {
      ...this.form.value,
      parceirosList: this.partners
    };

    try {
      this._db
        .collection('users')
        .doc(this.user.id)
        .update({ ...user });

      this._snackbarService.openSnackBar(
        'Parceiros atualizado com sucesso!',
        SnackbarType.Success
      );
    } catch (err) {
      if (err instanceof Error) {
        this._snackbarService.openSnackBar(err.message, SnackbarType.Success);
      }
    }
  }
}
