import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-add-partner-dialog',
  templateUrl: './add-partner-dialog.component.html',
  styleUrls: ['./add-partner-dialog.component.scss']
})
export class AddPartnerDialogComponent implements OnInit {
  form: FormGroup;

  constructor(
    private dialogRef: MatDialogRef<AddPartnerDialogComponent>,
    private _fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.form = this._fb.group({
      cnpj: [null, Validators.required],
      nome: [null, Validators.required]
    });
  }

  close(action: number) {
    if (action === 0) {
      this.dialogRef.close();
    }

    if (action === 1) {
      this.dialogRef.close(this.form.value);
    }
  }
}
