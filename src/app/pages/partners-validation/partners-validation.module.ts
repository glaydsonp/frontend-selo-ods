import { SharedModule } from 'src/app/shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PartnersValidationRoutingModule } from './partners-validation-routing.module';
import { PartnersValidationComponent } from './partners-validation.component';
import { AddPartnerDialogComponent } from './add-partner-dialog/add-partner-dialog.component';


@NgModule({
  declarations: [
    PartnersValidationComponent,
    AddPartnerDialogComponent
  ],
  imports: [
    CommonModule,
    PartnersValidationRoutingModule,
    SharedModule
  ]
})
export class PartnersValidationModule { }
