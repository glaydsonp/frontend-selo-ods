import { AngularFireAuth } from '@angular/fire/compat/auth';
import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { IDialogMessage } from 'src/app/models/dialog-message';
import { IUser, IUserRole } from 'src/app/models/user';
import { SnackbarService } from 'src/app/services/snackbar.service';
import { GenericDialogComponent } from 'src/app/shared/generic-dialog/generic-dialog.component';
import { NovoObjetivoComponent } from '../../projetos/cadastro-projeto/objetivos/novo-objetivo/novo-objetivo.component';

@Component({
  selector: 'app-adicionar-usuario',
  templateUrl: './adicionar-usuario.component.html',
  styleUrls: ['./adicionar-usuario.component.scss']
})
export class AdicionarUsuarioComponent implements OnInit {
  form: FormGroup;

  constructor(
    private _fb: FormBuilder,
    private _snackbarService: SnackbarService,
    private dialog: MatDialog,
    private dialogRef: MatDialogRef<NovoObjetivoComponent>,
    private _db: AngularFirestore,
    private firebaseAuth: AngularFireAuth
  ) {}

  ngOnInit(): void {
    this.buildForm();
  }

  async buildForm() {
    this.form = this._fb.group({
      userRole: [null, Validators.required],
      email: [null, Validators.required],
      emailConfirmation: [null, Validators.required]
    });
  }

  createUser() {
    const form = Object.assign(this.form.value);

    const texts: IDialogMessage = {
      title: 'Confirma o envio do convite?',
      message: '',
      dismiss: 'Cancelar',
      confirm: 'Confirmar'
    };

    const dialogRef = this.dialog.open(GenericDialogComponent, { data: texts });

    dialogRef.afterClosed().subscribe(async (res) => {
      if (res) {
        this.firebaseAuth.createUserWithEmailAndPassword(form.email, '123456');

        await this._db
          .collection('users')
          .add({ role: form.userRole, email: form.email, active: true })
          .then((added) => {
            added.update({ id: added.id });
          });

        this.dialog.closeAll();
      }
    });
  }

  submit() {}

  close() {
    this.dialogRef.close(null);
  }
}
