import { AdicionarUsuarioComponent } from './adicionar-usuario/adicionar-usuario.component';
import { UserService } from './../../services/user.service';
import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { IOrganization } from 'src/app/models/organization';
import { IProject } from 'src/app/models/project';
import { IUser, IUserRole } from 'src/app/models/user';
import { AuthService } from 'src/app/services/auth.service';
import { ProjectService } from 'src/app/services/project.service';
import { OrganizationDialogComponent } from '../projetos-adm/listagem/organization-dialog/organization-dialog.component';
import { ProjectDialogComponent } from '../projetos-adm/listagem/project-dialog/project-dialog.component';

@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  styleUrls: ['./usuarios.component.scss']
})
export class UsuariosComponent implements OnInit {
  tiposCadastro = [
    { desc: 'Todos', value: '' },
    { desc: 'Participante', value: 0 },
    { desc: 'Curador', value: 1 },
    { desc: 'Administrador', value: 2 }
  ];

  ciclos = [
    { desc: 'Todos', value: '' },
    { desc: 'Ciclo 2021', value: 2021 },
    { desc: 'Ciclo 2022', value: 2022 },
    { desc: 'Ciclo 2023', value: 2023 }
  ];

  usuarios: IUser[] = [];

  filtroUsuariosForm = new FormGroup({});

  IUserRole = IUserRole;

  constructor(
    public userService: UserService,
    private _authService: AuthService,
    private _db: AngularFirestore,
    private _fb: FormBuilder,
    private dialog: MatDialog
  ) {}

  ngOnInit() {
    this.buildFiltros();
    this.getUsers();
  }

  buildFiltros() {
    this.filtroUsuariosForm = this._fb.group({
      nome: null,
      tipoCadastro: null,
      ciclo: null
    });
  }

  getUsers() {
    this.userService.getUsers().then((res) => {
      this.usuarios = res;
    });
  }

  async openOrganizationDialog(ownerOrganizationId: string) {
    const ownerOrganizationNameRef = await this._db
      .collection('organizations')
      .doc<IOrganization>(ownerOrganizationId)
      .get()
      .toPromise();

    const ownerOrganization = ownerOrganizationNameRef.data() as IOrganization;

    const dialogRef = this.dialog.open(OrganizationDialogComponent, {
      data: ownerOrganization
    });

    dialogRef.afterClosed().subscribe((res) => {});
  }

  createCurator() {
    const dialogRef = this.dialog.open(AdicionarUsuarioComponent);

    dialogRef.afterClosed().subscribe((res) => {});
  }

  filtrar() {
    const filterForm = this.filtroUsuariosForm.value;
    console.log(filterForm);

    this.userService.getUsers().then((res) => {
      this.usuarios = res.filter((el) => {
        if (!filterForm?.nome && filterForm?.tipoCadastro === '' && !filterForm?.ciclo) {
          return true;
        }

        if (
          el?.nome?.toLowerCase().includes(filterForm?.nome?.toLowerCase()) ||
          el?.role === filterForm?.tipoCadastro
        ) {
          return true;
        }

        // if (el?.nome?.toLowerCase().includes(filterForm?.nome?.toLowerCase()) && el?.role === filterForm.tipoCadastro) {
        //   return true;
        // }

        // if (el?.nome?.toLowerCase().includes(filterForm?.nome?.toLowerCase()) && el?.role !== filterForm.tipoCadastro) {
        //   return true;
        // }

        // if (
        //   !el?.nome?.toLowerCase().includes(filterForm?.nome?.toLowerCase()) &&
        //   el?.role === filterForm.tipoCadastro
        // ) {
        //   return true;
        // }

        // if (el?.geral?.nome?.toLowerCase().includes(filterForm?.nome?.toLowerCase()) && !filterForm?.avaliacao) {
        //   return true;
        // }

        // if (el?.geral?.nome?.toLowerCase().includes(filterForm?.nome?.toLowerCase())) {
        //   return true;
        // }

        return false;
      });
    });
  }

  resetFilter() {
    this.filtroUsuariosForm.reset();
  }

  // async getOrganizationName(id: string) {
  //   const orgId = (
  //     await this._db.collection('users').doc<IUser>(id).get().toPromise()
  //   ).data()?.organizationId;

  //   const orgName = (
  //     await this._db
  //       .collection('organizations')
  //       .doc<IOrganization>(orgId)
  //       .get()
  //       .toPromise()
  //   ).data()?.dadosCadastrais.nomeInstituicao as string;

  //   return orgName;
  // }
}
