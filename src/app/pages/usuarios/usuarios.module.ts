import { MatSelectModule } from '@angular/material/select';
import { SharedModule } from './../../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UsuariosRoutingModule } from './usuarios-routing.module';
import { UsuariosComponent } from './usuarios.component';
import { AdicionarUsuarioComponent } from './adicionar-usuario/adicionar-usuario.component';
import { EdicaoComponent } from './edicao/edicao.component';
import { EditarPermissoesComponent } from './editar-permissoes/editar-permissoes.component';

@NgModule({
  declarations: [UsuariosComponent, AdicionarUsuarioComponent, EdicaoComponent, EditarPermissoesComponent],
  imports: [CommonModule, UsuariosRoutingModule, SharedModule, MatSelectModule]
})
export class UsuariosModule {}
