import { EdicaoComponent } from './edicao/edicao.component';
import { UsuariosComponent } from './usuarios.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'listagem',
    component: UsuariosComponent
  },
  {
    path: ':id',
    component: EdicaoComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsuariosRoutingModule {}
