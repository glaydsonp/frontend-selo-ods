import { Router } from '@angular/router';
import { Component, Inject, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { FormGroup, FormBuilder } from '@angular/forms';
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA
} from '@angular/material/dialog';
import { IDialogMessage } from 'src/app/models/dialog-message';
import { IUser } from 'src/app/models/user';
import { SnackbarService } from 'src/app/services/snackbar.service';
import { GenericDialogComponent } from 'src/app/shared/generic-dialog/generic-dialog.component';
import { NovoObjetivoComponent } from '../../projetos/cadastro-projeto/objetivos/novo-objetivo/novo-objetivo.component';

@Component({
  templateUrl: './editar-permissoes.component.html',
  styleUrls: ['./editar-permissoes.component.scss']
})
export class EditarPermissoesComponent implements OnInit {
  // form: FormGroup;
  role = 0;
  id = '';

  permissions = [
    {
      description: 'Administrador',
      role: 2
    },
    {
      description: 'Curador',
      role: 1
    },
    // {
    //   description: 'Responsável pela página da organização',
    //   role: 1
    // },
    {
      description: 'Participante',
      role: 0
    }
  ];

  constructor(
    private _fb: FormBuilder,
    private _snackbarService: SnackbarService,
    private dialog: MatDialog,
    private dialogRef: MatDialogRef<NovoObjetivoComponent>,
    private _db: AngularFirestore,
    private router: Router,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit(): void {
    // this.buildForm();

    if (this.data) {
      this.role = this.data.role;
      this.id = this.data.id;
    }
  }

  // async buildForm() {
  //   const userRef = await this._db
  //     .collection('users')
  //     .doc<IUser>(this.data.owner)
  //     .get()
  //     .toPromise();

  //   const user = userRef.data();

  //   this.form = this._fb.group({
  //     cnpj: this.data?.dadosCadastrais?.cnpj,
  //     nome: this.data?.dadosCadastrais?.nomeInstituicao,
  //     responsavel: user?.nome,
  //     celular: user?.whatsapp,
  //     registro: user?.registro
  //   });
  // }

  deleteUser() {
    const texts: IDialogMessage = {
      title: 'Tem certeza que deseja excluir esta organização?',
      message: 'Essa ação é permanente e não pode ser desfeita!',
      dismiss: 'Cancelar',
      confirm: 'Confirmar'
    };

    const dialogRef = this.dialog.open(GenericDialogComponent, { data: texts });

    dialogRef.afterClosed().subscribe(async (res) => {
      if (res) {
        await this._db
          .collection('users')
          .doc(this.id)
          .update({ active: false });

        this.dialog.closeAll();
        this.router.navigate(['/usuarios/listagem']);
      }
    });
  }

  updateRole() {
    this.dialogRef.close(this.role);
  }

  close() {
    this.dialogRef.close(null);
  }
}
