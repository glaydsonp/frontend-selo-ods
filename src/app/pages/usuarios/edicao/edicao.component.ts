import { SnackbarService } from './../../../services/snackbar.service';
import { IProject } from './../../../models/project';
import { ProjectService } from './../../../services/project.service';
import { OrganizationService } from './../../../services/organization.service';
import { EditarPermissoesComponent } from './../editar-permissoes/editar-permissoes.component';
import { MatDialog } from '@angular/material/dialog';
import { IOrganization } from './../../../models/organization';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { IUser, IUserRole } from 'src/app/models/user';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { SnackbarType } from 'src/app/models/snackbar-type.enum';

@Component({
  selector: 'app-edicao',
  templateUrl: './edicao.component.html',
  styleUrls: ['./edicao.component.scss']
})
export class EdicaoComponent implements OnInit {
  form: FormGroup;
  IUserRole = IUserRole;
  user: IUser;
  id: string = '';
  preCadastroValid = false;
  cadastroOrganizationValid = false;
  cadastroProjetoValid = false;

  constructor(
    private activatedRoute: ActivatedRoute,
    private _db: AngularFirestore,
    private _fb: FormBuilder,
    private dialog: MatDialog,
    private _organizationService: OrganizationService,
    private _projectService: ProjectService,
    private router: Router,
    private _snackbarService: SnackbarService
  ) {}

  async ngOnInit() {
    const params = this.activatedRoute.snapshot.params;

    this.id = params.id;

    this.buildForm();
    this.getUser();

    this.checkPreCadastro();
    this.checkCadastroOrganization();
    this.checkCadastroProjeto();
  }

  async getUser() {
    const usersRef = await this._db
      .collection('users')
      .doc<IUser>(this.id)
      .get()
      .toPromise();

    const user = usersRef.data() as IUser;
    this.user = user;

    this.getOrganization();
  }

  async getOrganization() {
    const orgRef = await this._db
      .collection('organizations')
      .doc<IOrganization>(this.user?.organizationId)
      .get()
      .toPromise();

    const organizationName = orgRef.data()?.dadosCadastrais?.nomeInstituicao;

    this.form.patchValue({ ...this.user, organizationName });

    console.log(this.form);

    this.checkPreCadastro();
    this.checkCadastroOrganization();
    this.checkCadastroProjeto();
  }

  buildForm() {
    this.form = this._fb.group({
      nome: [null, Validators.required],
      role: [null, Validators.required],
      organizationName: [null, Validators.required],
      telefone: [null, Validators.required],
      email: [null, Validators.required],
      whatsapp: [null, Validators.required],
      registro: [null]
    });
  }

  editPermissions() {
    const dialogRef = this.dialog.open(EditarPermissoesComponent, {
      data: { id: this.user.id, role: this.user.role }
    });

    dialogRef.afterClosed().subscribe(async (res) => {
      if (res !== null && res !== undefined) {
        try {
          await this._db.collection('users').doc(this.id).update({ role: res });

          this._snackbarService.openSnackBar(
            'Permissão atualizada com sucesso!',
            SnackbarType.Success
          );

          this.getUser();
        } catch (err) {
          if (err instanceof Error) {
            this._snackbarService.openSnackBar(
              err.message,
              SnackbarType.Success
            );
          }
        }
      }
    });
  }

  async deletePartner(index: number) {
    this.user.parceiros.parceirosList.splice(index, 1);

    try {
      await this._db
        .collection('users')
        .doc(this.id)
        .update({ ...this.user });

      this._snackbarService.openSnackBar(
        'Parceiro deletado com sucesso!',
        SnackbarType.Success
      );

      this.getUser();
    } catch (err) {
      if (err instanceof Error) {
        this._snackbarService.openSnackBar(err.message, SnackbarType.Danger);
      }
    }
  }

  async deleteUser() {
    try {
      await this._db.collection('users').doc(this.id).update({ active: false });

      this._snackbarService.openSnackBar(
        'Usuário deletado com sucesso!',
        SnackbarType.Success
      );

      this.router.navigate(['/usuarios/listagem']);
    } catch (err) {
      if (err instanceof Error) {
        this._snackbarService.openSnackBar(err.message, SnackbarType.Danger);
      }
    }
  }

  checkPreCadastro() {
    const user = this.user;

    if (
      user &&
      user.nome &&
      user.cargo &&
      user.email &&
      user.telefone &&
      user.whatsapp &&
      user.endereco &&
      user.comoFicouSabendoDoPrograma
    ) {
      this.preCadastroValid = true;
    } else {
      this.preCadastroValid = false;
    }
  }

  checkCadastroOrganization() {
    if (this.user) {
      const form = this._organizationService.buildForm();

      let valid = false;

      this._organizationService.form.subscribe((val) => {
        form.patchValue(val.value);

        const dadosCadastraisValid = this.form.get('dadosCadastrais')?.valid;
        const enderecoValid = this.form.get('endereco')?.valid;
        const pessoalValid = this.form.get('pessoal')?.valid;
        const perfilValid = this.form.get('perfil')?.valid;
        const projetosValid = this.form.get('projetos')?.valid;
        const expectativaValid = this.form.get('expectativa')?.valid;
        const comunicacaoValid = this.form.get('comunicacao')?.valid;

        if (
          dadosCadastraisValid &&
          enderecoValid &&
          pessoalValid &&
          perfilValid &&
          projetosValid &&
          expectativaValid &&
          comunicacaoValid
        ) {
          valid = true;
        }
      });

      this.cadastroOrganizationValid = valid;
    } else this.cadastroOrganizationValid = false;
  }

  async checkCadastroProjeto() {
    try {
      if (this.user) {
        let valid = true;

        const projetosRef = await this._db
          .collection<IProject>('projects')
          .get()
          .toPromise();

        const projetos = projetosRef.docs
          .map((el) => el.data())
          .filter((el) => el.owner === this.id);

        projetos.forEach((el) => {
          const form = this._projectService.buildForm();

          this._projectService.form.subscribe((val) => {
            form.patchValue(val.value);

            const geralValid = form.get('geral')?.valid;
            const estudoRealidadeValid = form.get('estudoRealidade')?.valid;
            const justificativaValid = form.get('justificativa')?.valid;
            const objetivosValid = form.get('objetivos')?.valid;
            const publicoValid = form.get('publico')?.valid;
            const resumoValid = form.get('resumo')?.valid;
            const investimentosValid = form.get('investimentos')?.valid;
            const albunsValid = form.get('albuns')?.value.length > 0;
            const artigosValid = form.get('artigos')?.value.length > 0;
            const eventosValid = form.get('eventos')?.value.length > 0;
            const videosValid = form.get('albuns')?.value.length > 0;

            if (
              !geralValid ||
              !estudoRealidadeValid ||
              !justificativaValid ||
              !objetivosValid ||
              !publicoValid ||
              !resumoValid ||
              !investimentosValid ||
              !albunsValid ||
              !artigosValid ||
              !eventosValid ||
              !videosValid
            ) {
              valid = false;
              return;
            }
          });
        });

        this.cadastroProjetoValid = valid;
      } else this.cadastroProjetoValid = false;
    } catch (err) {
      if (err instanceof Error) {
        this._snackbarService.openSnackBar(err.message, SnackbarType.Danger);
      }
    }
  }
}
