import { Router } from '@angular/router';
import { SessionService } from './../../services/session.service';
import { Component, OnInit, Sanitizer } from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.scss']
})
export class WelcomeComponent implements OnInit {
  url = '';

  mainLinks = {
    guiaAgenda: 'https://www.guiaagenda2030.org/',
    videoInstitucional: 'https://www.youtube.com/embed/hLEBV4qBUUg'
  };

  items = [
    {
      linkName: 'Clique aqui',
      link: 'https://idsc-br.sdgindex.org/',
      text: 'para visualizar relatórios dinâmicos dos avanços dos indicadores dos ODS de todos os estados e municípios, com análises textuais, mapas, gráficos e infográficos que poderão ajudar em seu trabalho de reflexão, análise e decisão sobre o que fazer.'
    },
    {
      linkName: 'Aqui',
      link: 'http://rd.portalods.com.br/',
      text: 'você encontra uma ferramenta para estimular e monitorar o cumprimento dos Objetivos de Desenvolvimento Sustentável (ODS) em diversas cidades brasileiras.'
    },
    {
      linkName: 'Aqui',
      link: 'https://odsbrasil.gov.br/objetivo/objetivo?n=1',
      text: 'você encontra todos os indicadores separados por ODS e a sua situação no Brasil em 4 dimensões: Indicadores produzidos; Indicadores em análise/construção; Indicadores sem dados; Indicadores que Não se aplicam no Brasil.'
    },
    {
      linkName: 'Aqui',
      link: 'https://gtagenda2030.org.br/relatorio-luz/relatorio-luz-2021/',
      text: 'você encontra um relatório que é produzido anualmente pelo Grupo de Trabalho da Sociedade Civil para a Agenda 2030 (GTSC-A2030). Em 2020, o Relatório Luz é sobre o impacto do novo coronavírus no Brasil, em diálogo com cada um dos 17 Objetivos de Desenvolvimento Sustentável.'
    },
    {
      linkName: 'Aqui',
      link: 'https://www.ipea.gov.br/ods/publicacoes',
      text: 'você encontra a publicação Cadernos ODS. Os cadernos é uma produção do Instituto de Pesquisa Econômica Aplicada (Ipea), para divulgar estudos e pesquisas que visam contribuir para o esforço nacional de alcançar os desafios lançados durante a Cúpula de Desenvolvimento Sustentável da Assembleia Geral das Nações Unidas 2015. Nesses cadernos, é apresentado um diagnóstico sobre a situação do país em relação a cada ODS.'
    },
    {
      linkName: 'Aqui',
      link: 'https://www.youtube.com/watch?v=IySbYfIJlWk',
      text: 'você encontra a reportagem da TV Câmara, exibida em 31/07/2020, sobre o lançamento do IV Relatório Luz da Sociedade Civil sobre a Agenda 2030 no Brasil, em audiência pública da Frente Parlamentar Mista de Apoio aos ODS.'
    },
    {
      linkName: 'Aqui',
      link: 'https://drive.google.com/file/d/1OkoDkHK5nQYHl0SbSaBTgZR1rdWmUOv4/view?usp=sharing',
      text: 'você encontra em PDF o Relatório Luz produzido pelo GTSC-A2030 sobre o impacto do novo coronavírus no Brasil, em diálogo com cada um dos 17 Objetivos de Desenvolvimento Sustentável.'
    }
  ];

  constructor(
    private location: Location,
    public sessionService: SessionService,
    private _router: Router
  ) // private sanitizer: DomSanitizationService
  {
    const pathName = this.location.path();
    const urlName = pathName.substring(1, pathName.length);

    this.url = urlName;
  }

  ngOnInit(): void {}

  sendToRegister() {
    this._router.navigate(['/pre-cadastro']);
  }

  // sanitizeVideoUrl(url: string) {
  //   return this.sanitizer.byPass
  // }
}
