import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProtocolosRelatoriosComponent } from './protocolos-relatorios.component';

const routes: Routes = [
  {
    path: '',
    component: ProtocolosRelatoriosComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProtocolosRelatoriosRoutingModule {}
