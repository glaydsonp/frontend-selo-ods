import { MatSelectModule } from '@angular/material/select';
import { SharedModule } from 'src/app/shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProtocolosRelatoriosRoutingModule } from './protocolos-relatorios-routing.module';
import { EnviarProtocoloComponent } from './enviar-protocolo/enviar-protocolo.component';
import { ProtocolosRelatoriosComponent } from './protocolos-relatorios.component';
import { EnvioConcluidoComponent } from './envio-concluido/envio-concluido.component';

@NgModule({
  declarations: [ProtocolosRelatoriosComponent, EnviarProtocoloComponent, EnvioConcluidoComponent],
  imports: [CommonModule, ProtocolosRelatoriosRoutingModule, SharedModule, MatSelectModule]
})
export class ProtocolosRelatoriosModule {}
