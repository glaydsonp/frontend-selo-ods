import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-envio-concluido',
  templateUrl: './envio-concluido.component.html',
  styleUrls: ['./envio-concluido.component.scss']
})
export class EnvioConcluidoComponent implements OnInit {
  constructor(private dialog: MatDialog) {}

  ngOnInit(): void {}

  close() {
    this.dialog.closeAll();
  }
}
