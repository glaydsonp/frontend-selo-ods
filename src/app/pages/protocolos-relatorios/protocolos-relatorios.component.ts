import { EnviarProtocoloComponent } from './enviar-protocolo/enviar-protocolo.component';
import { Component, OnInit } from '@angular/core';
import { IProtocolo, ProtocoloStatusEnum } from 'src/app/models/protocolos';
import { FormGroup, FormBuilder } from '@angular/forms';
import { CuradorStatusEnum, ProjectStatusEnum } from 'src/app/models/project-status.enum';
import { IUserRole } from 'src/app/models/user';
import { AuthService } from 'src/app/services/auth.service';
import { ProtocolosService } from 'src/app/services/protocolos.service';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-protocolos-relatorios',
  templateUrl: './protocolos-relatorios.component.html',
  styleUrls: ['./protocolos-relatorios.component.scss']
})
export class ProtocolosRelatoriosComponent implements OnInit {
  avaliacoes = [
    { desc: 'Todos', value: '' },
    { desc: 'Sim', value: 1 },
    { desc: 'Não', value: 0 }
  ];

  protocolos: IProtocolo[] = [];

  filtroProtocolos = new FormGroup({});

  IUserRole = IUserRole;
  CuradorStatusEnum = CuradorStatusEnum;
  ProjectStatusEnum = ProjectStatusEnum;
  ProtocoloStatusEnum = ProtocoloStatusEnum;

  constructor(
    public protocolosService: ProtocolosService,
    private _authService: AuthService,
    private dialog: MatDialog,
    private authService: AuthService,
    private _fb: FormBuilder
  ) {}

  ngOnInit() {
    this.buildFiltros();
    this.getProtocolos();
  }

  buildFiltros() {
    this.filtroProtocolos = this._fb.group({
      participante: null,
      avaliacao: null
    });
  }

  getProtocolos() {
    const isCurator =
      this._authService.getUserRole() === IUserRole.Curador || this._authService.getUserRole() === IUserRole.Adm;

    this.protocolosService.getAll(isCurator).subscribe((res) => {
      this.protocolos = res;
    });
  }

  filtrar() {
    const filterForm = this.filtroProtocolos.value;
    console.log(filterForm)

    const isCurator = this.authService.getUserRole() === IUserRole.Curador;

    this.protocolosService.getAll(isCurator).subscribe((res) => {
      this.protocolos = res.filter((el) => {
        if (!filterForm?.participante && !filterForm?.avaliacao) {
          return true;
        }

        if (
          el?.participante?.toLowerCase().includes(filterForm?.participante?.toLowerCase()) &&
          el?.status === filterForm?.avaliacao
        ) {
          return true;
        }

        if (
          el?.participante?.toLowerCase().includes(filterForm?.participante?.toLowerCase()) &&
          !filterForm?.avaliacao
        ) {
          return true;
        }

        return false;
      });
    });
  }

  resetFilter() {
    this.filtroProtocolos.reset();
  }

  openProtocoloDialog() {
    const dialogRef = this.dialog.open(EnviarProtocoloComponent);

    dialogRef.afterClosed().subscribe((res) => {});
  }
}
