import { OrganizationService } from 'src/app/services/organization.service';
import { EnvioConcluidoComponent } from './../envio-concluido/envio-concluido.component';
import { AuthService } from './../../../services/auth.service';
import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { SnackbarService } from 'src/app/services/snackbar.service';
import { NovoObjetivoComponent } from '../../projetos/cadastro-projeto/objetivos/novo-objetivo/novo-objetivo.component';
import { AngularFireStorage } from '@angular/fire/compat/storage';
import { finalize } from 'rxjs/operators';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { IUser } from 'src/app/models/user';
import { IOrganization } from 'src/app/models/organization';
import { ProtocoloStatusEnum } from 'src/app/models/protocolos';

@Component({
  templateUrl: './enviar-protocolo.component.html',
  styleUrls: ['./enviar-protocolo.component.scss']
})
export class EnviarProtocoloComponent implements OnInit {
  fileName = '';
  ProtocoloStatusEnum = ProtocoloStatusEnum;

  constructor(
    private _snackbarService: SnackbarService,
    private _organizationService: OrganizationService,
    private dialog: MatDialog,
    private dialogRef: MatDialogRef<NovoObjetivoComponent>,
    private _db: AngularFirestore,
    private storage: AngularFireStorage,
    private router: Router,
    private authService: AuthService
  ) {}

  ngOnInit(): void {}

  async onFileChange(event: any) {
    const file = event.files[0];

    if (file) {
      const userRef = this.authService.getCurrentUser();
      const userId = userRef.userId;

      this.fileName = file.name;

      const name = file.name.split('.')[0];
      const fileExt = file.type.split('/')[1];
      const filePath = `protocols/${userId}/${name}.${fileExt}`;

      const fileRef = this.storage.ref(filePath);

      const task = this.storage.upload(filePath, file);

      task
        .snapshotChanges()
        .pipe(
          finalize(() => {
            const downloadUrl = fileRef.getDownloadURL();
            downloadUrl.subscribe(async (url) => {
              if (url) {
                const currentUser = this.authService.getCurrentUser().userId;
                const userRef = await this._db.collection('users').doc<IUser>(currentUser).get().toPromise();

                const user = userRef.data() as IUser;

                const organizationRef = await this._db
                  .collection('organizations')
                  .doc<IOrganization>(user.organizationId)
                  .get()
                  .toPromise();

                const organization = organizationRef.data() as IOrganization;

                this._db
                  .collection('protocols')
                  .add({
                    url,
                    owner: userId,
                    ano: new Date().getFullYear(),
                    participante: organization.dadosCadastrais.nomeInstituicao,
                    status: ProtocoloStatusEnum['Em avaliação']
                  })
                  .then((added) => added.update({ id: added.id }));
              }
            });
          })
        )
        .subscribe();
      // .toPromise();

      // task.percentageChanges().subscribe((val) => {
      //   this.uploadProgress$ = val;
      //   console.log(val);
      // });
      // return upload.ref.getDownloadURL();
    } else return;
  }

  submit() {
    this.dialog.open(EnvioConcluidoComponent);
  }

  close() {
    this.dialogRef.close(null);
  }
}
