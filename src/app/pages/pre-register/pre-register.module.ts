import { SharedModule } from './../../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PreRegisterRoutingModule } from './pre-register-routing.module';
import { PreRegisterComponent } from './pre-register.component';

@NgModule({
  declarations: [PreRegisterComponent],
  imports: [CommonModule, PreRegisterRoutingModule, SharedModule]
})
export class PreRegisterModule {}
