import { IUser } from './../../models/user';
import { AuthService } from 'src/app/services/auth.service';
import { UserService } from './../../services/user.service';
import { SnackbarType } from './../../models/snackbar-type.enum';
import { SnackbarService } from './../../services/snackbar.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';

@Component({
  selector: 'app-pre-register',
  templateUrl: './pre-register.component.html',
  styleUrls: ['./pre-register.component.scss']
})
export class PreRegisterComponent implements OnInit {
  form = new FormGroup({});

  options = ['Redes sociais', 'Google', 'Evento com a Universidade', 'E-mails'];

  regulamentoLink =
    'https://drive.google.com/file/d/1Qfgtvqrs9Zk0h0y_f9PO_6pIWnAz8TOT/view?usp=sharing';


  constructor(
    private _fb: FormBuilder,
    private _snackbarService: SnackbarService,
    private _userService: UserService,
    private _authService: AuthService,
    private _db: AngularFirestore
  ) {}

  ngOnInit(): void {
    this.buildForm();
    this.getUserInfo();

    // const input = this.form.get('whatsapp');

    // input?.valueChanges.subscribe((val) => {
    //   val = '+55' + val;

    //   input.
    // });
    // setTimeout(() => {
    //   this.getUserInfo();
    // }, 500);
  }

  buildForm(): void {
    this.form = this._fb.group({
      nome: [null, Validators.required],
      cargo: [null, Validators.required],
      email: [null, [Validators.required, Validators.email]],
      telefone: [null, Validators.required],
      whatsapp: [null, Validators.required],
      endereco: [null, Validators.required],
      comoFicouSabendoDoPrograma: [null, Validators.required],
      comoFicouSabendoDoProgramaOutros: [null],
      regulamentoAceito: [null, Validators.required]
    });
  }

  getUserInfo() {
    const id = this._authService.getCurrentUser().userId;

    const userRef = this._db.collection('users').doc<IUser>(id).get();

    userRef.subscribe((user) => {
      const userInfo = user.data();
      this.form.patchValue({ ...userInfo });
    });
  }

  submit() {
    if (!this.form.valid || !this.form.value.regulamentoAceito) {
      this._snackbarService.openSnackBar(
        'Formulário inválido!',
        SnackbarType.Danger
      );

      return;
    }

    this._userService.updateUser(this.form.value);
  }
}
