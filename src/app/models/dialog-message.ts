export interface IDialogMessage {
  title: string;
  message: string;
  confirm: string;
  dismiss: string;
}
