export enum ProjectCurrentFormEnum {
  Geral = 'geral',
  EstudoRealdiade = 'estudoRealidade',
  Justificativa = 'justificativa',
  Objetivos = 'objetivos',
  Publico = 'publico',
  Resumo = 'resumo',
  Investimentos = 'investimentos'
}
