export enum SnackbarType {
  Danger = 'danger',
  Success = 'success',
  Warn = 'warn'
}
