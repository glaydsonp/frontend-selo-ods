export interface IStep {
  title: string;
  step: number;
  substeps: ISubStep[];
  valid?: boolean;
}

export interface ISubStep {
  title: string;
  substep: number;
}
