export interface IProject {
  id: string;
  owner: string;
  ownerOrganizationId: string;
  ownerOrganizationName: string;
  geral: {
    nome?: string;
    finalidadesSelosOds: ISelo[];
    eixosProjeto: boolean[];
    tipoProjeto: string;
  };
  estudoRealidade: string;
  justificativa: string;
  objetivos: IObjetivo;
  publico: {
    pessoasImpactadasDiretamente: number;
    pessoasImpactadasIndiretamente: number;
    mulheresImpactadas: number;
    pessoasComDeficienciaImpactadas: number;
    impactadasIgualdadeSocial: number;
    pessoasIdosasImpactadas: number;
    criancaAdolescentesImpactados: number;
    pessoasLGBTQIAImpactadas: number;
    pessoasAtendidasProgramasContraMiseria: number;
    pessoasAtendidasProgramasContraFome: number;
    pessoasAtendidasProgramasContraCancer: number;
    pessoasAtendidasProgramasEstimuloEsporte: number;
    publicosAdicionados: IPublico[];
  };
  resumo: string;
  investimentos: {
    exibirInvestimentosSite: boolean;
    valorInvestimento: string[];
    valorReais: number;
    docentesEnvolvidos: number;
    servidoresTecnico: number;
    projetoPromoveBolsas: boolean;
    bolsasIniciacaoCientifica: number;
    bolsasExtensao: number;
    bolsasMestrado: number;
    bolsasDoutorado: number;
  };
  albuns: IAlbum[];
  videos: IVideo[];
  eventos: IEvento[];
  artigos: IArtigo[];
  status: number;
  curador1: ICurador;
  curador2: ICurador;
}

export interface ICurador {
  curadorId: string;
  statusCuradoria: number;
}

export interface ISelo {
  url: string;
  id: string;
  checked: boolean;
  descricao: string;
  comoResultadoFoiPercebido: string;
  quemBeneficiouResultado: string;
  contextoQueAconteceu: string;
  resultadoPercebido: string;
  nome: string;
  validado: boolean;
  ideiaSugerida: string;
}

export interface IObjetivo {
  objetivoGeral: string;
  objetivosEspecificos: {
    realizado: boolean;
    nomeObjetivo: string;
    planosDeAcao: {
      descricao: string;
      realizado: boolean;
      justificativa: string;
    }[];
  }[];
}

export interface IPublico {
  nomePublico: string;
  descricaoPublico: string;
  ambitoImpactoGerado: string;
  tipoDeImpacto: string;
  endereco: string;
}

export interface IAlbum {
  nomeAlbum: string;
  legenda: string;
  imagesUrl: string[];
}

export interface IVideo {
  nomeVideo: string;
  legenda: string;
  linkVideo: string;
}

export interface IEvento {
  nomeEvento: string;
  descricao: string;
  formato: string;
  linkCobertura: string;
  numeroParticipantes: number;
}

export interface IArtigo {
  titulo: string;
  linkAcesso: string;
}

export interface IProjectCurador {
  objetivos: {
    objetivoGeral: string;
    justificativa?: string;
    objetivosEspecificos: {
      realizado: boolean;
      nomeObjetivo: string;
      planosDeAcao: {
        descricao: string;
        realizado: boolean;
        justificativa: string;
      }[];
    }[];
  };
  ods: ISelo[];
}
