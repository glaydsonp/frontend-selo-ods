export interface ILoginForm {
  email: string;
  senha: string;
}

export interface ISession {
  email: string;
  exp: number;
  userId: string;
  authTime: string;
  expirationTime: string;
  token: string;
  lastLogin: number;
  role: number;
}
