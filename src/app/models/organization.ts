export interface IOrganization {
  dadosCadastrais: {
    abrangencia: string;
    cnpj: string;
    nomeInstituicao: string;
    setor: string;
  };
  endereco: {
    bairro: string;
    cep: string;
    cidade: string;
    complemento: string;
    estado: string;
    longradouro: string;
  };
  id: string;
  owner: string;
  perfil: {
    areasAtuacao: boolean[];
    entradaRanking: string;
    existeComite: string;
    odsFazPartePdi: string;
    participaRede: string;
    participaRedeQual: string;
  };
  pessoal: {
    representanteDaPagina: {
      cpf: string;
      emailContato: string;
      emailOpcional: string;
      nomeCompleto: string;
      ocupacaoInstituicao: string;
      telefone: string;
      usarDadosPreCadastro: boolean;
    };
    representanteLegal: {
      cpf: string;
      emailContato: string;
      emailOpcional: string;
      nomeCompleto: string;
      ocupacaoInstituicao: string;
      telefone: string;
    };
  };
  projetos: {
    finalidades: boolean[];
    lugarProjetoSocial: string;
    parceriaComOutrasOrganizacoes: string;
    projetosComColaboradores: string;
    projetosComColaboradoresOds: { id: string; checked: boolean }[];
    projetosComunidadeLocal: string;
    projetosComunidadeLocalOds: boolean[];
    projetosComunidadeLocalOption: boolean;
  };
  expectativa: {
    aumentarRedeComunicacao: number;
    conhecerOutrasOrganizacoes: number;
    divulgarOTrabalhoOrganizacao: number;
    integrarGrupoSocialLocal: number;
    serCertificadaPeloCompromisso: number;
  };
  valid?: boolean;
}
