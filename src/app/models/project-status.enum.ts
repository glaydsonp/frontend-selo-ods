export enum ProjectStatusEnum {
  'Em edição' = 0,
  'Em curadoria' = 1,
  'Protocolo gerado' = 2
}

export enum CuradorStatusEnum {
  'Em avaliação' = 0,
  Confirmado = 1
}
