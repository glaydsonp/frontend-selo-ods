export interface IProtocolo {
  numero: number;
  ano: number;
  participante: string;
  status: number;
  id: string;
  owner: string;
}

export enum ProtocoloStatusEnum {
  'Em avaliação' = 0,
  'Confirmado' = 1
}