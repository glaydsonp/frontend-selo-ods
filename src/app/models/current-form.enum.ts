export enum OrganizationCurrentFormEnum {
  DadosCadastrais = 'dadosCadastrais',
  Endereco = 'endereco',
  Pessoal = 'pessoal',
  Perfil = 'perfil',
  Projetos = 'projetos',
  Expectativa = 'expectativa',
  Comunicacao = 'comunicacao'
}
