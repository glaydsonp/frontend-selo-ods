export interface IUser {
  firstLogin: boolean;
  id?: string;
  lastLogin: number;
  nome: string;
  cargo: string;
  email: string;
  telefone: string;
  whatsapp: string;
  endereco: string;
  comoFicouSabendoDoPrograma: string;
  comoFicouSabendoDoProgramaOutros: string;
  regulamentoAceito: boolean;
  organizationId: string;
  organizationName?: string;
  role: number;
  curatorRef: boolean;
  registro: number;
  parceiros: {
    projetoCicloSelo: boolean;
    instituicoesParceiras: boolean;
    parceirosList: IPartner[];
  };
}

export interface IPartner {
  nome: string;
  cnpj: string;
}

export interface IPreRegisterForm {
  nome: string;
  cargo: string;
  email: string;
  telefone: string;
  whatsapp: string;
  endereco: string;
  comoFicouSabendoDoPrograma: string;
  comoFicouSabendoDoProgramaOutros: string;
  regulamentoAceito: boolean;
}

export enum IUserRole {
  Participante = 0,
  Curador = 1,
  Adm = 2
}
