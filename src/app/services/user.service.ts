import { IOrganization } from './../models/organization';
import { Router } from '@angular/router';
import { SessionService } from './session.service';
import { SnackbarType } from './../models/snackbar-type.enum';
import { SnackbarService } from './snackbar.service';
import { AuthService } from 'src/app/services/auth.service';
import { IPreRegisterForm, IUser, IUserRole } from './../models/user';
import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  IUserRole = IUserRole;

  constructor(
    private _authService: AuthService,
    private _db: AngularFirestore,
    private _snackbarService: SnackbarService,
    private _sessionService: SessionService,
    private _router: Router
  ) {}

  async updateUser(user: IPreRegisterForm) {
    const id = this._authService.getCurrentUser().userId;

    try {
      await this._db
        .collection('users')
        .doc<IUser>(id)
        .update({ ...user });

      this._snackbarService.openSnackBar(
        'Usuário atualizado com sucesso!',
        SnackbarType.Success
      );

      const firstLogin = this._sessionService.getSessionFirstLogin();

      if (firstLogin) {
        this._sessionService.setSessionFirstLogin(false);
        this._router.navigateByUrl('/home');
      }

      return { success: true };
    } catch (err) {
      if (err instanceof Error) {
        const message = err.message || 'Erro ao atualizar usuário';

        console.error(message);

        this._snackbarService.openSnackBar(message, SnackbarType.Danger);
      }

      return { success: false };
    }
  }

  async getUsers() {
    try {
      const usersRef = await this._db
        .collection<IUser>('users')
        .get()
        .toPromise();

      const rawUsers = usersRef.docs.map((el) => el.data());

      const users = rawUsers.filter((el) => el.role !== IUserRole.Adm);

      users.forEach(async el => {
        const orgRef = await this._db.collection('organizations').doc<IOrganization>(el.organizationId).get().toPromise();

        const orgName = orgRef.data()?.dadosCadastrais?.nomeInstituicao || 'Indefinido';

        el.organizationName = orgName;
      })

      return users;
    } catch (err) {
      if (err instanceof Error) {
        const message = err.message || 'Erro ao atualizar usuário';

        console.error(message);

        this._snackbarService.openSnackBar(message, SnackbarType.Danger);
      }

      return [];
    }
  }

  createCurator() {
    
  }
}
