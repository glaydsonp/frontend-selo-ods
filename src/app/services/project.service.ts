import {
  IAlbum,
  IArtigo,
  IEvento,
  IObjetivo,
  IProject,
  IPublico,
  ISelo,
  IVideo
} from './../models/project';
import { AuthService } from './auth.service';
import { Router } from '@angular/router';
import { SnackbarType } from './../models/snackbar-type.enum';
import { Injectable, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { SnackbarService } from './snackbar.service';
import {
  FormControl,
  FormBuilder,
  Validators,
  FormGroup,
  FormArray
} from '@angular/forms';
import { IUser } from '../models/user';
import { IOrganization } from '../models/organization';
import { ProjectStatusEnum } from '../models/project-status.enum';

@Injectable({
  providedIn: 'root'
})
export class ProjectService implements OnInit {
  projectId = '';
  userId = '';
  selosOds: ISelo[] = [];
  finalidadesSelosOds: any = [];
  form = new BehaviorSubject(new FormGroup({}));
  currentForm = '';
  updatingForm = new BehaviorSubject(new FormGroup({}));

  objetivos: IObjetivo;
  publicos: IPublico[];
  albuns: IAlbum[];
  videos: IVideo[];
  eventos: IEvento[];
  artigos: IArtigo[];

  ProjectStatusEnum = ProjectStatusEnum;

  eixosProjeto = ['Ensino', 'Pesquisa', 'Extensão', 'Gestão'];

  valoresInvestimento = [
    'Próprio',
    'Agência de Fomento (CNPq/FAPESP/FAPERJ/etc)',
    'Fundos de Investimento Privado',
    'Outro'
  ];

  constructor(
    private _db: AngularFirestore,
    private _snackbarService: SnackbarService,
    private _router: Router,
    private _authService: AuthService,
    private _fb: FormBuilder
  ) {
    this.getSelosOds();

    this.userId = _authService.getCurrentUser().userId;
  }

  ngOnInit() {}

  getAll(seeAll: boolean): Observable<IProject[]> {
    return this._db
      .collection<IProject>('projects')
      .snapshotChanges()
      .pipe(
        map((actions) => {
          // return actions
          //   .map((value) => {
          //     let data = value.payload.doc.data();

          //     let id = value.payload.doc.id;

          //     return { ...data, id };
          //   })
          //   .filter((el) => el.owner === this.userId);

          const projects = actions.map((value) => {
            let data = value.payload.doc.data();

            let id = value.payload.doc.id;

            return { ...data, id };
          });

          return seeAll
            ? projects
            : projects.filter((el) => el.owner === this.userId);
        })
      );
  }

  getFinalidadesSelos() {
    return this.finalidadesSelosOds;
  }

  setInitialInfo(project: IProject) {
    this.projectId = project.id;

    this.finalidadesSelosOds = project.geral?.finalidadesSelosOds || [];
    this.objetivos = project.objetivos;
    this.publicos = project.publico?.publicosAdicionados || [];
    this.albuns = project.albuns || [];
    this.videos = project.videos || [];
    this.eventos = project.eventos || [];
    this.artigos = project.artigos || [];
    // this.finalidadesSelosOds = selosOds;
  }

  async createProject() {
    const ownerOrganizationIdRef = await this._db
      .collection('users')
      .doc<IUser>(this.userId)
      .get()
      .toPromise();

    const ownerOrganizationId = ownerOrganizationIdRef.data()?.organizationId;

    const ownerOrganizationNameRef = await this._db
      .collection('organizations')
      .doc<IOrganization>(ownerOrganizationId)
      .get()
      .toPromise();

    const ownerOrganizationName = ownerOrganizationNameRef.data()
      ?.dadosCadastrais?.nomeInstituicao as string;

    return this._db
      .collection('projects')
      .add({
        owner: this.userId,
        ownerOrganizationId,
        ownerOrganizationName,
        status: ProjectStatusEnum['Em edição'],
        curador1: null,
        curador2: null
      })
      .then((project) => {
        project.update({ id: project.id });
        this._snackbarService.openSnackBar(
          'Projeto criado com sucesso!',
          SnackbarType.Danger
        );

        this._router.navigate([`/meus-projetos/${project.id}`]);

        return { success: true, id: project.id };
      })
      .catch((err) =>
        this._snackbarService.openSnackBar(err.message, SnackbarType.Danger)
      );
  }

  async getSelosOds() {
    this.selosOds = await this._db
      .collection<ISelo>('ods')
      .get()
      .toPromise()
      .then((ods) => {
        return ods.docs.map((el) => el.data());
      });
  }

  async updateProject(projectId: string) {
    const form = this.updatingForm.value;

    this.form.next(form);

    if (form.get(this.currentForm)?.valid || this.currentForm === 'geral') {
      const formulario = form.value;

      try {
        await this._db
          .collection('projects')
          .doc<IProject>(projectId)
          .update({ ...formulario })
          .then(() => {
            this._snackbarService.openSnackBar(
              'Projeto atualizada com sucesso!',
              SnackbarType.Success
            );
          })
          .catch((err) => {
            this._snackbarService.openSnackBar(
              err.message,
              SnackbarType.Danger
            );
          });

        return { success: true };
      } catch (err) {
        if (err instanceof Error) {
          const message = err.message || 'Erro ao atualizar o projeto.';

          console.error(message);

          this._snackbarService.openSnackBar(message, SnackbarType.Danger);
        }

        return { success: false };
      }
    } else {
      this._snackbarService.openSnackBar(
        'Fomrulário inválido!',
        SnackbarType.Danger
      );
      return { success: false };
    }
  }

  initializeForm(form: FormGroup) {
    this.form.next(form);
  }

  buildSelosOds() {
    const values = this.selosOds.map((v) => {
      if (
        this.finalidadesSelosOds &&
        this.finalidadesSelosOds.some((el: any) => el.id === v.id)
      ) {
        return this._fb.group({
          id: v.id,
          url: v.url,
          checked: true,
          descricao: v.descricao,
          comoResultadoFoiPercebido: [
            v.comoResultadoFoiPercebido,
            Validators.required
          ],
          quemBeneficiouResultado: [
            v.quemBeneficiouResultado,
            Validators.required
          ],
          contextoQueAconteceu: [v.contextoQueAconteceu, Validators.required],
          resultadoPercebido: [v.resultadoPercebido, Validators.required],
          nome: v.nome,
          validado: v.validado,
          ideiaSugerida: v.ideiaSugerida
        });
      } else
        return this._fb.group({
          id: v.id,
          url: v.url,
          checked: false,
          descricao: v.descricao,
          comoResultadoFoiPercebido: v.comoResultadoFoiPercebido,
          quemBeneficiouResultado: v.quemBeneficiouResultado,
          contextoQueAconteceu: v.contextoQueAconteceu,
          resultadoPercebido: v.resultadoPercebido,
          nome: v.nome,
          validado: v.validado,
          ideiaSugerida: v.ideiaSugerida
        });
    });

    return this._fb.array(values);
  }

  buildForm() {
    return this._fb.group({
      geral: this._fb.group({
        nome: [null, Validators.required],
        eixosProjeto: this.buildEixosProjeto(),
        tipoProjeto: [null, Validators.required],
        finalidadesSelosOds: this.buildSelosOds()
      }),
      estudoRealidade: [null, Validators.required],
      justificativa: [null, Validators.required],
      objetivos: this._fb.group({
        objetivoGeral: [null, Validators.required],
        objetivosEspecificos: this.buildObjetivosEspecificos()
      }),
      publico: this._fb.group({
        pessoasImpactadasDiretamente: [0, Validators.required],
        pessoasImpactadasIndiretamente: [0, Validators.required],
        mulheresImpactadas: [0, Validators.required],
        pessoasComDeficienciaImpactadas: [0, Validators.required],
        impactadasIgualdadeSocial: [0, Validators.required],
        pessoasIdosasImpactadas: [0, Validators.required],
        criancaAdolescentesImpactados: [0, Validators.required],
        pessoasLGBTQIAImpactadas: [0, Validators.required],
        pessoasAtendidasProgramasContraMiseria: [0, Validators.required],
        pessoasAtendidasProgramasContraFome: [0, Validators.required],
        pessoasAtendidasProgramasContraCancer: [0, Validators.required],
        pessoasAtendidasProgramasEstimuloEsporte: [0, Validators.required],
        publicosAdicionados: this.buildPublicosAdicionados()
      }),
      resumo: [null, Validators.required],
      investimentos: this._fb.group({
        exibirInvestimentosSite: [null, Validators.required],
        valorInvestimento: this.buildValoresInvestimento(),
        valorReais: [null, Validators.required],
        docentesEnvolvidos: [null, Validators.required],
        servidoresTecnico: [null, Validators.required],
        projetoPromoveBolsas: [null, Validators.required],
        bolsasIniciacaoCientifica: [null],
        bolsasExtensao: [null],
        bolsasMestrado: [null],
        bolsasDoutorado: [null]
      }),
      albuns: this.buildAlbuns(),
      videos: this.buildVideos(),
      artigos: this.buildArtigos(),
      eventos: this.buildEventos()
    });
  }

  buildEixosProjeto() {
    const values = this.eixosProjeto.map((v) => new FormControl(false));

    return this._fb.array(values);
  }

  buildAlbuns() {
    const albuns = this.albuns;

    if (albuns && albuns.length > 0) {
      const values = albuns.map((el) =>
        this._fb.group({
          nomeAlbum: new FormControl(el.nomeAlbum),
          legenda: new FormControl(el.legenda),
          imagesUrl: new FormArray(
            el.imagesUrl.map((image) => new FormControl(image))
          )
        })
      );

      return this._fb.array(values);
    } else {
      return this._fb.array([]);
    }
  }

  buildVideos() {
    const videos = this.videos;

    if (videos && videos.length > 0) {
      const values = videos.map((el) =>
        this._fb.group({
          nomeVideo: new FormControl(el.nomeVideo),
          legenda: new FormControl(el.legenda),
          linkVideo: new FormControl(el.linkVideo)
        })
      );

      return this._fb.array(values);
    } else {
      return this._fb.array([]);
    }
  }

  buildArtigos() {
    const artigos = this.artigos;

    if (artigos && artigos.length > 0) {
      const values = artigos.map((el) =>
        this._fb.group({
          titulo: new FormControl(el.titulo),
          linkAcesso: new FormControl(el.linkAcesso)
        })
      );

      return this._fb.array(values);
    } else {
      return this._fb.array([]);
    }
  }

  buildEventos() {
    const eventos = this.eventos;

    if (eventos && eventos.length > 0) {
      const values = eventos.map((el) =>
        this._fb.group({
          nomeEvento: new FormControl(el.nomeEvento),
          descricao: new FormControl(el.descricao),
          formato: new FormControl(el.formato),
          linkCobertura: new FormControl(el.linkCobertura),
          numeroParticipantes: new FormControl(el.numeroParticipantes)
        })
      );

      return this._fb.array(values);
    } else {
      return this._fb.array([]);
    }
  }

  buildValoresInvestimento() {
    const values = this.valoresInvestimento.map((v) => new FormControl(false));

    return this._fb.array(values);
  }

  buildObjetivosEspecificos() {
    const objetivos = this.objetivos;

    if (objetivos && objetivos.objetivosEspecificos.length > 0) {
      const values = objetivos.objetivosEspecificos.map((el) =>
        this._fb.group({
          nomeObjetivo: new FormControl(el.nomeObjetivo),
          planosDeAcao: new FormArray(
            el.planosDeAcao.map(
              (plano) =>
                new FormGroup({
                  descricao: new FormControl(plano.descricao),
                  realizado: new FormControl(plano.realizado),
                  justificativa: new FormControl(plano.justificativa)
                })
            )
          )
        })
      );

      return this._fb.array(values);
    } else {
      return this._fb.array([]);
    }
  }

  buildPublicosAdicionados() {
    const publicos = this.publicos;

    if (publicos && publicos.length > 0) {
      const values = publicos.map((el) =>
        this._fb.group({
          nomePublico: el.nomePublico,
          descricaoPublico: el.descricaoPublico,
          ambitoImpactoGerado: el.ambitoImpactoGerado,
          tipoDeImpacto: el.tipoDeImpacto,
          endereco: el.endereco
        })
      );

      return this._fb.array(values);
    } else {
      return this._fb.array([]);
    }

    // return this._fb.array([]);
  }

  setUpdatingForm(form: FormGroup, currentForm: string) {
    this.updatingForm.next(form);
    this.currentForm = currentForm;
  }

  getForm() {
    return this.form;
  }
}
