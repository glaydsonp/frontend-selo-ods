import { environment } from 'src/environments/environment';
import { Injectable } from '@angular/core';
import { ISession } from '../models/auth';
import firebase from 'firebase/compat/app';

@Injectable({
  providedIn: 'root'
})
export class SessionService {
  constructor() {}

  setSession(session: ISession): void {
    const stringSession = JSON.stringify(session);

    sessionStorage.setItem(
      `${environment.sessionStoragePrefix}_session`,
      stringSession
    );
  }

  setSessionCustomItem(item: string, value: any) {
    const valueStringfied = JSON.stringify(value);

    sessionStorage.setItem(
      `${environment.sessionStoragePrefix}_${item}`,
      valueStringfied
    );
  }

  getSessionCustomItem(item: string) {
    const sessionStorageItem = sessionStorage.getItem(item) as string;

    const parseValue = JSON.parse(sessionStorageItem);

    return parseValue;
  }

  getOrganizationId() {
    const sessionStorageItem = sessionStorage.getItem(
      `${environment.sessionStoragePrefix}_organization_id`
    ) as string;

    const parseValue = JSON.parse(sessionStorageItem);

    return parseValue;
  }

  getSession() {
    const sessionStorageItem = sessionStorage.getItem(
      `${environment.sessionStoragePrefix}_session`
    );

    const session = sessionStorageItem
      ? (JSON.parse(sessionStorageItem) as ISession)
      : '';

    return session;
  }

  getSessionFirstLogin() {
    const sessionFirstLogin = sessionStorage.getItem(
      `${environment.sessionStoragePrefix}_firstLogin`
    );

    const firstLoginInfo = sessionFirstLogin
      ? (JSON.parse(sessionFirstLogin) as ISession)
      : '';

    return firstLoginInfo;
  }

  setSessionFirstLogin(firstLogin: boolean): void {
    sessionStorage.setItem(
      `${environment.sessionStoragePrefix}_firstLogin`,
      `${firstLogin}`
    );
  }

  isCuratorRef() {
    const sessionStorageItem = sessionStorage.getItem(
      `${environment.sessionStoragePrefix}_curator_ref`
    ) as string;

    const parseValue = JSON.parse(sessionStorageItem);

    return parseValue;
  }

  reset() {
    sessionStorage.clear();
  }
}
