import { AuthService } from './auth.service';
import { BehaviorSubject, Subject } from 'rxjs';
import { IObjetivo, IProject, IProjectCurador } from 'src/app/models/project';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { Injectable } from '@angular/core';
import { CuradorStatusEnum } from '../models/project-status.enum';
import { IUser } from '../models/user';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CuradoriaService {
  projetoCurador: Subject<IProjectCurador> = new Subject<IProjectCurador>();
  CuradorStatusEnum = CuradorStatusEnum;

  constructor(
    private _db: AngularFirestore,
    private _authService: AuthService
  ) {}

  async getProjectById(id: string) {
    const projectRef = await this._db
      .collection('projects')
      .doc<IProject>(id)
      .get()
      .toPromise();

    const project = projectRef.data() as IProject;

    const objetivos: IObjetivo & { justificativa?: string } = project.objetivos;
    objetivos.justificativa = project?.justificativa;

    const projectCurador: IProjectCurador = {
      objetivos,
      ods: project?.geral.finalidadesSelosOds
    };

    this.projetoCurador.next(projectCurador);

    return projectCurador;
  }

  getProjectCurador() {
    return this.projetoCurador;
  }

  async updateProjectOds(projectId: string, odsValue: any) {
    const projetoRef = await this._db
      .collection('projects')
      .doc<IProject>(projectId)
      .get()
      .toPromise();

    const projeto = projetoRef.data() as IProject;

    projeto.geral.finalidadesSelosOds.forEach((el) => {
      odsValue.ods.forEach((ods: any) => {
        if (el.id === ods.id) {
          el.validado = ods.validado;
          el.ideiaSugerida = ods.ideiaSugerida;
        }
      });
    });

    const curador1 = projeto.curador1;
    const curador2 = projeto.curador2;

    const curador = this._authService.getCurrentUser();

    if (
      curador1?.curadorId !== curador?.userId &&
      curador2?.curadorId !== curador?.userId
    ) {
      if (!curador1) {
        if (!odsValue.ods.some((el: any) => !el.validado)) {
          projeto.curador1 = {
            curadorId: curador?.userId,
            statusCuradoria: CuradorStatusEnum.Confirmado
          };
        } else {
          projeto.curador1 = {
            curadorId: curador?.userId,
            statusCuradoria: CuradorStatusEnum['Em avaliação']
          };
        }
      } else if (curador1 && !curador2) {
        if (!odsValue.ods.some((el: any) => !el.validado)) {
          projeto.curador2 = {
            curadorId: curador?.userId,
            statusCuradoria: CuradorStatusEnum.Confirmado
          };
        } else {
          projeto.curador2 = {
            curadorId: curador?.userId,
            statusCuradoria: CuradorStatusEnum['Em avaliação']
          };
        }
      }
    }

    // const finalidadesSelosOds = ods.value;

    return await this._db
      .collection('projects')
      .doc<IProject>(projectId)
      .update({ ...projeto });
  }

  getCuratorPartners(userId: string) {
    return this._db
      .collection<IUser>('users')
      .snapshotChanges()
      .pipe(
        map((actions) => {
          const users = actions.map((value) => {
            let data = value.payload.doc.data();

            let id = value.payload.doc.id;

            return { ...data, id };
          });

          return users.find(el => el.id === userId);
        })
      );
  }
}
