import { IOrganization } from 'src/app/models/organization';
import { AuthService } from './auth.service';
import { FormGroup, FormBuilder, Validators, FormControl, FormArray } from '@angular/forms';
import { SnackbarService } from './snackbar.service';
import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { SnackbarType } from '../models/snackbar-type.enum';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { IProject } from '../models/project';

@Injectable({
  providedIn: 'root'
})
export class OrganizationService {
  form = new BehaviorSubject(new FormGroup({}));
  updatingForm = new BehaviorSubject(new FormGroup({}));
  formBuilder = new FormGroup({});
  currentForm = '';
  areasAtuacao: string[] = ['Humanas', 'Biológicas', 'Exatas', 'Tecnológicas'];
  selosOds: { id: string; url: string }[] = [];
  organizationSelosOds: any = [];
  organizationSelosOdsLocal: any = [];
  setores = ['Público', 'Privado', 'SCO'];

  projetosComColaboradoresOds = [];
  projetosComunidadeLocalOds = [];

  finalidades = [
    'Ações de voluntariado',
    'Ações internas entre colaboradores',
    'Arrecadação de recursos para causas',
    'Atendimento direto, intervenções',
    'Consultorias, parcerias, apoio logístico à organizações',
    'Doação, patrocínio, apoio financeiro',
    'Estímulo ao Empreendedorismo Social',
    'Mobilização, mutirão, cooperação',
    'Palestras, oficinas, workshops',
    'Participação ou promoção de campanhas temáticas e conscientização',
    'Promoção de atividades físicas, culturais, interação com o meio ambiente',
    'Outros'
  ];

  constructor(
    private _db: AngularFirestore,
    private _snackbarService: SnackbarService,
    private _fb: FormBuilder,
    private _authService: AuthService
  ) {
    this.getSelosOds();
  }

  getAll(): Observable<IOrganization[]> {
    return this._db
      .collection<IOrganization>('organizations')
      .snapshotChanges()
      .pipe(
        map((actions) => {
          // return actions
          //   .map((value) => {
          //     let data = value.payload.doc.data();

          //     let id = value.payload.doc.id;

          //     return { ...data, id };
          //   })
          //   .filter((el) => el.owner === this.userId);

          const organizations = actions.map((value) => {
            let data = value.payload.doc.data();

            let id = value.payload.doc.id;

            return { ...data, id };
          });

          return organizations;
        })
      );
  }

  async updateOrganization(organizationId: string) {
    const form = this.updatingForm.value;
    this.form.next(form);

    if (form.get(this.currentForm)?.valid) {
      const formulario = form.value;

      if (this.currentForm === 'projetos') {
        formulario.projetos.projetosComColaboradoresOds = this.projetosComColaboradoresOds || [];

        formulario.projetos.projetosComunidadeLocalOds = this.projetosComunidadeLocalOds || [];
      }

      try {
        await this._db
          .collection('organizations')
          .doc<IOrganization>(organizationId)
          .update({ ...formulario });

        if (this.currentForm === 'projetos') {
          this.getSelosOds();
          this.buildForm();
        }

        this._snackbarService.openSnackBar('Organização atualizada com sucesso!', SnackbarType.Success);

        return { success: true };
      } catch (err) {
        if (err instanceof Error) {
          const message = err.message || 'Erro ao atualizar a organização.';

          console.error(message);

          this._snackbarService.openSnackBar(message, SnackbarType.Danger);
        }

        return { success: false };
      }
    } else {
      this._snackbarService.openSnackBar('Fomrulário inválido!', SnackbarType.Danger);
      return { success: false };
    }
  }

  initializeForm(form: FormGroup) {
    this.form.next(form);
  }

  getForm() {
    return this.form;
  }

  setForm(form: FormGroup) {
    this.form.next(form);
  }

  setUpdatingForm(form: FormGroup, currentForm: string) {
    this.updatingForm.next(form);
    this.currentForm = currentForm;
  }

  buildForm() {
    return this._fb.group({
      dadosCadastrais: this._fb.group({
        cnpj: [null, Validators.required],
        nomeInstituicao: [null, Validators.required],
        setor: [null, Validators.required],
        abrangencia: [null, Validators.required]
      }),
      endereco: this._fb.group({
        cep: [null, Validators.required],
        longradouro: [null, Validators.required],
        numero: [null, Validators.required],
        complemento: [null],
        cidade: [null, Validators.required],
        bairro: [null, Validators.required],
        estado: [null, Validators.required]
      }),
      pessoal: this._fb.group({
        representanteLegal: this._fb.group({
          nomeCompleto: [null, Validators.required],
          cpf: [null, Validators.required],
          ocupacaoInstituicao: [null, Validators.required],
          telefone: [null, Validators.required],
          whatsapp: [null, Validators.required],
          emailContato: [null, Validators.required],
          emailOpcional: [null]
        }),
        representanteDaPagina: this._fb.group({
          nomeCompleto: [null],
          cpf: [null],
          ocupacaoInstituicao: [null],
          telefone: [null],
          whatsapp: [null],
          emailContato: [null],
          emailOpcional: [null],
          usarDadosPreCadastro: [null]
        })
      }),
      perfil: this._fb.group({
        areasAtuacao: this.buildAreasAtuacao(),
        setor: [null, Validators.required],
        odsFazPartePdi: [null, Validators.required],
        existeComite: [null, Validators.required],
        participaRede: [null, Validators.required],
        participaRedeQual: [null],
        entradaRanking: [null, Validators.required]
      }),
      projetos: this._fb.group({
        projetosComColaboradores: [null, Validators.required],
        projetosComColaboradoresOds: this.buildSelosOds(),
        projetosComunidadeLocal: [null, Validators.required],
        projetosComunidadeLocalOption: [null, Validators.required],
        projetosComunidadeLocalOds: this.buildSelosOdsLocal(),
        lugarProjetoSocial: [null, Validators.required],
        parceriaComOutrasOrganizacoes: [null, Validators.required],
        setores: this.buildSetores(),
        finalidades: this.buildFinalidades()
      }),
      expectativa: this._fb.group({
        aumentarRedeComunicacao: [null, Validators.required],
        conhecerOutrasOrganizacoes: [null, Validators.required],
        divulgarOTrabalhoOrganizacao: [null, Validators.required],
        integrarGrupoSocialLocal: [null, Validators.required],
        serCertificadaPeloCompromisso: [null, Validators.required]
      }),
      comunicacao: this._fb.group({
        breveRelease: [null, Validators.required],
        razaoDeSerDaOrganizacao: [null, Validators.required],
        propostaOrganizacao: [null, Validators.required],
        principiosOrganizacao: [null, Validators.required],
        assessoria: this._fb.group({
          nomeCompleto: [null, Validators.required],
          cpf: [null, Validators.required],
          ocupacao: [null, Validators.required],
          telefone: [null, Validators.required],
          whatsapp: [null, Validators.required],
          emailContato: [null, Validators.required],
          emailOpcional: [null]
        }),
        midiasSociais: this._fb.group({
          site: [null, Validators.required],
          facebook: [null, Validators.required],
          twitter: [null, Validators.required],
          instagram: [null, Validators.required]
        })
      })
    });
  }

  buildAreasAtuacao() {
    const values = this.areasAtuacao.map((v) => new FormControl(false));

    return this._fb.array(values);
  }

  buildSetores() {
    const values = this.setores.map((v) => new FormControl(false));

    return this._fb.array(values);
  }

  buildFinalidades() {
    const values = this.finalidades.map((v) => new FormControl(false));

    return this._fb.array(values);
  }

  buildSelosOds() {
    const values = this.selosOds.map((v) => {
      if (this.organizationSelosOds?.some((el: any) => el.id === v.id)) {
        return new FormControl(true);
      } else return new FormControl(false);
    });

    return this._fb.array(values);
  }

  buildSelosOdsLocal() {
    const values = this.selosOds.map((v) => {
      if (this.organizationSelosOdsLocal?.some((el: any) => el.id === v.id)) {
        return new FormControl(true);
      } else return new FormControl(false);
    });

    return this._fb.array(values);
  }

  getSelosOds() {
    const organizationId = this._authService.getOrganizationId() || ' ';

    this._db
      .collection<{ id: string; url: string }>('ods')
      .get()
      .subscribe((ods) => {
        this.selosOds = ods.docs.map((el) => el.data());
      });

    this._db
      .collection('organizations')
      .doc<IOrganization>(organizationId)
      .get()
      .subscribe((el) => {
        this.organizationSelosOds = el.data()?.projetos?.projetosComColaboradoresOds;
      });

    this._db
      .collection('organizations')
      .doc<IOrganization>(organizationId)
      .get()
      .subscribe((el) => {
        this.organizationSelosOdsLocal = el.data()?.projetos?.projetosComunidadeLocalOds;
      });
  }

  updateSelos(selos: any) {
    this.projetosComColaboradoresOds = selos;
  }

  updateSelosLocal(selos: any) {
    this.projetosComunidadeLocalOds = selos;
  }

  checkOrganizationCnpj(cnpj: string): Promise<{ cnpjExists: boolean; organizationId: string }> {
    return new Promise(async (resolve, reject) => {
      const organizationsRef = await this._db.collection('organizations').get().toPromise();

      const organizations = organizationsRef.docs.map((el) => el.data()) as IOrganization[];

      resolve({
        cnpjExists: organizations.some((el) => el?.dadosCadastrais?.cnpj === cnpj),
        organizationId: organizations.find((el) => el?.dadosCadastrais?.cnpj === cnpj)?.id as string
      });
    });
  }
}
