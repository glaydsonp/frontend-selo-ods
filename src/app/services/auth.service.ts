import { OrganizationService } from 'src/app/services/organization.service';
import { SnackbarType } from './../models/snackbar-type.enum';
import { Router } from '@angular/router';
import { SessionService } from './session.service';
import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import firebase from 'firebase/compat/app';
import {
  AngularFirestore,
  AngularFirestoreDocument
} from '@angular/fire/compat/firestore';
import { ILoginForm, ISession } from '../models/auth';
import { IUser } from '../models/user';
import { SnackbarService } from './snackbar.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  constructor(
    public auth: AngularFireAuth,
    private _sessionService: SessionService,
    private db: AngularFirestore,
    private _router: Router,
    private _snackbarService: SnackbarService
  ) {}

  login(form: ILoginForm) {
    this.auth
      .signInWithEmailAndPassword(form.email, form.senha)
      .then((res) => {
        if (res && res.user) {
          res.user.getIdTokenResult().then(async (token) => {
            if (!token.claims) {
              return;
            }

            const userData = await this.db
              .collection('users')
              .doc<IUser>(token.claims.user_id)
              .get()
              .toPromise();

            let user = userData.data() as IUser;

            const organizationId =
              user?.organizationId ??
              (await this.createOrganization(token.claims.user_id));

            if (!user) {
              await this.db
                .collection('users')
                .doc(token.claims.user_id)
                .set({ id: token.claims.user_id, organizationId })
                .then(async () => {
                  const userData = await this.db
                    .collection('users')
                    .doc<IUser>(token.claims.user_id)
                    .get()
                    .toPromise();

                  user = userData.data() as IUser;
                });
            }

            !user?.lastLogin
              ? this._sessionService.setSessionFirstLogin(true)
              : this._sessionService.setSessionFirstLogin(false);

            const session: ISession = {
              email: token.claims.email,
              exp: token.claims.exp,
              userId: token.claims.user_id,
              expirationTime: token.expirationTime,
              authTime: token.authTime,
              token: token.token,
              lastLogin: user?.lastLogin,
              role: user.role
            };

            this.db.collection('users').doc(session.userId).update({
              lastLogin: Date.now(),
              organizationId
            });

            this._sessionService.setSession(session);

            this._sessionService.setSessionCustomItem(
              'curator_ref',
              user.curatorRef || false
            );

            this._sessionService.setSessionCustomItem(
              'organization_id',
              organizationId
            );

            const firstLogin = this._sessionService.getSessionFirstLogin();

            firstLogin
              ? this._router.navigate(['/boas-vindas'])
              : this._router.navigate(['/home']);
          });
        }
      })
      .catch((err) => {
        console.error(err);

        if (err.message.includes('user record')) {
          this._snackbarService.openSnackBar(
            'Email e/ou senha inválidos.',
            SnackbarType.Danger
          );
        } else {
          this._snackbarService.openSnackBar(err.message, SnackbarType.Danger);
        }
      });
  }

  async createOrganization(id: string) {
    return await this.db
      .collection('organizations')
      .add({ owner: id })
      .then((added) => {
        added.update({ id: added.id });

        return added.id;
      });
  }

  generateId() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, (c) => {
      // tslint:disable-next-line: no-bitwise
      const r = (Math.random() * 16) | 0;
      // tslint:disable-next-line: no-bitwise
      const v = c === 'x' ? r : (r & 0x3) | 0x8;
      return v.toString(16);
    });
  }

  isAuthenticated(): boolean {
    const sessao = this._sessionService.getSession() as ISession;

    if (sessao && sessao.expirationTime) {
      const expires = new Date(sessao.expirationTime);
      const now = new Date();

      if (expires > now) {
        return true;
      }
    }

    this._sessionService.reset();

    return false;
  }

  getCurrentUser() {
    const sessao = this._sessionService.getSession() as ISession;
    return sessao;
  }

  getUserRole() {
    const sessao = this._sessionService.getSession() as ISession;
    return sessao.role;
  }

  getOrganizationId() {
    return this._sessionService.getOrganizationId();
  }

  async getUserName() {
    const session: ISession = this.getCurrentUser();

    return (
      await this.db
        .collection('users')
        .doc<IUser>(session.userId)
        .get()
        .toPromise()
    ).data();
  }
}
