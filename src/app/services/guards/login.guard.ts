import { SessionService } from './../session.service';
import { AuthService } from 'src/app/services/auth.service';
import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
  UrlTree
} from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoginGuard implements CanActivate {
  constructor(
    private _authService: AuthService,
    private _router: Router,
    private _sessionService: SessionService
  ) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    const isAuth = this._authService.isAuthenticated();

    const firstLogin = this._sessionService.getSessionFirstLogin();

    if (!isAuth) {
      return true;
    } else {
      firstLogin
        ? this._router.navigate(['/boas-vindas'])
        : this._router.navigate(['/home']);

      return false;
    }
  }
}
