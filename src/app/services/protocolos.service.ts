import { AuthService } from './auth.service';
import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { IProtocolo } from '../models/protocolos';

@Injectable({
  providedIn: 'root'
})
export class ProtocolosService {
  userId = '';

  constructor(private _db: AngularFirestore, private _authService: AuthService) {
    this.userId = _authService.getCurrentUser().userId;
  }

  getAll(seeAll: boolean): Observable<IProtocolo[]> {
    return this._db
      .collection<IProtocolo>('protocols')
      .snapshotChanges()
      .pipe(
        map((actions) => {
          const protocolos = actions.map((value) => {
            let data = value.payload.doc.data();

            let id = value.payload.doc.id;

            return { ...data, id };
          });

          return seeAll ? protocolos : protocolos.filter((el) => el.owner === this.userId);
        })
      );
  }
}
