import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-non-user-header',
  templateUrl: './non-user-header.component.html',
  styleUrls: ['./non-user-header.component.scss']
})
export class NonUserHeaderComponent implements OnInit {
  title = '';

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private location: Location
  ) {
    const pathName = this.location.path();
    const urlName = pathName.substring(1, pathName.length);

    const thisConfigRoute = this.router.config.find(
      (route) => route.path === urlName
    );

    this.title = thisConfigRoute?.data?.title || '';
  }
  ngOnInit(): void {}
}
