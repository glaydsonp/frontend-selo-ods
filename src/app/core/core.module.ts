import { SharedModule } from './../shared/shared.module';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { CoreRoutingModule } from './core-routing.module';
import { CoreComponent } from './core.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SidebarComponent } from './sidebar/sidebar.component';
import { HeaderComponent } from './header/header.component';
import { NonUserHeaderComponent } from './non-user-header/non-user-header.component';
import { MAT_RADIO_DEFAULT_OPTIONS } from '@angular/material/radio';
// import { initializeApp } from 'firebase/app';
import { provideFirebaseApp, getApp, initializeApp } from '@angular/fire/app';
import { getFirestore, provideFirestore } from '@angular/fire/firestore';
import { environment } from 'src/environments/environment';
import { AngularFireModule } from '@angular/fire/compat';
import { InterceptorModule } from '../services/interceptor.module';
import { AngularFireStorageModule, BUCKET } from '@angular/fire/compat/storage';
import { NgxMaskModule } from 'ngx-mask';

@NgModule({
  declarations: [
    CoreComponent,
    SidebarComponent,
    HeaderComponent,
    NonUserHeaderComponent
  ],
  imports: [
    BrowserModule,
    CoreRoutingModule,
    BrowserAnimationsModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    InterceptorModule,
    SharedModule,
    NgxMaskModule.forRoot()
  ],
  providers: [
    {
      provide: MAT_RADIO_DEFAULT_OPTIONS,
      useValue: { color: 'primary' }
    },
    { provide: BUCKET, useValue: environment.firebaseConfig.storageBucket }
  ],
  bootstrap: [CoreComponent]
})
export class CoreModule {
  // app = initializeApp(this.firebaseConfig);
}
