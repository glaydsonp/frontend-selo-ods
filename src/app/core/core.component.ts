import { SessionService } from './../services/session.service';
import { ChangeDetectorRef, Component } from '@angular/core';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { LoaderService } from '../services/loader.service';

@Component({
  selector: 'core-root',
  templateUrl: './core.component.html',
  styleUrls: ['./core.component.scss']
})
export class CoreComponent {
  isLoading: Subject<boolean> = this.loaderService.isLoading;

  title = 'frontend-selo-ods';

  constructor(
    public loaderService: LoaderService,
    private cdr: ChangeDetectorRef,
    private router: Router,
    public sessionService: SessionService
  ) {
    router.events.subscribe(() => {
      this.cdr.detectChanges();
    });
  }
}
