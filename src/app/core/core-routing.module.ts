import { ProtocolosRelatoriosModule } from './../pages/protocolos-relatorios/protocolos-relatorios.module';
import { OrganizationsModule } from './../pages/organizations/organizations.module';
import { UsuariosModule } from './../pages/usuarios/usuarios.module';
import { ProjetosAdmModule } from './../pages/projetos-adm/projetos-adm.module';
import { CuradoriaProjetosModule } from './../pages/curadoria-projetos/curadoria-projetos.module';
import { PartnersValidationModule } from './../pages/partners-validation/partners-validation.module';
import { LoginGuard } from './../services/guards/login.guard';
import { AuthGuard } from './../services/guards/auth.guard';
import { OrganizationModule } from './../pages/organization/organization.module';
import { PreRegisterModule } from './../pages/pre-register/pre-register.module';
import { WelcomeModule } from './../pages/welcome/welcome.module';
import { LoginModule } from './../pages/login/login.module';
import { NgModule } from '@angular/core';
import { RouterModule, Routes, CanActivate } from '@angular/router';
import { HomeModule } from '../pages/home/home.module';
import { ProjetosModule } from '../pages/projetos/projetos.module';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  // {
  //   path: '**',
  //   redirectTo: 'home'
  // },
  {
    path: 'home',
    loadChildren: () => HomeModule,
    canActivate: [AuthGuard]
  },
  {
    path: 'login',
    loadChildren: () => LoginModule,
    data: { title: 'Plataforma Selo ODS' },
    canActivate: [LoginGuard]
  },
  {
    path: 'boas-vindas',
    loadChildren: () => WelcomeModule,
    data: { title: 'Boas-vindas' },
    canActivate: [AuthGuard]
  },
  {
    path: 'pre-cadastro',
    loadChildren: () => PreRegisterModule,
    data: { title: 'Cadastro' },
    canActivate: [AuthGuard]
  },
  {
    path: 'meus-projetos',
    loadChildren: () => ProjetosModule,
    data: { title: 'Meus projetos' },
    canActivate: [AuthGuard]
  },
  {
    path: 'projetos',
    loadChildren: () => ProjetosAdmModule,
    data: { title: 'Projetos' },
    canActivate: [AuthGuard]
  },
  {
    path: 'organizacao',
    loadChildren: () => OrganizationModule,
    data: { title: 'Instituição / Cadastro' },
    canActivate: [AuthGuard]
  },
  {
    path: 'organizacoes',
    loadChildren: () => OrganizationsModule,
    data: { title: 'Organizações' },
    canActivate: [AuthGuard]
  },
  {
    path: 'validacao-parceiros',
    loadChildren: () => PartnersValidationModule,
    data: { title: 'Validação de Parceiros' },
    canActivate: [AuthGuard]
  },
  {
    path: 'curadoria-projetos',
    loadChildren: () => CuradoriaProjetosModule,
    data: { title: 'Curadoria de Projetos' },
    canActivate: [AuthGuard]
  },
  {
    path: 'usuarios',
    loadChildren: () => UsuariosModule,
    data: { title: 'Usuários' },
    canActivate: [AuthGuard]
  },
  {
    path: 'protocolos-e-relatorios',
    loadChildren: () => ProtocolosRelatoriosModule,
    data: { title: 'Protocolos e relatórios' },
    canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class CoreRoutingModule {}
