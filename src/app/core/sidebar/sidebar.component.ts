import { AuthService } from 'src/app/services/auth.service';
import { IUser } from './../../models/user';
import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { ISession } from 'src/app/models/auth';
import { doc, onSnapshot } from 'firebase/firestore';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { map } from 'rxjs/operators';

interface IMenuItem {
  title: string;
  icon: string;
  link: string;
  submenu?: { title: string; link: string; param?: string }[];
  role: number[];
}

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
  displayName = '';

  toggleSidebar = false;
  toggled: number[] = [];

  menuItems: IMenuItem[] = [
    {
      title: 'Início',
      icon: '/assets/icons/sidebar_home.svg',
      link: '/home',
      role: [0, 1, 2]
    },
    {
      title: 'Pré-cadastro',
      icon: '/assets/icons/sidebar_precadastro.svg',
      link: '/pre-cadastro',
      role: [0, 1]
    },
    {
      title: 'Boas-vindas',
      icon: '/assets/icons/icone-boas-vindas.svg',
      link: '/boas-vindas',
      role: [0, 1, 2]
    },
    {
      title: 'Meus dados',
      icon: '/assets/icons/sidebar_precadastro.svg',
      link: '/pre-cadastro',
      role: [2]
    },
    {
      title: 'Participantes',
      icon: '/assets/icons/sidebar_company.svg',
      link: '/usuarios/listagem',
      role: [2]
    },
    {
      title: 'Minha organização',
      icon: '/assets/icons/sidebar_company.svg',
      link: '/minha-ogranizacao',
      role: [0, 1],
      submenu: [
        {
          title: 'Cadastro',
          link: '/organizacao/cadastro',
          param: ''
        },
        {
          title: 'Perfil',
          link: '/organizacao/cadastro',
          param: 'perfil'
        },
        {
          title: 'Comunicação',
          link: '/organizacao/cadastro',
          param: 'comunicacoes'
        }
      ]
    },
    {
      title: 'Organizações',
      icon: '/assets/icons/sidebar_projects.svg',
      link: '/organizacoes',
      role: [2]
    },
    {
      title: 'Projetos',
      icon: '/assets/icons/sidebar_projects.svg',
      link: '/projetos',
      role: [2]
    },
    {
      title: 'Meus projetos',
      icon: '/assets/icons/sidebar_projects.svg',
      link: '/meus-projetos',
      role: [0]
    },
    {
      title: 'Parceiros Cadastrados',
      icon: '/assets/icons/sidebar_projects.svg',
      link: '/validacao-parceiros',
      role: [1]
    },
    {
      title: 'Curadoria de projetos',
      icon: '/assets/icons/icone-historico-projetos.svg',
      link: '/curadoria-projetos',
      role: [1]
    },
    // {
    //   title: 'Histórico de projetos',
    //   icon: '/assets/icons/icone-historico-projetos.svg',
    //   link: '/historico-projetos',
    //   role: [0]
    // },
    {
      title: 'Protocolos e relatórios',
      icon: '/assets/icons/icone-protocolos-relatorios.svg',
      link: '/protocolos-e-relatorios',
      role: [0, 1]
    },
    // {
    //   title: 'Ciclos',
    //   icon: '/assets/icons/icone-protocolos-relatorios.svg',
    //   link: '/protocolos-relatorios',
    //   role: [2]
    // },
    // {
    //   title: 'Notícias',
    //   icon: '/assets/icons/icone-noticias.svg',
    //   link: '/noticias',
    //   role: [0, 1]
    // },
    // {
    //   title: 'Emails',
    //   icon: '/assets/icons/icone-noticias.svg',
    //   link: '/noticias',
    //   role: [2]
    // },
    {
      title: 'Relatórios',
      icon: '/assets/icons/icone-mensagens.svg',
      link: '/protocolos-e-relatorios',
      role: [2]
    },
    // {
    //   title: 'Mensagens',
    //   icon: '/assets/icons/icone-mensagens.svg',
    //   link: '/mensagens',
    //   role: [0, 1]
    // }
  ];

  url = '';

  constructor(
    private _router: Router,
    private _authService: AuthService,
    private db: AngularFirestore,
    public authService: AuthService
  ) {
    this._router.events.subscribe((res) => {
      if (res instanceof NavigationEnd) {
        this.url = res.url;
      }
    });
  }

  ngOnInit(): void {
    this.getDisplayName();
  }

  getDisplayName() {
    this._authService.getUserName().then((el) => {
      let id = '';

      if (el && el.id) {
        id = el.id;
      }

      this.db
        .collection('users')
        .doc<IUser>(id)
        .snapshotChanges()
        .subscribe((el) => {
          const data = el.payload.data();

          if (data && data.nome) {
            this.displayName = data?.nome.split(' ')[0];
          }
        });
    });
  }

  toggle(): void {
    this.toggleSidebar = !this.toggleSidebar;
    if (this.toggled.length > 0) {
      this.toggled = [];
    }
  }

  hideSubmenu(index: number) {
    if (!this.toggleSidebar) {
      this.toggleSidebar = true;
      return;
    }

    if (!this.toggled.some((el) => el === index)) {
      this.toggled.push(index);
    } else {
      const ind = this.toggled.findIndex((el) => el === index);
      this.toggled.splice(ind, 1);
    }
  }
}
