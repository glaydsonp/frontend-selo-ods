import { SessionService } from './../../services/session.service';
import { AuthService } from 'src/app/services/auth.service';
import { NavigationEnd, Router } from '@angular/router';
import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { IUser } from 'src/app/models/user';
import { AngularFirestore } from '@angular/fire/compat/firestore';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  displayName = '';
  url = '';
  title = '';
  routePath = ' / ';
  route = '';

  constructor(
    private location: Location,
    private router: Router,
    private _authService: AuthService,
    private db: AngularFirestore,
    public sessionService: SessionService
  ) {
    this.router.events.subscribe((res) => {
      if (res instanceof NavigationEnd) {
        const pathName = this.location.path();
        const urlName = pathName.substring(1, pathName.length);

        console.log(pathName, urlName)

        this.url = urlName.split('/')[0];

        const thisConfigRoute = this.router.config.find((route) => route.path === this.url);

        this.title = thisConfigRoute?.data?.title || '';

        switch (this.title) {
          case 'Boas-vindas':
            this.routePath = ' / ';
            this.route = 'Boas-vindas';
            break;
          case 'Cadastro':
            this.routePath = ' / ';
            this.route = 'Cadastro';
            break;
          case 'Instituição / Cadastro':
            this.routePath = ' / Instituição / ';
            this.route = 'Cadastro';
            break;
          case 'Meus projetos':
            this.routePath = ' / ';
            this.route = 'Meus projetos';
            break;
          case 'Projetos':
            this.routePath = ' / ';
            this.route = 'Projetos';
            break;
          case 'Validação de Parceiros':
            this.routePath = ' / ';
            this.route = 'Validação de Parceiros';
            break;
          case 'Curadoria de Projetos':
            this.routePath = ' / ';
            this.route = 'Curadoria de Projetos';
            break;
          case 'Protocolos e relatórios':
            this.routePath = ' / ';
            this.route = 'Protocolos e relatórios';
            break;
          case 'Usuários':
            this.routePath = ' / ';
            this.route = 'Usuários';
            break;
          case 'Organizações':
            this.routePath = ' / ';
            this.route = 'Organizações';
            break;
        }

        switch (this.url) {
          case 'login':
            this.setHeaderBackground('#19486A', false);
            break;
          case 'home':
            this.setHeaderBackground('#fff', false);
            break;
          case 'pre-cadastro':
          case 'boas-vindas':
            // case 'meus-projetos':
            this.setHeaderBackground('#F9F9F9', true);
            break;
          default:
            this.setHeaderBackground('#F9F9F9', false);
            return;
        }
      }
    });
  }

  ngOnInit(): void {
    this.getDisplayName();
  }

  getDisplayName() {
    this._authService.getUserName().then((el) => {
      let id = '';

      if (el && el.id) {
        id = el.id;
      }

      this.db
        .collection('users')
        .doc<IUser>(id)
        .snapshotChanges()
        .subscribe((el) => {
          const data = el.payload.data();

          if (data && data.nome) {
            this.displayName = data?.nome.split(' ')[0];
          }
        });
    });
  }

  setHeaderBackground(color: string, shadow: boolean): void {
    const header = document.querySelector('.header') as HTMLElement;
    header.style.backgroundColor = color;

    const app = document.querySelector('.app') as HTMLElement;

    if (shadow) {
      if (window.innerWidth > 1000) {
        app.style.boxShadow = '90px -14px 20px rgba(0, 0, 0, 0.07)';
      } else if (window.innerWidth < 1000) {
        app.style.boxShadow = '50px -14px 20px rgba(0, 0, 0, 0.07)';
      }
    } else {
      app.style.boxShadow = 'none';
    }
  }

  logout() {
    this.sessionService.reset();

    this.router.navigate(['/login']);
  }
}
